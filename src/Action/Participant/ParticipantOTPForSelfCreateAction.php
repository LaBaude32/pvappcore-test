<?php

namespace App\Action\Participant;

use App\Domain\PvHasUser\Data\ParticipantOTPData;
use App\Domain\PvHasUser\Service\ParticipantOTPCreator;
use Psr\Http\Message\ResponseInterface;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ParticipantOTPForSelfCreateAction 
 * 
 * @OA\get(
 *     path="/participantsOTP/pvId",
 *     tags={"Participant"},
 *     description="Recuperer le code OTP pour que les participants puissent s'enregistrer eux même", *  *     @OA\Parameter(
 *         name="pvId",
 *         in="path",
 *         description="Id du Pv",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="201", 
 *          description="Renvoie l'OTP",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/ParticipantOTPData"
 *          )
 *     ),
 *     @OA\Response(
 *          response="500", 
 *          description="Le pvId est vide",
 *     )
 * )
 */
final class ParticipantOTPForSelfCreateAction
{
    protected $participantOTPCreator;

    public function __construct(ParticipantOTPCreator $participantOTPCreator)
    {
        $this->participantOTPCreator =  $participantOTPCreator;
    }


    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $data = (array) $request->getQueryParams();

        if (isset($data['pvId']) && !empty($data['pvId'])) {
            $participantOTP = new ParticipantOTPData();
            $participantOTP->pvId = (int) htmlspecialchars($data['pvId']);
        } else {
            return $response->withStatus(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR);
        }

        $participantOTP->otp = rand(100000, 999999);

        $newParticipantOTP = $this->participantOTPCreator->createParticiantOTP($participantOTP);

        $response->getBody()->write((string)json_encode($newParticipantOTP, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}
