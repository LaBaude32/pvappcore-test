<?php

namespace App\Action\Participant;

use App\Domain\PvHasUser\Data\ParticipantOTPData;
use Symfony\Polyfill\Uuid\Uuid;
use Psr\Http\Message\ResponseInterface;
use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Service\UserCreator;
use Fig\Http\Message\StatusCodeInterface;
use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Service\ParticipantOTPValidator;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\PvHasUser\Service\PvHasUserCreator;

/**
 * ParticipantCreateAction 
 * 
 * @OA\Post(
 *     path="/participants/selfCreate",
 *     tags={"Participant"},
 *     description="Ajout d'un participant (directement lui même)",
 *     @OA\RequestBody(
 *         description="Données",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/PvHasUserData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="201", 
 *          description="Renvoie le nouveau participant",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/PvHasUserData"
 *          )
 *     ),
 *      @OA\Response(
 *          response="406", 
 *          description="Erreur, le code OTP est invalide",
 *     ),
 *      @OA\Response(
 *          response="500", 
 *          description="Erreur, le participant n''a pas été créé",
 *     )
 * )
 * TODO:Corriger la request ajouter l'OTP et le pvId
 */
final class ParticipantSelfCreateAction
{
    private $userCreator;
    private $pvHasUserCreator;
    private $otpValidator;

    public function __construct(UserCreator $userCreator, PvHasUserCreator $pvHasUserCreator, ParticipantOTPValidator $otpValidator)
    {
        $this->userCreator = $userCreator;
        $this->pvHasUserCreator = $pvHasUserCreator;
        $this->otpValidator = $otpValidator;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        //Check OTP
        $otp = new ParticipantOTPData();
        $otp->otp = htmlspecialchars($data['otp']);
        $otp->pvId = htmlspecialchars($data['pvId']);

        $valid = (bool) $this->otpValidator->validateParticiantOTP($otp);

        if (!$valid) {
            return $response->withStatus(StatusCodeInterface::STATUS_NOT_ACCEPTABLE);
        }

        //Create Participant
        $user = new UserCreateData();
        $user->email = htmlspecialchars($data['email']);
        $user->password = uuid_create(UUID_TYPE_RANDOM);
        $user->firstName = htmlspecialchars($data['firstName']);
        $user->lastName = htmlspecialchars($data['lastName']);
        $user->phone = htmlspecialchars($data['phone']);
        $user->userGroup = array_key_exists('userGroup', $data) ? htmlspecialchars($data['userGroup']) : null;
        $user->userFunction = array_key_exists('userFunction', $data) ? htmlspecialchars($data['userFunction']) : null;
        $user->organism = array_key_exists('organism', $data) ? htmlspecialchars($data['organism']) : null;

        $userId = $this->userCreator->createUser($user);

        $pvHasUser = new PvHasUserData();
        $pvHasUser->pvId = htmlspecialchars($data['pvId']);
        $pvHasUser->userId = $userId;
        $pvHasUser->statusPAE = "Présent";
        $pvHasUser->invitedCurrentMeeting = null;
        $pvHasUser->invitedNextMeeting = null;
        $pvHasUser->distribution = null;

        $newParticipant = $this->pvHasUserCreator->createPvHasUser($pvHasUser);

        if (!$newParticipant) {
            return $response->withStatus(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR);
        }

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($newParticipant, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}
