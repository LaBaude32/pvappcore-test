<?php

declare(strict_types=1);

namespace App\Action\Participant;

use App\Domain\PvHasUser\Service\PvHasUserGetter;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ParticipantGetConnectedAction
 * 
 * @OA\Get(
 *     path="/participants/connected/userId",
 *     tags={"Participant"},
 *     description="Récupération des participants connectés à un user",
 *     @OA\Parameter(
 *         name="userId",
 *         in="path",
 *         description="Id du participant",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie le tableau de tous les participants",
 *          @OA\JsonContent(
 *              type="array",
 *              @OA\Items(
 *                  ref="#/components/schemas/PvHasUserData"
 *              )
 *          )
 *      )
 * )
 */
final class ParticipantGetConnectedAction
{
    protected $pHUGetter;

    public function __construct(PvHasUserGetter $pHUGetter)
    {
        $this->pHUGetter = $pHUGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        $userId = (int) htmlspecialchars($data['userId']);

        //recuperer tous les pvs liés à cet user
        $result = $this->pHUGetter->getAllConnectedParticipants($userId);
        //recuperer les personnes de tous ces pvs

        if (!$result) {
            return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
        }

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
