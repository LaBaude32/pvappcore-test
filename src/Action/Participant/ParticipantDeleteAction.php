<?php

namespace App\Action\Participant;

use App\Domain\PvHasUser\Service\PvHasUserDeletor;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ParticipantDeleteAction 
 * 
 * @OA\Delete(
 *     path="/participants/userId",
 *     tags={"Participant"},
 *     description="Suppression d'un participant",
 *     @OA\Parameter(
 *         name="userId",
 *         in="path",
 *         description="Id du participant à supprimer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="pvId",
 *         in="path",
 *         description="Id du pv associé",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="204", 
 *          description="Ne revoie rien mais l'opération c'est bien déroulé",
 *     )
 * )
 * 
 */
final class ParticipantDeleteAction
{
    protected $pvHasUserDeletor;

    public function __construct(PvHasUserDeletor $pvHasUserDeletor)
    {
        $this->pvHasUserDeletor = $pvHasUserDeletor;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        $pvHasUser['pvId'] = (int) htmlspecialchars($data['pvId']);
        $pvHasUser['userId'] = (int) htmlspecialchars($data['userId']);

        // Invoke the Domain with inputs and retain the result
        $this->pvHasUserDeletor->deletePvHasUser($pvHasUser);

        // Transform the result into the JSON representation
        $result = "success";

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
    }
}
