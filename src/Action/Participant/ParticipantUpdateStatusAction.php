<?php

namespace App\Action\Participant;

use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Service\PvHasUserGetter;
use App\Domain\PvHasUser\Service\PvHasUserUpdater;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ParticipantUpdateStatusAction
 * 
 * @OA\Put(
 *     path="/participants/userId/updateStatus",
 *     tags={"Participant"},
 *     description="Mise à jour du status d'un participan",
 *     @OA\RequestBody(
 *         description="Données du participant (utilisateur) à mettre à jour",
 *        @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                      property="userId",
 *                      type="integer"
 *                  ),
 *                 @OA\Property(
 *                      property="pvId",
 *                      type="integer"
 *                  ),
 *                 @OA\Property(
 *                      property="statusPAE",
 *                      type="string"
 *                  ),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie le participant mis à jour",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/PvHasUserData"
 *         ),
 *      )
 * )
 * 
 */
final class ParticipantUpdateStatusAction
{
    protected $pvHasUserUpdater;
    protected $pvHasUserGetter;

    public function __construct(PvHasUserUpdater $pvHasUserUpdater, PvHasUserGetter $pvHasUserGetter)
    {
        $this->pvHasUserUpdater = $pvHasUserUpdater;
        $this->pvHasUserGetter = $pvHasUserGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $pvHasUser = new PvHasUserData();
        $pvHasUser->userId = (int) $data['userId'];
        $pvHasUser->pvId = (int) $data['pvId'];
        $pvHasUser->statusPAE = (array_key_exists('statusPAE', $data) && !is_null($data['statusPAE']) && strlen($data['statusPAE']) > 0) ? (string) $data['statusPAE'] : NULL;
        $pvHasUser->invitedCurrentMeeting = !is_null($data['invitedCurrentMeeting']) ? (bool) htmlspecialchars($data['invitedCurrentMeeting']) : NULL;
        $pvHasUser->invitedNextMeeting = !is_null($data['invitedNextMeeting']) ? (bool) htmlspecialchars($data['invitedNextMeeting']) : NULL;
        $pvHasUser->distribution = !is_null($data['distribution']) ? (bool) htmlspecialchars($data['distribution']) : NULL;

        $this->pvHasUserUpdater->updatePvHasUser($pvHasUser);

        $result = $this->pvHasUserGetter->getParticipantStatus($pvHasUser->userId, $pvHasUser->pvId);

        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));
        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
