<?php

namespace App\Action\Participant;

use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Service\PvHasUserUpdater;
use UnexpectedValueException;
use App\Domain\User\Data\UserGetData;
use App\Domain\User\Service\UserGetter;
use App\Domain\User\Service\UserUpdater;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ParticipantUpdateAction
 * 
 * @OA\Put(
 *     path="/participants/userId",
 *     tags={"Participant"},
 *     description="Mise à jour d'un participan",
 *     @OA\RequestBody(
 *         description="Données du participant (utilisateur) à mettre à jour",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/UserGetData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie du participant (utilisateur)",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/UserGetData"
 *         ),
 *      )
 * )
 * 
 */
final class ParticipantUpdateAction
{
    private $userUpdater;
    protected $userGetter;

    protected $pvHasUserUpdater;

    public function __construct(UserUpdater $userUpdater, UserGetter $userGetter, PvHasUserUpdater $pvHasUserUpdater)
    {
        $this->userUpdater = $userUpdater;
        $this->userGetter = $userGetter;
        $this->pvHasUserUpdater = $pvHasUserUpdater;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $user = new UserGetData();
        $user->userId = (int) htmlspecialchars($data['userId']);
        $user->email = (string) htmlspecialchars($data['email']);
        $user->firstName = (string) htmlspecialchars($data['firstName']);
        $user->lastName = (string) htmlspecialchars($data['lastName']);
        $user->phone = (string) htmlspecialchars($data['phone']);
        $user->userGroup = (string) htmlspecialchars($data['userGroup']);
        $user->userFunction = (string) htmlspecialchars($data['userFunction']);
        $user->organism = (string) htmlspecialchars($data['organism']);

        // Invoke the Domain with inputs and retain the result
        $this->userUpdater->updateParticipant($user);

        $newUser = $this->userGetter->getUserById($user->userId);

        foreach ($newUser as $key => $value) {
            if ($user->$key !== $value && $key != "pwd" && $key != "userGroup") {
                throw new UnexpectedValueException('Erreur sur le ' . $key . ' qui est différent');
            }
        }

        // update Status_PAE
        $pvHasUser = new PvHasUserData();
        $pvHasUser->userId = $newUser->userId;
        $pvHasUser->pvId = $data['pvId'];
        $pvHasUser->statusPAE = (!is_null($data['statusPAE']) && strlen($data['statusPAE']) > 0) ? (string) $data['statusPAE'] : NULL;
        $pvHasUser->invitedCurrentMeeting = !is_null($data['invitedCurrentMeeting']) ? (bool) htmlspecialchars($data['invitedCurrentMeeting']) : NULL;
        $pvHasUser->invitedNextMeeting = !is_null($data['invitedNextMeeting']) ? (bool) htmlspecialchars($data['invitedNextMeeting']) : NULL;
        $pvHasUser->distribution = !is_null($data['distribution']) ? (bool) htmlspecialchars($data['distribution']) : NULL;

        $this->pvHasUserUpdater->updatePvHasUser($pvHasUser);

        $newUser = $this->userGetter->getUserWithStatusById($user->userId);

        // Transform the result into the JSON representation
        $result = $newUser;

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
