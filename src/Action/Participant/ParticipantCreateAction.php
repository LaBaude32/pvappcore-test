<?php

namespace App\Action\Participant;

use Psr\Http\Message\ResponseInterface;
use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Service\PvHasUserCreator;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ParticipantCreateAction 
 * 
 * @OA\Post(
 *     path="/participants",
 *     tags={"Participant"},
 *     description="Associer un utilisateur à un Pv",
 *     @OA\RequestBody(
 *         description="Données",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/PvHasUserData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="201", 
 *          description="Renvoie le nouveau participant",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/PvHasUserData"
 *          )
 *     )
 * )
 * TODO:Corriger la request
 */
final class ParticipantCreateAction
{
    private $pvHasUserCreator;

    public function __construct(PvHasUserCreator $pvHasUserCreator)
    {
        $this->pvHasUserCreator = $pvHasUserCreator;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $pvHasUser = new PvHasUserData();
        $pvHasUser->pvId = htmlspecialchars($data['pvId']);
        $pvHasUser->userId = htmlspecialchars($data['userId']);
        $pvHasUser->statusPAE = !is_null($data['statusPAE']) ? htmlspecialchars($data['statusPAE']) : NULL;
        $pvHasUser->invitedCurrentMeeting = (bool) isset($data['invitedCurrentMeeting']) ?  $data['invitedCurrentMeeting'] : false;
        $pvHasUser->invitedNextMeeting = (bool) isset($data['invitedNextMeeting']) ?  $data['invitedNextMeeting'] : false;
        $pvHasUser->distribution = (bool) isset($data['distribution']) ? $data['distribution'] : false;

        // Invoke the Domain with inputs and retain the result
        $newParticipant = $this->pvHasUserCreator->createPvHasUser($pvHasUser);

        // Transform the result into the JSON representation
        // $result = [
        //     'Response' => 'Success'
        // ];

        if (!$newParticipant) {
            return $response->withStatus(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR);
        }

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($newParticipant, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}
