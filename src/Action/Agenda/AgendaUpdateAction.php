<?php

namespace App\Action\Agenda;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Agenda\Data\AgendaGetData;
use App\Domain\Agenda\Service\AgendaGetter;
use App\Domain\Agenda\Service\AgendaUpdater;
use Fig\Http\Message\StatusCodeInterface;

/**
 * AgendaUpdateAction
 * 
 * @OA\Put(
 *     path="/agendas/agendaId",
 *     tags={"Agenda"},
 *     description="Mise à jour de l'ordre du jour",
 *     @OA\RequestBody(
 *         description="Données de l'ordre du jour à mettre à jour",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/AgendaGetData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie l'ordre du jour mis à jour",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/AgendaGetData"
 *         ),
 *      )
 * )
 * 
 */
final class AgendaUpdateAction
{
    private $agendaUpdater;

    protected $agendaGetter;

    public function __construct(AgendaUpdater $agendaUpdater, AgendaGetter $agendaGetter)
    {
        $this->agendaUpdater = $agendaUpdater;
        $this->agendaGetter = $agendaGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $datas = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        foreach ($datas as $data) {
            $agenda = new AgendaGetData();
            $agenda->agendaId = (int) array_key_exists('agendaId', $data) ? htmlspecialchars($data['agendaId']) : NULL;
            $agenda->position = (int) htmlspecialchars($data['position']);
            $agenda->title = (string) htmlspecialchars($data['title']);
            $agenda->pvId = (int) htmlspecialchars($data['pvId']);
            $agendas[] = $agenda;
            $pvId = (int) htmlspecialchars($data['pvId']);
        }

        // Invoke the Domain with inputs and retain the result
        $newAgenda = $this->agendaUpdater->updateAgenda($agendas, $pvId);

        // Transform the result into the JSON representation
        $result = $newAgenda;

        if (!$result) {
            return $response->withStatus(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR);
        }

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
