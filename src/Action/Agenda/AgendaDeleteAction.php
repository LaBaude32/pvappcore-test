<?php

declare(strict_types=1);

namespace App\Action\Agenda;

use Psr\Http\Message\ResponseInterface;
use Fig\Http\Message\StatusCodeInterface;
use App\Domain\Agenda\Service\AgendaDeletor;
use Psr\Http\Message\ServerRequestInterface;


/**
 * AgendaDeleteAction 
 * 
 * @OA\Delete(
 *     path="/agendas/agendaId",
 *     tags={"Agenda"},
 *     description="Suppression d'un ordre du jour",
 *     @OA\Parameter(
 *         name="agendaId",
 *         in="path",
 *         description="Id de l'ordre du jour à supprimer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="204", 
 *          description="Ne revoie rien mais l'opération c'est bien déroulé",
 *     )
 * )
 * 
 */
class AgendaDeleteAction
{
    private $agendaDeletor;

    public function __construct(AgendaDeletor $agendaDeletor)
    {
        $this->agendaDeletor = $agendaDeletor;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        $id = (int) htmlspecialchars($data['agendaId']);

        // Invoke the Domain with inputs and retain the result
        $this->agendaDeletor->deleteAgenda($id);

        return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
    }
}
