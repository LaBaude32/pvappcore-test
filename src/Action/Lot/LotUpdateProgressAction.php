<?php

namespace App\Action\Lot;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use UnexpectedValueException;
use App\Domain\Lot\Data\LotGetData;
use App\Domain\Lot\Data\LotProgressGetData;
use App\Domain\Lot\Service\LotGetter;
use App\Domain\Lot\Service\LotUpdater;
use Fig\Http\Message\StatusCodeInterface;

/**
 * LotUpdateProgressAction
 * 
 * @OA\Put(
 *     path="/lots/progress",
 *     tags={"Lot"},
 *     description="Mise à jour de l'avancement d'un lot",
 *     @OA\RequestBody(
 *         description="Données du lot à mettre à jour",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/LotProgressGetData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="202", 
 *          description="Renvoie le lot mis à jour",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/LotProgressGetData"
 *         ),
 *      ),
 *      @OA\Response(
 *          response="500", 
 *          description="Erreur lors de la mise à jour du Lot",
 *      )
 * )
 * 
 */
final class LotUpdateProgressAction
{
    private $lotUpdater;

    protected $lotGetter;

    public function __construct(LotUpdater $lotUpdater, LotGetter $lotGetter)
    {
        $this->lotUpdater = $lotUpdater;
        $this->lotGetter = $lotGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $lot = new LotProgressGetData();
        $lot->lotId = (int) htmlspecialchars($data['lotId']);
        $lot->pvId = (int) htmlspecialchars($data['pvId']);
        $lot->name = htmlspecialchars($data['name']);
        $lot->progress = (int) htmlspecialchars($data['progress']);
        $lot->alreadyDone = !empty($data['alreadyDone']) ? htmlspecialchars($data['alreadyDone']) : NULL;

        // Invoke the Domain with inputs and retain the result
        $newLot = $this->lotUpdater->updateLotProgress($lot);

        // Transform the result into the JSON representation
        $result = $newLot;

        if (!$result) {
            return $response->withStatus(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR);
        }

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_ACCEPTED);
    }
}
