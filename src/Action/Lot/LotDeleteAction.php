<?php

namespace App\Action\Lot;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Lot\Service\LotDeletor;
use Fig\Http\Message\StatusCodeInterface;

/**
 * LotDeleteAction 
 * 
 * @OA\Delete(
 *     path="/lots/lotId",
 *     tags={"Lot"},
 *     description="Suppression d'un lot",
 *     @OA\Parameter(
 *         name="lotId",
 *         in="path",
 *         description="Id du lot à supprimer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="204", 
 *          description="Ne revoie rien mais l'opération c'est bien déroulé",
 *     )
 * )
 * 
 */
final class LotDeleteAction
{
    private $lotDeletor;

    public function __construct(LotDeletor $lotDeletor)
    {
        $this->lotDeletor = $lotDeletor;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        $id = (int) htmlspecialchars($data['lotId']);

        // Invoke the Domain with inputs and retain the result
        $this->lotDeletor->deleteLot($id);

        return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
    }
}
