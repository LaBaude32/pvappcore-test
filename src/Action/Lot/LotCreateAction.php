<?php

namespace App\Action\Lot;

use App\Domain\Lot\Data\LotCreateData;
use App\Domain\Lot\Service\LotCreator;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * LotCreateAction
 * 
 * @OA\Post(
 *     path="/lots",
 *     tags={"Lot"},
 *     description="Ajout d'un lot",
 *     @OA\RequestBody(
 *         description="Données du lot à ajouter",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/LotCreateData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="201", 
 *          description="Renvoie le nouveau lot",
 *          @OA\JsonContent(
 *                 type="object",
 *                 ref="#/components/schemas/LotGetData"
 *          )
 *     ),
 *      @OA\Response(
 *          response="500", 
 *          description="Erreur à la création du Lot",
 *     )
 * )
 *
 */

final class LotCreateAction
{
    private $lotCreator;

    public function __construct(LotCreator $lotCreator)
    {
        $this->lotCreator = $lotCreator;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $lot = new LotCreateData();
        $lot->name = htmlspecialchars($data['name']);
        $lot->affairId = (int) htmlspecialchars($data['affairId']);

        // Invoke the Domain with inputs and retain the result
        $lot = $this->lotCreator->createLot($lot);

        // Transform the result into the JSON representation
        $result = $lot;

        if (!$result) {
            return $response->withStatus(StatusCodeInterface::STATUS_INTERNAL_SERVER_ERROR);
        }

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}
