<?php

namespace App\Action\Pv;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Pv\Data\PvGetData;
use App\Domain\Pv\Service\PvGetter;
use App\Domain\Pv\Service\PvUpdater;
use Fig\Http\Message\StatusCodeInterface;

/**
 * PvUpdateAction
 * 
 * @OA\Put(
 *     path="/pvs/pvId",
 *     tags={"Pv"},
 *     description="Mise à jour d'un pv",
 *     @OA\RequestBody(
 *         description="Données du pv à mettre à jour",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/PvGetData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="204", 
 *          description="Renvoie le pv",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/PvGetData"
 *          ),
 *      )
 * )
 * TODO: retirer les lots de la requete
 */
final class PvUpdateAction
{
    private $pvUpdater;

    protected $pvGetter;

    public function __construct(PvUpdater $pvUpdater, PvGetter $pvGetter)
    {
        $this->pvUpdater = $pvUpdater;
        $this->pvGetter = $pvGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $pv = new PvGetData();
        $pv->pvId = htmlspecialchars($data['pvId']);
        $pv->state = htmlspecialchars($data['state']);
        if (!empty($data['meetingDate'])) {
            $pv->meetingDate = htmlspecialchars($data['meetingDate']);
        }
        $pv->meetingPlace = htmlspecialchars($data['meetingPlace']);
        if (!empty($data['meetingNextDate'])) {
            $pv->meetingNextDate = htmlspecialchars($data['meetingNextDate']);
        }
        $pv->meetingNextPlace = $data['meetingNextPlace'] ? htmlspecialchars($data['meetingNextPlace']) : null;
        $pv->affairId = htmlspecialchars($data['affairId']);

        // Invoke the Domain with inputs and retain the result
        $pvId = $this->pvUpdater->updatePv($pv);

        $pvToReturn = $this->pvGetter->getPvById($pvId);

        // Transform the result into the JSON representation
        $result = $pvToReturn;

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
