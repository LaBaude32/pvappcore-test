<?php

namespace App\Action\Pv;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Pv\Service\PvGetter;
use App\Domain\Item\Service\ItemGetter;
use App\Domain\PvHasUser\Service\PvHasUserGetter;
use App\Domain\User\Service\UserGetter;
use App\Domain\Affair\Service\AffairGetter;
use Fig\Http\Message\StatusCodeInterface;

/**
 * PvGetReleasedDetails
 * 
 * @OA\Get(
 *     path="/pvs/pvId/released",
 *     tags={"Pv"},
 *     description="Récupération d'un pv par son Id",
 *     @OA\Parameter(
 *         name="pvId",
 *         in="path",
 *         description="Id du pv à récupérer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie le pv avec toutes ses infos",
 *          @OA\JsonContent(
 *              type="object",
 *              @OA\Property(
 *                  property="pv",
 *                  ref="#/components/schemas/PvGetData"
 *              ),
 *              @OA\Property(
 *                  property="items",
 *                  type="array",
 *                  @OA\Items(
 *                      ref="#/components/schemas/ItemGetData"
 *                  )
 *              ),
 *              @OA\Property(
 *                  property="users",
 *                  type="array",
 *                  @OA\Items(
 *                      ref="#/components/schemas/UserGetData"
 *                  )
 *              ),
 *              @OA\Property(
 *                  property="owner",
 *                  ref="#/components/schemas/UserGetData"
 *              ),
 *              @OA\Property(
 *                  property="affair",
 *                  type="object",
 *                  @OA\Property(
 *                      property="affair",
 *                      ref="#/components/schemas/UserGetData"
 *                  ),
 *                  @OA\Property(
 *                      property="lots",
 *                      type="array",
 *                      @OA\Items(
 *                          ref="#/components/schemas/LotGetData"
 *                      )
 *                  ), 
 *              ),
 *          )
 *      )
 * )
 */
final class PvGetReleasedDetails
{
    private $pvGetter;
    private $itemGetter;
    private $userGetter;
    private $pvHasUserGetter;
    private $affairGetter;

    public function __construct(PvGetter $pvGetter, ItemGetter $itemGetter, UserGetter $userGetter, PvHasUserGetter $pvHasUserGetter, AffairGetter $affairGetter)
    {
        $this->pvGetter = $pvGetter;
        $this->itemGetter = $itemGetter;
        $this->userGetter = $userGetter;
        $this->pvHasUserGetter = $pvHasUserGetter;
        $this->affairGetter = $affairGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        $id = (int) htmlspecialchars($data['pvId']);

        // Invoke the Domain with inputs and retain the result
        $pv = $this->pvGetter->getPvById($id);

        $items = $this->itemGetter->getVisibleItemsByPvId($id);

        if ($items) {
            $itemsWithLots = $this->itemGetter->getLotsForItems($items);
        }

        $participants = $this->pvHasUserGetter->getParticipantsForPv($id);

        $owner = $this->pvHasUserGetter->getPvOwner($pv->pvId);

        $affair = $this->affairGetter->getAffairById($pv->affairId);

        $result = [
            'pv' => $pv,
            'participants' => $participants,
            'owner' => $owner,
            'affair' => $affair
        ];

        if (isset($itemsWithLots)) {
            $result['items'] = $itemsWithLots;
        };

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
