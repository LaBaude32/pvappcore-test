<?php

declare(strict_types=1);

namespace App\Action\Pv;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Pv\Service\PvGetter;
use Fig\Http\Message\StatusCodeInterface;

/**
 * PvsGetReleasedByUserIdAction
 * 
 * @OA\Get(
 *     path="/pvs/userId/released",
 *     tags={"Pv"},
 *     description="Récupération des pv diffusés par un user",
 *     @OA\Parameter(
 *         name="userId",
 *         in="path",
 *         description="Id de l'user",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie un tableau de pv",
 *          @OA\JsonContent(
 *               type="array",
 *               @OA\Items(
 *                  ref="#/components/schemas/PvGetData"
 *               )
 *          )
 *     ),
 *     @OA\Response(
 *          response="204", 
 *          description="Ne renvoie rien car il n'y a pas de Pvs",
 *     )
 * )
 *
 */
final class PvsGetReleasedByUserIdAction
{
    private $pvGetter;

    public function __construct(PvGetter $pvGetter)
    {
        $this->pvGetter = $pvGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $params = (array) $request->getQueryParams();

        $userId = (int) htmlspecialchars($params['userId']);

        // Invoke the Domain with inputs and retain the result
        $result = $this->pvGetter->getRelesedPvsByUserId($userId);

        if (!$result) {
            return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
        }

        // Build the HTTP response 
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
