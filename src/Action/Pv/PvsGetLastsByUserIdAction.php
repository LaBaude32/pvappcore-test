<?php

namespace App\Action\Pv;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Pv\Service\PvGetter;
use Fig\Http\Message\StatusCodeInterface;

/**
 * PvsGetLastsByUserIdAction
 * 
 * @OA\Get(
 *     path="/pvs/userId",
 *     tags={"Pv"},
 *     description="Récupération des pv d'une affaire",
 *     @OA\Parameter(
 *         name="userId",
 *         in="path",
 *         description="Id de l'user",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="numberOfPvs",
 *         in="path",
 *         description="Nombre de pv à récupérer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie un tableau de pv",
 *          @OA\JsonContent(
 *               type="array",
 *               @OA\Items(
 *                  ref="#/components/schemas/PvGetData"
 *               )
 *          )
 *     ),
 *     @OA\Response(
 *          response="204", 
 *          description="Ne renvoie rien car il n'y a pas de Pvs",
 *     )
 * )
 *
 */
final class PvsGetLastsByUserIdAction
{
    private $pvGetter;

    public function __construct(PvGetter $pvGetter)
    {
        $this->pvGetter = $pvGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $params = (array) $request->getQueryParams();

        $userId = (int) htmlspecialchars($params['userId']);
        $numberOfPvs = (int) htmlspecialchars($params['numberOfPvs']);

        $data = [
            "userId" => $userId,
            "numberOfPvs" => $numberOfPvs
        ];

        // Invoke the Domain with inputs and retain the result
        $result = $this->pvGetter->getLastsPvByUserId($data);

        if (!$result) {
            return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
        }

        // Build the HTTP response 
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
