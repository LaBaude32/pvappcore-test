<?php

namespace App\Action\Pv;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Pv\Service\PvGetter;
use App\Domain\Item\Service\ItemGetter;
use App\Domain\PvHasUser\Service\PvHasUserGetter;
use Fig\Http\Message\StatusCodeInterface;

/**
 * PvGetByIdAction
 * 
 * @OA\Get(
 *     path="/pvs/pvId",
 *     tags={"Pv"},
 *     description="Récupération d'un pv par son Id",
 *     @OA\Parameter(
 *         name="pvId",
 *         in="path",
 *         description="Id du pv à récupérer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="userId",
 *         in="path",
 *         description="Id du user",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie le pv avec toutes ses infos",
 *          @OA\JsonContent(
 *              type="object",
 *              @OA\Property(
 *                  property="pv",
 *                  ref="#/components/schemas/PvGetData"
 *              ),
 *              @OA\Property(
 *                  property="items",
 *                  type="array",
 *                  @OA\Items(
 *                      ref="#/components/schemas/ItemGetData"
 *                  )
 *              ),
 *              @OA\Property(
 *                  property="participants",
 *                  type="array",
 *                  @OA\Items(
 *                      ref="#/components/schemas/PvHasUserData"
 *                  )
 *              ),
 *              @OA\Property(
 *                  property="connectedParticipants",
 *                  type="array",
 *                  @OA\Items(
 *                      ref="#/components/schemas/PvHasUserData"
 *                  )
 *              ),
 *          )
 *      )
 * )
 */
final class PvGetByIdAction
{
    private $pvGetter;
    private $itemGetter;

    protected $pHUGetter;

    public function __construct(PvGetter $pvGetter, ItemGetter $itemGetter, PvHasUserGetter $pHUGetter)
    {
        $this->pvGetter = $pvGetter;
        $this->itemGetter = $itemGetter;
        $this->pHUGetter = $pHUGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        $id = (int) htmlspecialchars($data['pvId']);
        $userId = (int) htmlspecialchars($data['userId']);

        // Invoke the Domain with inputs and retain the result
        $pv = $this->pvGetter->getPvById($id);
        $pv = $this->pvGetter->getLotsForPv($pv);

        $items = $this->itemGetter->getItemsByPvId($id);

        if ($items) {
            $itemsWithLots = $this->itemGetter->getLotsForItems($items);
        } else {
            $itemsWithLots = null;
        }

        $participants = $this->pHUGetter->getParticipantsForPv($id);

        //CONNECTED PARTICIPANTS
        $connectedParticipants = $this->pHUGetter->getAllConnectedParticipants($userId);

        //Suppression des connected participants qui sont déjà participants
        $participants_userId = array_column($participants, 'userId');
        $filterdConnectedParticipants = array_values(array_filter($connectedParticipants, function ($participant) use ($participants_userId) {
            return !in_array($participant->userId, $participants_userId);
        }));


        $result = [
            'pv' => $pv,
            'items' => $itemsWithLots,
            'participants' => $participants,
            'connectedParticipants' => $filterdConnectedParticipants
        ];

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
