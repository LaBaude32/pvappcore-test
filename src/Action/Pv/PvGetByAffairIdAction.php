<?php

namespace App\Action\Pv;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Pv\Service\PvGetter;
use Fig\Http\Message\StatusCodeInterface;

/**
 * PvGetByAffairIdAction
 * 
 * @OA\Get(
 *     path="/pvs/affairId",
 *     tags={"Pv"},
 *     description="Récupération des pv d'une affaire",
 *     @OA\Parameter(
 *         name="affairId",
 *         in="path",
 *         description="Id de l'affaire",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie un tableau de pv",
 *          @OA\JsonContent(
 *               type="array",
 *               @OA\Items(
 *                  ref="#/components/schemas/PvGetData"
 *               )
 *          )
 *     )
 * )
 *
 */
final class PvGetByAffairIdAction
{
    private $pvGetter;

    public function __construct(PvGetter $pvGetter)
    {
        $this->pvGetter = $pvGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        $id = (int) htmlspecialchars($data['affairId']);

        // Invoke the Domain with inputs and retain the result
        $result = $this->pvGetter->getPvByAffairId($id);

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
