<?php

namespace App\Action\Pv;

use App\Domain\Agenda\Service\AgendaGetter;
use App\Domain\Agenda\Service\AgendaUpdater;
use App\Domain\Item\Service\ItemCreator;
use App\Domain\Item\Service\ItemGetter;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Pv\Data\PvCreateData;
use App\Domain\Pv\Service\PvCreator;
use App\Domain\Pv\Service\PvGetter;
use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Service\PvHasUserCreator;
use App\Domain\PvHasUser\Service\PvHasUserGetter;
use Fig\Http\Message\StatusCodeInterface;

/**
 * PvCreateAction
 * 
 * @OA\Post(
 *     path="/pvs",
 *     tags={"Pv"},
 *     description="Ajout d'un pv",
 *     @OA\RequestBody(
 *         description="Données du pv à ajouter",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/PvCreateData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="201", 
 *          description="Renvoie nouveau pv complet",
 *          @OA\JsonContent(
 *               type="object",
 *               ref="#/components/schemas/PvGetData"
 *          )
 *     )
 * )
 *
 */
final class PvCreateAction
{
    private $pvCreator;
    private $pvHasUserCreator;
    private $itemCreator;
    private $pvGetter;
    private $itemGetter;
    private $pvHasUserGetter;
    private $agendaGetter;
    private $agendaUpdater;

    public function __construct(PvCreator $pvCreator, PvHasUserCreator $pvHasUserCreator, ItemCreator $itemCreator, PvGetter $pvGetter, ItemGetter $itemGetter, PvHasUserGetter $pvHasUserGetter, AgendaGetter $agendaGetter, AgendaUpdater $agendaUpdater)
    {
        $this->pvCreator = $pvCreator;
        $this->pvHasUserCreator = $pvHasUserCreator;
        $this->itemCreator = $itemCreator;
        $this->pvGetter = $pvGetter;
        $this->itemGetter = $itemGetter;
        $this->pvHasUserGetter = $pvHasUserGetter;
        $this->agendaGetter = $agendaGetter;
        $this->agendaUpdater = $agendaUpdater;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $pv = new PvCreateData();
        $pv->state = (string) $data['state'];
        $pv->meetingDate = (string) htmlspecialchars($data['meetingDate']);
        $pv->meetingPlace = (string) htmlspecialchars($data['meetingPlace']);
        $pv->meetingNextDate = array_key_exists('meetingNextDate', $data) && $data["meetingNextDate"] != null ? (string) htmlspecialchars($data['meetingNextDate']) : null;
        $pv->meetingNextPlace = array_key_exists('meetingNextPlace', $data) && $data["meetingNextPlace"] != null ? (string) htmlspecialchars($data['meetingNextPlace']) : null;
        $pv->affairId = (int) htmlspecialchars($data['affairId']);

        // Invoke the Domain with inputs and retain the result
        $pvId = $this->pvCreator->createPv($pv);

        $pv = $this->pvGetter->getPvById($pvId);

        if ($pv->pvNumber > 1) {
            //Recuperer l'ancien pv
            $pvs = $this->pvGetter->getPvByAffairId($pv->affairId);
            foreach ($pvs as $value) {
                if ($value->pvNumber == $pv->pvNumber - 1) {
                    $previousPv = $value;
                }
            }

            //Récuperer tous les pHI du $previousPv 
            $allPHI = $this->itemGetter->getPvHasItem($previousPv);

            if (!empty($allPHI)) {
                //On met l'id du nouveau pv
                foreach ($allPHI as $pHI) {
                    $pHI->pvId = $pv->pvId;
                }

                //Créer les nouveaux pHI
                $this->itemCreator->addItemsToNewPv($allPHI);
            }

            //Récuperer tous les pHU
            $allPHU = $this->pvHasUserGetter->getPvHasUsers($previousPv);

            //Les ajouter dans le nouveau PV
            foreach ($allPHU as $pHU) {
                $pHU->pvId = $pv->pvId;
            }

            //Créer les nouveaux pHI
            $this->pvHasUserCreator->addUsersToNewPv($allPHU);

            //Récuperer les ODJ du PV n-1
            $allAgendas = $this->agendaGetter->getAgendasByPvId($previousPv->pvId);

            if (!empty($allAgendas)) {
                foreach ($allAgendas as $agenda) {
                    $agenda->pvId = $pv->pvId;
                }
                $this->agendaUpdater->updateAgenda($allAgendas, $pv->pvId);
            }
        } else {
            //Si c'est le premier pv
            $pHU = new PvHasUserData();
            $pHU->pvId = $pvId;
            $pHU->userId = (int) htmlspecialchars($data['userId']);
            $pHU->statusPAE = "Présent";
            $pHU->invitedCurrentMeeting = 1;
            $pHU->invitedNextMeeting = 1;
            $pHU->distribution = 1;
            $pHU->owner = 1;

            $this->pvHasUserCreator->createPvHasUser($pHU);
        }

        // Transform the result into the JSON representation
        $result =  $pv;

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}
