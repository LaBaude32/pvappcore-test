<?php

namespace App\Action\Pv;

use App\Domain\Pv\Service\PvUpdater;
use Psr\Http\Message\ResponseInterface;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * PvUnValidateAction 
 * 
 * @OA\Put(
 *     path="/pvs/pvId/unValidate",
 *     tags={"Pv"},
 *     description="Débloquer un pv validé",
 *     @OA\Parameter(
 *         name="pvId",
 *         in="path",
 *         description="Id du pv",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie le Pv mis à jour",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/PvGetData"
 *          ),
 *     )
 * )
 */
final class PvUnValidateAction
{
    private $pvUpdater;

    public function __construct(PvUpdater $pvUpdater)
    {
        $this->pvUpdater = $pvUpdater;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $idPv = (int) htmlspecialchars($data['pvId']);

        // Invoke the Domain with inputs and retain the result
        $pv = $this->pvUpdater->unValidatePv($idPv);

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($pv, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
