<?php

namespace App\Action\Pv;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Pv\Service\PvUpdater;
use Fig\Http\Message\StatusCodeInterface;

/**
 * PvValidateAction 
 * 
 * @OA\Put(
 *     path="/pvs/pvId/validation",
 *     tags={"Pv"},
 *     description="Valider un pv",
 *     @OA\Parameter(
 *         name="pvId",
 *         in="path",
 *         description="Id du pv",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="204", 
 *          description="Ne revoie rien mais l'opération c'est bien déroulé",
 *     )
 * )
 * 
 */
final class PvValidateAction
{
    private $pvUpdater;

    public function __construct(PvUpdater $pvUpdater)
    {
        $this->pvUpdater = $pvUpdater;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)

        $idPv = (int) htmlspecialchars($data['pvId']);

        // Invoke the Domain with inputs and retain the result
        $this->pvUpdater->validatePv($idPv);

        // // Transform the result into the JSON representation
        // $result = "success";

        // // Build the HTTP response
        // $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
    }
}
