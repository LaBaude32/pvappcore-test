<?php

namespace App\Action\Pv;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Pv\Service\PvDeletor;
use Fig\Http\Message\StatusCodeInterface;

/**
 * PvDeleteAction 
 * 
 * @OA\Delete(
 *     path="/pvs/pvId",
 *     tags={"Pv"},
 *     description="Suppression d'un pv",
 *     @OA\Parameter(
 *         name="pvId",
 *         in="path",
 *         description="Id du pv à supprimer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="204", 
 *          description="Ne revoie rien mais l'opération c'est bien déroulé",
 *     )
 * )
 * 
 */
final class PvDeleteAction
{
    private $pvDeletor;

    public function __construct(PvDeletor $pvDeletor)
    {
        $this->pvDeletor = $pvDeletor;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        $id = (int) htmlspecialchars($data['pvId']);

        // Invoke the Domain with inputs and retain the result
        $this->pvDeletor->deletePv($id);

        // $result = ["Le pv a bien été supprimé"];

        // // Build the HTTP response
        // $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
    }
}
