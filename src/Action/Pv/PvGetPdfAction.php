<?php

namespace App\Action\Pv;

use Dompdf\Dompdf;
use Dompdf\Options;
use Slim\Views\Twig;
use App\Domain\Pv\Service\PvGetter;
use App\Domain\Item\Service\ItemGetter;
use App\Domain\User\Service\UserGetter;
use Psr\Http\Message\ResponseInterface;
use App\Domain\Affair\Service\AffairGetter;
use App\Domain\Lot\Service\LotGetter;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\PvHasUser\Service\PvHasUserGetter;
use Psr\Http\Message\StreamFactoryInterface;

/**
 * PvGetPdfAction
 * 
 * @OA\Get(
 *     path="/pvs/pvId/released/pdf",
 *     tags={"Pv"},
 *     description="Récupération d'un pv par son Id",
 *     @OA\Parameter(
 *         name="pvId",
 *         in="path",
 *         description="Id du pv à récupérer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *           response="200",
 *           description="Renvoie le pdf du pv à telecharger",
 *           content={
 *              @OA\MediaType(
 *                 mediaType="application/pdf", 
 *                 )
 *            }
 *     )
 * )
 */
final class PvGetPdfAction
{
    private $twig;
    private $userGetter;
    private $pvGetter;
    private $affairGetter;
    private $itemGetter;
    private $pvHasUserGetter;
    private $lotGetter;
    private StreamFactoryInterface $streamFactory;

    public function __construct(Twig $twig, UserGetter $userGetter, PvGetter $pvGetter, AffairGetter $affairGetter, ItemGetter $itemGetter, PvHasUserGetter $pvHasUserGetter, LotGetter $lotGetter,  StreamFactoryInterface $streamFactory)
    {
        $this->twig = $twig;
        $this->userGetter = $userGetter;
        $this->pvGetter = $pvGetter;
        $this->affairGetter = $affairGetter;
        $this->itemGetter = $itemGetter;
        $this->pvHasUserGetter = $pvHasUserGetter;
        $this->lotGetter = $lotGetter;
        $this->streamFactory = $streamFactory;
    }
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response)
    {
        $data = (array) $request->getQueryParams();

        $pvId = (int) htmlspecialchars($data['pvId']);

        $pv = $this->pvGetter->getPvById($pvId);
        $owner = $this->pvHasUserGetter->getPvOwner($pv->pvId);
        $affair = $this->affairGetter->getAffairById($pv->affairId);
        $participants = $this->pvHasUserGetter->getParticipantsForPv($pvId);
        // $items = array_reverse($this->itemGetter->getVisibleItemsByPvId($pvId));
        $items = $this->itemGetter->getVisibleItemsByPvId($pvId);
        if ($items) {
            $itemsWithLots = $this->itemGetter->getLotsForItems($items);
        }
        $lots = NULL;
        if ($pv->affairMeetingType == "Chantier") {
            $lots = $this->lotGetter->getLotsByPvId($pv->pvId);
        }

        $maitriseOeuvre = array();
        $maitriseOuvrage = array();

        foreach ($participants as $user) {
            if ($user->userGroup == "Maîtrise d'oeuvre") {
                if (!in_array($user->organism, $maitriseOeuvre)) {
                    $maitriseOeuvre[] = $user->organism;
                }
            }
            if ($user->userGroup == "Maîtrise d'ouvrage") {
                if (!in_array($user->organism, $maitriseOuvrage)) {
                    $maitriseOuvrage[] = $user->organism;
                }
            }
            $status = array();
            if ($user->invitedCurrentMeeting) {
                $status[] = "C1";
            }
            if ($user->statusPAE == "Présent" && $user->invitedCurrentMeeting) {
                $status[] = "P";
            }
            if ($user->statusPAE == "Absent" && $user->invitedCurrentMeeting) {
                $status[] = "A";
            }
            if ($user->statusPAE == "Excusé" && $user->invitedCurrentMeeting) {
                $status[] = "E";
            }
            if ($user->invitedNextMeeting) {
                $status[] = "C2";
            }
            if ($user->distribution) {
                $status[] = "D";
            }
            $user->statusPAE = implode(" - ", $status);
        }

        $data = [
            'pv' => $pv,
            'affair' => $affair,
            //le groupe Maîtrise d'ouvrage est abscent car il est traiter séparement dans twig
            'userGroups' => [
                "Assistance à la maîtrise d'ouvrage",
                "Maîtrise d'oeuvre",
                'Entreprise',
                'Concessionnaire',
                'Personne public associée',
                'COPIL',
                'COTEC',
                'Divers'
            ],
            'users' => $participants,
            'owner' => $owner,
            "isImage" => false
        ];

        if (isset($lots)) {
            $data['lots'] = $lots;
        };

        if (isset($itemsWithLots)) {
            $data['items'] = $itemsWithLots;
        };

        foreach ($data['items'] as $item) {
            if ($item->image != null || $item->image != "") {
                $path = realpath('') . "/images/" . $item->image;
                $imgData = file_get_contents($path);
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $pic = 'data:image/' . $type . ';base64,' . base64_encode($imgData);
                $item->image = $pic;
                $data["isImage"] = true;
            }
        }

        if (!empty($maitriseOeuvre)) {
            $data["maitriseOeuvre"] = $maitriseOeuvre;
        }
        $GLOBALS['maitriseOeuvreList'] =
            implode(" - ", $maitriseOeuvre);

        if (!empty($maitriseOuvrage)) {
            $data["maitriseOuvrage"] = $maitriseOuvrage;
        }
        $html = $this->twig->fetch('pvReleased.twig', $data);

        $options = new Options();
        $options->set('isRemoteEnabled', true);

        // instantiate and use the dompdf class
        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'landscape');

        // Render the HTML as PDF
        $dompdf->render();
        $canvas = $dompdf->getCanvas();
        $canvas->page_script(function ($pageNumber, $pageCount, $canvas, $fontMetrics) {
            $pageNumberText = "Page $pageNumber sur $pageCount";
            $maitriseOeuvreText = $GLOBALS['maitriseOeuvreList'];
            $footerText = "Sauf observations formulées par mail (contact@agencecasals.fr) dans les 4 jours, le présent document est accepté sans réserves";
            $font = $fontMetrics->getFont('Segoe UI');
            $pageWidth = $canvas->get_width();
            $pageHeight = $canvas->get_height();
            $size = 10;
            $widthPageNumber = $fontMetrics->getTextWidth($pageNumberText, $font, $size);
            $widthfooter = $fontMetrics->getTextWidth($footerText, $font, $size);
            $canvas->text($pageWidth - $widthPageNumber - 20, 20, $pageNumberText, $font, $size, array(0, 0, 0, 1));
            $canvas->text(20, 20, $maitriseOeuvreText, $font, $size, array(0, 0, 0, 1));
            $canvas->text($pageWidth / 2 - $widthfooter / 2, $pageHeight - 20, $footerText, $font, $size, array(0, 0, 0, 1));
        });

        $file = $dompdf->output();

        $response
            ->withHeader('Content-Type', 'application/pdf');
        return $response->withBody($this->streamFactory->createStream($file));
    }
}
