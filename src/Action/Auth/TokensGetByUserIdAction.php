<?php

namespace App\Action\Auth;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Token\Service\TokenGetter;

final class TokensGetByUserIdAction
{
    private $tokenGetter;

    public function __construct(TokenGetter $tokenGetter)
    {
        $this->tokenGetter = $tokenGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        $userId = (int) $data['user_id'];

        // Invoke the Domain with inputs and retain the result
        $tokens = $this->tokenGetter->getTokensByUserId($userId);
        //TODO: faire cette fonction

        $result = ["Tokens" => $tokens];

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(201);
    }
}
