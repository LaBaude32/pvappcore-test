<?php

namespace App\Action\Auth;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Token\Service\TokenDeletor;

final class TokenDeleteAction
{
    private $tokenDeletor;

    public function __construct(TokenDeletor $tokenDeletor)
    {
        $this->tokenDeletor = $tokenDeletor;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        $token = (string) $data['token'];

        // Invoke the Domain with inputs and retain the result
        $this->tokenDeletor->deleteToken($token);

        $result = ["Le token à bien été supprimé"];

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(201);
    }
}
