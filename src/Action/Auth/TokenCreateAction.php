<?php

namespace App\Action\Auth;

use App\Domain\User\Service\UserAuth;
use App\Security\JwtAuth;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class TokenCreateAction
{
    private JwtAuth $jwtAuth;
    private $userAuth;

    public function __construct(JwtAuth $jwtAuth, UserAuth $userAuth)
    {
        $this->jwtAuth = $jwtAuth;
        $this->userAuth = $userAuth;
    }

    public function __invoke(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $data = (array)$request->getParsedBody();

        $email = (string)($data['email'] ?? '');
        $password = (string)($data['password'] ?? '');

        // Validate login (pseudo code)
        // Warning: This should be done in an Service and not here!
        // $userAuthData = $this->userAuth->authenticate($username, $password);
        $isValidLogin = $this->userAuth->authenticate($email, $password);

        if (!$isValidLogin) {
            // Invalid authentication credentials
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(401, 'Unauthorized');
        }

        // Create a fresh token
        $token = $this->jwtAuth->createJwt(
            [
                'uid' => $email,
            ]
        );

        // Transform the result into a OAuh 2.0 Access Token Response
        // https://www.oauth.com/oauth2-servers/access-tokens/access-token-response/
        $result = [
            'access_token' => $token,
            'token_type' => 'Bearer',
            'expires_in' => $this->jwtAuth->getLifetime(),
        ];

        // Build the HTTP response
        $response = $response->withHeader('Content-Type', 'application/json');
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(201);
    }
}
