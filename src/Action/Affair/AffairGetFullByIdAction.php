<?php

namespace App\Action\Affair;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Lot\Service\LotGetter;
use App\Domain\Affair\Service\AffairGetter;
use Fig\Http\Message\StatusCodeInterface;

/**
 * AffairGetFullByIdAction
 * 
 * @OA\Get(
 *     path="/affairs/full/affairId",
 *     tags={"Affair"},
 *     description="Récupération d'une affaire par son Id avec ses lots et leur avancement",
 *     @OA\Parameter(
 *         name="affairId",
 *         in="path",
 *         description="Id de l'affaire à récupérer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie l'affaire avec ses lots",
 *          @OA\JsonContent(
 *              type="object",
 *              @OA\Property(
 *                property="affairInfos",
 *                ref="#/components/schemas/AffairGetData"
 *              ),
 *         )
 *      )
 * )
 * 
 */
final class AffairGetFullByIdAction
{
    private $affairGetter;
    protected $lotGetter;

    public function __construct(AffairGetter $affairGetter, LotGetter $lotGetter)
    {
        $this->affairGetter = $affairGetter;
        $this->lotGetter = $lotGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        $affairId = (int) htmlspecialchars($data['affairId']);

        $result = $this->affairGetter->getFullAffairById($affairId);

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
