<?php

namespace App\Action\Affair;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Affair\Service\AffairDeletor;
use Fig\Http\Message\StatusCodeInterface;

/**
 * AffairDeleteAction 
 * 
 * @OA\Delete(
 *     path="/affairs/affairId",
 *     tags={"Affair"},
 *     description="Suppression d'une affaire",
 *     @OA\Parameter(
 *         name="affairId",
 *         in="path",
 *         description="Id de l'affaire à supprimer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="204", 
 *          description="Ne revoie rien mais l'opération c'est bien déroulé",
 *     )
 * )
 * 
 */
final class AffairDeleteAction
{
    private $affairDeletor;

    public function __construct(AffairDeletor $affairDeletor)
    {
        $this->affairDeletor = $affairDeletor;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        $id = (int) htmlspecialchars($data['affairId']);

        // Invoke the Domain with inputs and retain the result
        $this->affairDeletor->deleteAffair($id);

        // $result = ["l'affaire a bien été supprimée"];

        // // Build the HTTP response
        // $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
    }
}
