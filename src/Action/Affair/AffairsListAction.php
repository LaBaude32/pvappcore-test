<?php

namespace App\Action\Affair;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Affair\Service\AffairGetter;
use Fig\Http\Message\StatusCodeInterface;

/**
 * AffairsListAction
 * 
 * @OA\Get(
 *     path="/affairs",
 *     tags={"Affair"},
 *     description="Récupération de toutes les affaires",
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie un tableau de tous les affaires",
 *          @OA\JsonContent(
 *              type="array",
 *              @OA\Items(
 *                ref="#/components/schemas/AffairGetData"
 *              )
 *         ),
 *      )
 * )
 * 
 */
final class AffairsListAction
{
    private $affairGetter;

    public function __construct(AffairGetter $affairGetter)
    {
        $this->affairGetter = $affairGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Invoke the Domain with inputs and retain the result
        $result = $this->affairGetter->getAllAffairs();

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
