<?php

namespace App\Action\Affair;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Pv\Service\PvGetter;
use App\Domain\Affair\Service\AffairGetter;
use App\Domain\PvHasUser\Service\PvHasUserGetter;
use Fig\Http\Message\StatusCodeInterface;

/**
 * AffairsGetByUserIdAction
 * 
 * @OA\Get(
 *     path="/affairs/userId",
 *     tags={"Affair"},
 *     description="Récupération des affairs d'un utilisateur",
 *     @OA\Parameter(
 *         name="userId",
 *         in="path",
 *         description="Id de l'utilisateur",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie un tableau de tous les affaires",
 *          @OA\JsonContent(
 *              type="array",
 *              @OA\Items(
 *                ref="#/components/schemas/AffairGetData"
 *              )
 *         ),
 *      ),
 *      @OA\Response(
 *          response="204", 
 *          description="Ne renvoie rien car il n'y a pas de pvs et pas d'affaires",
 *     )
 * )
 * 
 */
final class AffairsGetByUserIdAction
{
    private $affairGetter;

    public function __construct(AffairGetter $affairGetter)
    {
        $this->affairGetter = $affairGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $params = (array) $request->getQueryParams();

        $userId = (int) htmlspecialchars($params['userId']);

        //TODO: actuelement on récupère tous les PV, pas seulements ceux dont on est owner
        $result = $this->affairGetter->getAffairsByUserId($userId);

        if (!$result) {
            return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
        }

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
