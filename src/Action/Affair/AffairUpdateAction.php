<?php

namespace App\Action\Affair;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Affair\Data\AffairGetData;
use App\Domain\Affair\Data\AffairUpdateData;
use App\Domain\Affair\Service\AffairGetter;
use App\Domain\Affair\Service\AffairUpdater;
use Fig\Http\Message\StatusCodeInterface;

/**
 * AffairUpdateAction
 * 
 * @OA\Put(
 *     path="/affairs/affairId",
 *     tags={"Affair"},
 *     description="Mise à jour d'une affaire",
 *     @OA\RequestBody(
 *         description="Données de l'affair à mettre à jour",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/AffairGetData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie l'affair",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/AffairGetData"
 *         ),
 *      )
 * )
 * 
 */
final class AffairUpdateAction
{
    private $affairUpdater;
    private $affairGetter;

    public function __construct(AffairUpdater $affairUpdater, AffairGetter $affairGetter)
    {
        $this->affairUpdater = $affairUpdater;
        $this->affairGetter = $affairGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $affair = new AffairGetData();
        $affair->affairId = htmlspecialchars($data['affairId']);
        $affair->name = htmlspecialchars($data['name']);
        $affair->address = htmlspecialchars($data['address']);
        $affair->progress = htmlspecialchars($data['progress']);
        $affair->meetingType = htmlspecialchars($data['meetingType']);
        $affair->description = htmlspecialchars($data['description']);

        // Invoke the Domain with inputs and retain the result
        $affairId = $this->affairUpdater->updateAffair($affair);

        $newAffair = $this->affairGetter->getAffairById($affairId);

        $result = $newAffair;
        
        // Transform the result into the JSON representation
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));
        
        // Build the HTTP response
        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
