<?php

namespace App\Action\Affair;

use App\Domain\Affair\Data\AffairCreateData;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Affair\Service\AffairCreator;
use Fig\Http\Message\StatusCodeInterface;

/**
 * AffairCreateAction
 * 
 * @OA\Post(
 *     path="/affairs",
 *     tags={"Affair"},
 *     description="Ajout d'une affaire",
 *     @OA\RequestBody(
 *         description="Données de l'affaire à ajouter",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/AffairCreateData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="201", 
 *          description="Renvoie l'Id de la nouvelle affaire",
 *          @OA\JsonContent(
 *               type="object",
 *               @OA\Property(
 *                      type="integer",
 *                      property="affairId"
 *               )
 *           )
 *     )
 * )
 *
 */
final class AffairCreateAction
{
    private $affairCreator;

    public function __construct(AffairCreator $affairCreator)
    {
        $this->affairCreator = $affairCreator;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $affair = new AffairCreateData();
        $affair->name = (string) htmlspecialchars($data['name']);
        $affair->address = (string) htmlspecialchars($data['address']);
        $affair->progress = array_key_exists('progress', $data) ? htmlspecialchars($data['progress']): null;
        $affair->meetingType = (string) htmlspecialchars($data['meetingType']);
        $affair->description = array_key_exists('description', $data) ? htmlspecialchars($data['description']) : null;

        // Invoke the Domain with inputs and retain the result
        $affairId = $this->affairCreator->createAffair($affair);

        // Transform the result into the JSON representation
        $result = [
            'affairId' => $affairId
        ];

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}
