<?php

namespace App\Action\Item;

use DateTime;
use Symfony\Component\Uid\Uuid;
use App\Domain\Lot\Service\LotCreator;
use App\Domain\Item\Service\ItemGetter;
use Psr\Http\Message\ResponseInterface;
use App\Domain\Item\Data\ItemCreateData;
use App\Domain\Item\Service\ItemCreator;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * ItemCreateAction
 * 
 * @OA\Post(
 *     path="/items",
 *     tags={"Item"},
 *     description="Ajout d'un item",
 *     @OA\RequestBody(
 *         description="Données du item à ajouter",
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/ItemCreateData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="201", 
 *          description="Renvoie l'Id du nouveau item",
 *          @OA\JsonContent(
 *               type="object",
 *               ref="#/components/schemas/ItemGetData"
 *           )
 *     )
 * )
 *
 */
final class ItemCreateAction
{
    private $itemCreator;
    private $lotCreator;
    private $itemGetter;
    private string $storageDirectory = __DIR__ . "/../../../public/images";

    public function __construct(ItemCreator $itemCreator, LotCreator $lotCreator, ItemGetter $itemGetter)
    {
        $this->itemCreator = $itemCreator;
        $this->lotCreator = $lotCreator;
        $this->itemGetter = $itemGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $item = new ItemCreateData();
        $item->position = htmlspecialchars($data['position']);
        $item->note = ($data['note'] != "" && $data['note'] != "null") ? htmlspecialchars($data['note']) : null;
        $item->followUp = ($data['followUp'] != "" && $data['followUp'] != "null") ? htmlspecialchars($data['followUp']) : null;
        $item->resources = ($data['resources'] != "" && $data['resources'] != "null") ? htmlspecialchars($data['resources']) : null;
        $item->completion = (array_key_exists('completion', $data) && $data['completion'] != "" && $data['completion'] != "undefined") ? htmlspecialchars($data['completion']) : null;
        $item->completionDate = ($data['completionDate'] != "" && $data['completionDate'] != "null") ? htmlspecialchars($data['completionDate']) : null;
        $item->visible = (int) htmlspecialchars($data['visible']);
        $item->pvId = (int) htmlspecialchars($data['pvId']);
        if (gettype($data['lots']) == "array" && !empty($data['lots'])) {
            $item->lotsIds = $data['lots'];
        } elseif (gettype($data['lots']) == "string" && $data['lots'] != "") {
            $item->lotsIds = explode(',', $data['lots']);
        } else {
            $item->lotsIds = null;
        }

        $uploadedFiles = $request->getUploadedFiles();

        foreach ($uploadedFiles as $uploadedFile) {
            // $directory = $_SERVER["DOCUMENT_ROOT"] . "/images/";
            $filename =  $this->moveUploadedFile(realpath($this->storageDirectory), $uploadedFile);
            $item->image = (string) $filename;
            $item->thumbnail = null;
        }

        // Invoke the Domain with inputs and retain the result
        $itemId = $this->itemCreator->createItem($item);

        if (!empty($item->lotsIds)) {
            $this->lotCreator->linkLotsToItem($item->lotsIds, $itemId);
        }
        $item = $this->itemGetter->getItemById($itemId);

        // Transform the result into the JSON representation
        $result = $item;

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_CREATED);
    }

    /**
     * Moves the uploaded file to the upload directory and assigns it a unique name
     * to avoid overwriting an existing uploaded file.
     *
     * @param string $directory The directory to which the file is moved
     * @param UploadedFileInterface $uploadedFile The file uploaded file to move
     *
     * @return string The filename of moved file
     */
    private function moveUploadedFile(
        string $directory,
        UploadedFileInterface $uploadedFile
    ): string {
        $extension = (string)pathinfo(
            $uploadedFile->getClientFilename(),
            PATHINFO_EXTENSION
        );

        // Create unique id for this file
        $filename = sprintf('%s.%s', (string)Uuid::v4(), $extension);

        // Save the file into the storage
        $targetPath = sprintf('%s/%s', $directory, $filename);
        $uploadedFile->moveTo($targetPath);

        return $filename;
    }
}
