<?php

namespace App\Action\Item;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Item\Service\ItemDeletor;
use Fig\Http\Message\StatusCodeInterface;
use Nyholm\Psr7\ServerRequest;

/**
 * ItemDeleteAction 
 * 
 * @OA\Delete(
 *     path="/items/itemId",
 *     tags={"Item"},
 *     description="Suppression d'un item",
 *     @OA\Parameter(
 *         name="itemId",
 *         in="path",
 *         description="Id de l'item à supprimer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="pvId",
 *         in="path",
 *         description="Id du pv où on supprime le PV",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="204", 
 *          description="Ne revoie rien mais l'opération c'est bien déroulé",
 *     )
 * )
 * 
 */
final class ItemDeleteAction
{
    private $itemDeletor;

    public function __construct(ItemDeletor $itemDeletor)
    {
        $this->itemDeletor = $itemDeletor;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        $itemId = (int) htmlspecialchars($data['itemId']);
        $pvId = (int) htmlspecialchars($data['pvId']);

        $itemHasPv['itemId'] = (int) htmlspecialchars($data['itemId']);
        $itemHasPv['pvId'] = (int) htmlspecialchars($data['pvId']);

        $this->itemDeletor->deleteItemHasPv($itemHasPv);

        return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
    }
}
