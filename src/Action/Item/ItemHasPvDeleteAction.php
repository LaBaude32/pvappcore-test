<?php

namespace App\Action\Item;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use App\Domain\Item\Service\ItemDeletor;
use Fig\Http\Message\StatusCodeInterface;

/**
 * ItemUpdateVisibleAction
 * 
 * @OA\Delete(
 *     path="/itemHasPv",
 *     tags={"Item"},
 *     description="Supprimer un item d'un Pv",
 *     @OA\Parameter(
 *         name="itemId",
 *         in="path",
 *         description="Id de l'item à supprimer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="pvId",
 *         in="path",
 *         description="Id du pv associé",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="204", 
 *          description="Ne revoie rien mais l'opération c'est bien déroulé",
 *     )
 * )
 */
final class ItemHasPvDeleteAction
{
    private $itemDeletor;

    public function __construct(ItemDeletor $itemDeletor)
    {
        $this->itemDeletor = $itemDeletor;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        $itemHasPv['itemId'] = (int) htmlspecialchars($data['itemId']);
        $itemHasPv['pvid'] = (int) htmlspecialchars($data['pvId']);

        // Invoke the Domain with inputs and retain the result
        $this->itemDeletor->deleteItemHasPv($itemHasPv);

        // $result = "success";

        // // Build the HTTP response
        // $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
    }
}
