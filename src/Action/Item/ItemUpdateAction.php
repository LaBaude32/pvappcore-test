<?php

namespace App\Action\Item;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use UnexpectedValueException;
use App\Domain\Item\Data\ItemGetData;
use App\Domain\Item\Service\ItemGetter;
use App\Domain\Item\Service\ItemDeletor;
use App\Domain\Item\Service\ItemUpdater;
use App\Domain\Lot\Service\LotCreator;
use Fig\Http\Message\StatusCodeInterface;

/**
 * ItemUpdateAction
 * 
 * @OA\Put(
 *     path="/items/itemId",
 *     tags={"Item"},
 *     description="Mise à jour d'un item",
 *     @OA\RequestBody(
 *         description="Données de l'item à mettre à jour",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/ItemGetData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie l'item",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/ItemGetData"
 *         ),
 *      )
 * )
 * 
 */
final class ItemUpdateAction
{
    private $itemUpdater;
    protected $itemGetter;
    protected $itemDeletor;
    protected $lotCreator;

    public function __construct(ItemUpdater $itemUpdater, ItemGetter $itemGetter, ItemDeletor $itemDeletor, LotCreator $lotCreator)
    {
        $this->itemUpdater = $itemUpdater;
        $this->itemGetter = $itemGetter;
        $this->itemDeletor = $itemDeletor;
        $this->lotCreator = $lotCreator;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $item = new ItemGetData();
        $item->itemId = (int) htmlspecialchars($data['itemId']);
        $item->position = (int) htmlspecialchars($data['position']);
        $item->note = ($data['note'] != "" && $data['note'] != "null") ? htmlspecialchars($data['note']) : null;
        $item->followUp = ($data['followUp'] != "" && $data['followUp'] != "null") ? htmlspecialchars($data['followUp']) : null;
        $item->resources = ($data['resources'] != "" && $data['resources'] != "null") ? htmlspecialchars($data['resources']) : null;
        $item->completion = (array_key_exists('completion', $data) && $data['completion'] != "" && $data['completion'] != "undefined") ? htmlspecialchars($data['completion']) : null;
        $item->completionDate = ($data['completionDate'] != "" && $data['completionDate'] != "null") ? htmlspecialchars($data['completionDate']) : null;
        $item->visible = ($data['visible'] == "true" || $data['visible'] == 1) ? 1 : 0;
        $item->image = array_key_exists('image', $data) ? $data['image'] : null;
        $item->thumbnail = array_key_exists('thumbnail', $data) ? $data['thumbnail'] : null;
        if (gettype($data['lots']) == "array") {
            $item->lotsIds = $data['lots'];
        } elseif (gettype($data['lots']) == "string" && $data['lots'] != "") {
            $item->lotsIds = explode(',', $data['lots']);
        } else {
            $item->lotsIds = null;
        }

        //FIXME: Supprimer vraiment les images du server à la mise à jour si supprimer.
        //TODO: corriger les variable null qui sont en texte

        // Invoke the Domain with inputs and retain the result
        $this->itemUpdater->updateItem($item);

        $newItem = $this->itemGetter->getItemById($item->itemId);

        //FIXME: A voir s'il faut refaire ça, mais avec les htmlspecialchars ça pose des problèmes
        // foreach ($newItem as $key => $value) {
        //     if ($item->$key !== $value && $key != "completionDate" && $key != "lotsIds" && $key != "lots" && $key != "createdAt") {
        //         $oldValue = $item->$key;
        //         throw new UnexpectedValueException("$key est different.
        //         API : $oldValue - new value Valeur : $value ");
        //     }
        // }

        // Recupéré les lots éxistants
        $itemWithLots = $this->itemGetter->getLotsForItem($newItem);

        // Les supprimer
        if (!empty($itemWithLots->lots)) {
            $this->itemDeletor->deleteItemHasLot($itemWithLots);
        }

        // Ajouter les nouveaux
        if (!empty($item->lotsIds)) {
            $this->lotCreator->linkLotsToItem($item->lotsIds, $newItem->itemId);
        }

        //Récupéré tout l'item avec les nouveaux lots
        $itemToSend = $this->itemGetter->getItemById($newItem->itemId);

        // Transform the result into the JSON representation
        $result = $itemToSend;

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
