<?php

namespace App\Action\Item;

use Symfony\Component\Uid\Uuid;
use App\Domain\Item\Data\ItemGetData;
use App\Domain\Item\Service\ItemGetter;
use Psr\Http\Message\ResponseInterface;
use App\Domain\Item\Service\ItemUpdater;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ServerRequestInterface;


/**
 * ImageUploadAction
 * 
 * @OA\Post(
 *     path="/items/uploadImage",
 *     tags={"Item"},
 *     description="Upload d'une image d'un item existant",
 *     @OA\RequestBody(
 *         @OA\MediaType(
 *             mediaType="multipart/form-data",
 *             @OA\Schema(
 *                  @OA\Property(
 *                      property="itemId",
 *                      type="integer"
 *                  ),
 *                  @OA\Property(
 *                      property="image",
 *                      description="image à upload",
 *                      format="binary"
 *                  ),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="201", 
 *          description="Renvoie l'item",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/ItemGetData"
 *         ),
 *      )
 * )
 * 
 */
final class ImageUploadAction
{
    private $itemUpdater;
    protected $itemGetter;
    private string $storageDirectory = __DIR__ . "/../../../public/images";

    public function __construct(ItemUpdater $itemUpdater, ItemGetter $itemGetter)
    {
        $this->itemUpdater = $itemUpdater;
        $this->itemGetter = $itemGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $item = new ItemGetData();
        $item->itemId = (int) htmlspecialchars($data['itemId']);

        $uploadedFiles = $request->getUploadedFiles();

        foreach ($uploadedFiles as $uploadedFile) {
            // $directory = $_SERVER["DOCUMENT_ROOT"] . "/images/";
            $filename =  $this->moveUploadedFile(realpath($this->storageDirectory), $uploadedFile);
            $item->image = (string) $filename;
            // $item->thumbnail = null;
        }

        // Invoke the Domain with inputs and retain the result
        $this->itemUpdater->updateImage($item);

        $newItem = $this->itemGetter->getItemById($item->itemId);

        $result = $newItem;

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_CREATED);
    }

    /**
     * TODO:Code dupliqué avec ItemCreateAction --> A mettre dans un fichier de fonctions
     * Moves the uploaded file to the upload directory and assigns it a unique name
     * to avoid overwriting an existing uploaded file.
     *
     * @param string $directory The directory to which the file is moved
     * @param UploadedFileInterface $uploadedFile The file uploaded file to move
     *
     * @return string The filename of moved file
     */
    private function moveUploadedFile(
        string $directory,
        UploadedFileInterface $uploadedFile
    ): string {
        $extension = (string)pathinfo(
            $uploadedFile->getClientFilename(),
            PATHINFO_EXTENSION
        );

        // Create unique id for this file
        $filename = sprintf('%s.%s', (string)Uuid::v4(), $extension);

        // Save the file into the storage
        $targetPath = sprintf('%s/%s', $directory, $filename);
        $uploadedFile->moveTo($targetPath);

        return $filename;
    }
}
