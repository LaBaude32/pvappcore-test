<?php

namespace App\Action\Item;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use UnexpectedValueException;
use App\Domain\Item\Data\ItemGetData;
use App\Domain\Item\Service\ItemGetter;
use App\Domain\Item\Data\ItemCreateData;
use App\Domain\Item\Service\ItemUpdater;
use Fig\Http\Message\StatusCodeInterface;
use Twig\Extension\StagingExtension;

/**
 * ItemUpdateVisibleAction
 * 
 * @OA\Put(
 *     path="/items/itemId/visibility",
 *     tags={"Item"},
 *     description="Mise à jour d'un item",
 *     @OA\RequestBody(
 *         description="Données de l'item à mettre à jour",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 @OA\Property(
 *                      property="itemId",
 *                      type="integer"
 *                  ),
 *                  @OA\Property(
 *                      property="visible",
 *                      type="boolean"
 *                  ),
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie l'item",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/ItemGetData"
 *         ),
 *      )
 * )
 * TODO: corriger la réponse (les lots n'y sont pas)
 */
final class ItemUpdateVisibleAction
{
    private $itemUpdater;

    protected $itemGetter;

    public function __construct(ItemUpdater $itemUpdater, ItemGetter $itemGetter)
    {
        $this->itemUpdater = $itemUpdater;
        $this->itemGetter = $itemGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $item = new ItemGetData();
        $item->itemId = (int) htmlspecialchars($data['itemId']);
        $item->visible = (int) htmlspecialchars($data['visible']);

        // Invoke the Domain with inputs and retain the result
        $this->itemUpdater->updateVisible($item);

        $newItem = $this->itemGetter->getItemById($item->itemId);


        if ($newItem->visible !== $item->visible) {
            throw new UnexpectedValueException('Erreur, la valeur est différente');
        }

        // Transform the result into the JSON representation
        $result = $newItem;

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
