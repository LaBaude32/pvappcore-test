<?php

namespace App\Action\User;

use UnexpectedValueException;
use App\Domain\User\Data\UserGetData;
use App\Domain\User\Service\UserGetter;
use App\Domain\User\Service\UserUpdater;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Twig\Extension\StagingExtension;

/**
 * UserUpdateAction
 * 
 * @OA\Put(
 *     path="/users/userId",
 *     tags={"User"},
 *     description="Mise à jour d'un utilisateur",
 *     @OA\RequestBody(
 *         description="Données de l'utilisateur à mettre à jour",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/UserGetData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie l'utilisateur",
 *          @OA\JsonContent(
 *              type="object",
 *              ref="#/components/schemas/UserGetData"
 *         ),
 *      )
 * )
 * 
 */
final class UserUpdateAction
{
    private $userUpdater;

    protected $userGetter;

    public function __construct(UserUpdater $userUpdater, UserGetter $userGetter)
    {
        $this->userUpdater = $userUpdater;
        $this->userGetter = $userGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $user = new UserGetData();
        $user->userId = htmlspecialchars($data['userId']);
        $user->email = htmlspecialchars($data['email']);
        $user->password = htmlspecialchars($data['password']);
        $user->firstName = htmlspecialchars($data['firstName']);
        $user->lastName = htmlspecialchars($data['lastName']);
        $user->phone = htmlspecialchars($data['phone']);
        $user->userGroup = htmlspecialchars($data['userGroup']);
        $user->userFunction = htmlspecialchars($data['userFunction']);
        $user->organism = htmlspecialchars($data['organism']);

        // Invoke the Domain with inputs and retain the result
        $this->userUpdater->updateUser($user);

        $newUser = $this->userGetter->getUserById($user->userId);

        foreach ($newUser as $key => $value) {
            if ($user->$key !== $value) {
                throw new UnexpectedValueException('Erreur sur le ' . $key . ' qui est différent');
            }
        }

        // Transform the result into the JSON representation
        $result = $newUser;

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
