<?php

namespace App\Action\User;

use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Service\PvHasUserCreator;
use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Service\UserCreator;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * UserCreateAction
 * 
 * @OA\Post(
 *     path="/users",
 *     tags={"User"},
 *     description="Ajout d'un user",
 *     @OA\RequestBody(
 *         description="Données du user à ajouter",
 *         @OA\MediaType(
 *             mediaType="application/json",
 *             @OA\Schema(
 *                 type="object",
 *                 ref="#/components/schemas/UserCreateData"
 *             )
 *         )
 *     ),
 *     @OA\Response(
 *          response="201", 
 *          description="Renvoie l'Id du nouveau user",
 *          @OA\JsonContent(
 *               type="object",
 *               @OA\Property(
 *                      type="integer",
 *                      property="userId"
 *               )
 *           )
 *     )
 * )
 *
 */

final class UserCreateAction
{
    private $userCreator;

    protected $pvHasUserCreator;

    public function __construct(UserCreator $userCreator, PvHasUserCreator $pvHasUserCreator)
    {
        $this->userCreator = $userCreator;
        $this->pvHasUserCreator = $pvHasUserCreator;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getParsedBody();

        // Mapping (should be done in a mapper class)
        $user = new UserCreateData();
        $user->email = htmlspecialchars($data['email']);
        //TODO: renvoyer une erreur sur le mdp est vide
        $user->password = password_hash(htmlspecialchars($data['password']), PASSWORD_DEFAULT);
        $user->firstName = htmlspecialchars($data['firstName']);
        $user->lastName = htmlspecialchars($data['lastName']);
        $user->phone = htmlspecialchars($data['phone']);
        $user->userGroup = array_key_exists('userGroup', $data) ? htmlspecialchars($data['userGroup']) : null;
        $user->userFunction = array_key_exists('userFunction', $data) ? htmlspecialchars($data['userFunction']) : null;
        $user->organism = array_key_exists('organism', $data) ? htmlspecialchars($data['organism']) : null;

        // Invoke the Domain with inputs and retain the result
        $userId = $this->userCreator->createUser($user);

        if (array_key_exists('pvId', $data) && $data['pvId'] != "") {
            $pvHasUser = new PvHasUserData();
            $pvHasUser->pvId = htmlspecialchars($data['pvId']);
            $pvHasUser->userId = $userId;
            $pvHasUser->statusPAE = array_key_exists('statusPAE', $data) && !is_null($data['statusPAE']) ? htmlspecialchars($data['statusPAE']) : NULL;
            //TODO: verifier que c'est la bonne technique. Pourquoi il ne defini pas à 0 quand c'est null?
            $pvHasUser->owner = 0;

            $this->pvHasUserCreator->createPvHasUser($pvHasUser);
        }

        // Transform the result into the JSON representation
        $result = [
            'userId' => $userId
        ];

        // Build the HTTP response
        $response->getBody()->write(json_encode($result));

        return $response->withHeader('Content-Type', 'application/json')->withStatus(StatusCodeInterface::STATUS_CREATED);
    }
}
