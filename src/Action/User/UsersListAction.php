<?php

namespace App\Action\User;

use App\Domain\User\Service\UserGetter;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * UsersListAction
 * 
 * @OA\Get(
 *     path="/users",
 *     tags={"User"},
 *     description="Récupération de tous les utilisateurs",
 *     @OA\Response(
 *          response="200", 
 *          description="Renvoie un tableau de tous les utilisateurs",
 *          @OA\JsonContent(
 *              type="array",
 *              @OA\Items(
 *                ref="#/components/schemas/UserGetData"
 *              )
 *         ),
 *      )
 * )
 * 
 */
final class UsersListAction
{
    private $UserGetter;

    public function __construct(UserGetter $UserGetter)
    {
        $this->UserGetter = $UserGetter;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Invoke the Domain with inputs and retain the result
        $users = $this->UserGetter->getUsers();

        // Build the HTTP response
        $response->getBody()->write((string)json_encode($users, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_OK);
    }
}
