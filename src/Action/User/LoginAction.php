<?php

namespace App\Action\User;

use App\Domain\User\Service\UserAuth;
use App\Domain\User\Service\UserGetter;
use Psr\Http\Message\ResponseInterface;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * LoginAction
 * 
 * @OA\Get(
 *     path="/login",
 *     tags={"User"},
 *     description="Login",
 *     @OA\Parameter(
 *         name="email",
 *         in="path",
 *         description="email de l'user",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Parameter(
 *         name="password",
 *         in="path",
 *         description="mot de passe",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         )
 *     ),
 *     @OA\Response(
 *          response="202", 
 *          description="Identification validé, renvoie des infos de l'utilisateur",
 *          @OA\JsonContent(
 *              ref="#/components/schemas/UserGetData"
 *         ),
 *      ),
 *      @OA\Response(
 *          response="401", 
 *          description="Identifiants incorrectes",
 *      )
 * )
 * 
 */
final class LoginAction
{
    private $userGetter;
    private $userAuth;

    public function __construct(UserGetter $userGetter, UserAuth $userAuth)
    {
        $this->userGetter = $userGetter;
        $this->userAuth = $userAuth;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        // Mapping (should be done in a mapper class)

        $email = (string)($data['email'] ?? '');
        $password = (string)($data['password'] ?? '');

        $isValidLogin = $this->userAuth->authenticate($email, $password);

        if (!$isValidLogin) {
            // Invalid authentication credentials
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(StatusCodeInterface::STATUS_UNAUTHORIZED);
        }

        // Invoke the Domain with inputs and retain the result
        $user = $this->userGetter->identifyUser($email);
        $user->password = null;

        // Transform the result into the JSON representation
        $result = $user;

        $response->getBody()->write(json_encode($result));

        // Build the HTTP response

        return $response->withHeader('Content-Type', 'application/json')->withStatus(StatusCodeInterface::STATUS_ACCEPTED);
    }
}
