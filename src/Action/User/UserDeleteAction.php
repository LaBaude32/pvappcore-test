<?php

namespace App\Action\User;

use App\Domain\User\Service\UserDeletor;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * UserDeleteAction 
 * 
 * @OA\Delete(
 *     path="/users/userId",
 *     tags={"User"},
 *     description="Suppression d'un utilisateur",
 *     @OA\Parameter(
 *         name="userId",
 *         in="path",
 *         description="Id de l'utilisateur à supprimer",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int"
 *         )
 *     ),
 *     @OA\Response(
 *          response="204", 
 *          description="Ne revoie rien mais l'opération c'est bien déroulé",
 *     )
 * )
 * 
 */
final class UserDeleteAction
{
    private $userDeletor;

    public function __construct(UserDeletor $userDeletor)
    {
        $this->userDeletor = $userDeletor;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        // Collect input from the HTTP request
        $data = (array) $request->getQueryParams();

        $id = (int) htmlspecialchars($data['userId']);

        // Invoke the Domain with inputs and retain the result
        $this->userDeletor->deleteUser($id);

        // $result = "success";

        // // Build the HTTP response
        // $response->getBody()->write((string)json_encode($result, JSON_THROW_ON_ERROR));

        return $response->withStatus(StatusCodeInterface::STATUS_NO_CONTENT);
    }
}
