<?php

namespace App\Domain\PvHasUser\Service;

use UnexpectedValueException;
use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Data\PvHasUserCreateData;
use App\Domain\PvHasUser\Repository\PvHasUserCreatorRepository;
use App\Domain\PvHasUser\Repository\PvHasUserGetterRepository;

/**
 * Service.
 */
final class PvHasUserCreator
{
    private $getterRepository;

    /**
     * @var PvHasUserCreatorRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param PvHasUserCreatorRepository $repository The repository
     */
    public function __construct(PvHasUserCreatorRepository $repository, PvHasUserGetterRepository $getterRepository)
    {
        $this->repository = $repository;
        $this->getterRepository = $getterRepository;
    }

    /**
     * Create a new Affair.
     *
     * @param PvHasUserCreateData $Affair The Affair data
     *
     * @return PvHasUserData The new participant with his status
     */
    public function createPvHasUser(PvHasUserData $pvHasUser): PvHasUserData
    {
        // Validation
        if (empty($pvHasUser->pvId)) {
            throw new UnexpectedValueException('pvId required');
        }

        if (empty($pvHasUser->userId)) {
            throw new UnexpectedValueException('userId required');
        }

        // Insert pvHasUser
        // $pvHasUserId = $this->repository->insertPvHasUser($pvHasUser);
        $this->repository->insertPvHasUser($pvHasUser);
        $newParticipant = $this->getterRepository->getParticipantStatus($pvHasUser->userId, $pvHasUser->pvId);

        return $newParticipant;
    }

    public function addUsersToNewPv(array $data)
    {
        if (empty($data)) {
            throw new UnexpectedValueException('data required');
        }

        $this->repository->insertPvHasUserToNewPv($data);
    }
}
