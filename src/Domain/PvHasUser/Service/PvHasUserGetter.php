<?php

namespace App\Domain\PvHasUser\Service;

use UnexpectedValueException;
use App\Domain\Pv\Data\PvGetData;
use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Repository\PvHasUserGetterRepository;
use App\Domain\User\Data\UserGetData;
use App\Domain\User\Repository\UserGetterRepository;

/**
 * Service.
 */
final class PvHasUserGetter
{
    /**
     * @var PvHasUserGetterRepository
     */
    private $repository;

    protected $userRepository;

    /**
     * The constructor.
     *
     * @param PvHasUserGetterRepository $repository The repository
     */
    public function __construct(PvHasUserGetterRepository $repository, UserGetterRepository $userRepository)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
    }

    public function getPvByUserId(int $userId): array|null
    {
        // Validation
        if (empty($userId)) {
            throw new UnexpectedValueException('id required');
        }

        if ($userId == 0) {
            throw new UnexpectedValueException('id doit être positif');
        }

        $pvs = $this->repository->getAllPvIdByUserId($userId);

        //Supprime les doublons dans le tableau
        $result = $pvs ? array_unique($pvs) : null;

        return $result;
    }

    public function getPvOwner(int $pvId): UserGetData
    {
        $owner = $this->repository->getPvOwner($pvId);

        return $owner;
    }

    public function getPvHasUsers(PvGetData $pv): array
    {
        $pvHasUsers = $this->repository->getAllPvHasUser($pv->pvId);

        return $pvHasUsers;
    }

    /**
     * [Description for getPvHasUsersFromPvs]
     *
     * @param array[PvGetData] $pvs
     * 
     * @return array
     * 
     */
    public function getPvHasUsersFromPvs(array $pvs): array
    {
        foreach ($pvs as $pv) {
            $allPHU = $this->repository->getAllPvHasUser($pv->pvId);
            foreach ($allPHU as $pHU) {
                $users_ids[] = $pHU->userId;
            }
        }
        foreach ($users_ids as $user) {
            $allParticipants[] = $this->userRepository->getUserWithStatusById($user);
        }

        return (array) $allParticipants;
    }

    /**
     * Undocumented function
     *
     * @param integer $pvId
     * @return array(PvHasUserData)
     */
    public function getParticipantsForPv(int $pvId)
    {
        $participants = $this->repository->getParticipantsByPvId($pvId);

        return $participants;
    }

    public function getAllConnectedParticipants(int $userId): array|null
    {
        // Validation
        if (empty($userId)) {
            throw new UnexpectedValueException('id required');
        }

        $participants = $this->repository->getAllConnectedParticipants($userId);

        return $participants;
    }

    public function getParticipantStatus(int $userId, int $pvId): PvHasUserData
    {
        // Validation
        if (empty($userId)) {
            throw new UnexpectedValueException('userid required');
        }
        // Validation
        if (empty($pvId)) {
            throw new UnexpectedValueException('pvid required');
        }

        $participant = $this->repository->getParticipantStatus($userId, $pvId);

        return $participant;
    }
}
