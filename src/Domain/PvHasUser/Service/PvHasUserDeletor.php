<?php

namespace App\Domain\PvHasUser\Service;

use UnexpectedValueException;
use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Data\PvHasUserCreateData;
use App\Domain\PvHasUser\Repository\PvHasUserDeletorRepository;

/**
 * Service.
 */
final class PvHasUserDeletor
{
    /**
     * @var PvHasUserDeletorRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param PvHasUserDeletorRepository $repository The repository
     */
    public function __construct(PvHasUserDeletorRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create a new Affair.
     *
     * @param PvHasUserCreateData $Affair The Affair data
     *
     * @return int The new Affair ID
     */
    public function deletePvHasUser(array $data)
    {
        // var_dump($data);
        // Validation
        if (empty($data['pvId'])) {
            throw new UnexpectedValueException('pvId required');
        }

        if (empty($data['userId'])) {
            throw new UnexpectedValueException('userId required');
        }

        $this->repository->deletePvHasUser($data);
    }
}
