<?php

namespace App\Domain\PvHasUser\Service;

use DateTime;
use DateInterval;
use UnexpectedValueException;
use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Data\ParticipantOTPData;
use App\Domain\PvHasUser\Data\PvHasUserCreateData;

use App\Domain\PvHasUser\Repository\ParticipantOTPGetterRepository;
use App\Domain\PvHasUser\Repository\ParticipantOTPCreatorRepository;
use App\Domain\PvHasUser\Repository\ParticipantOTPDeletorRepository;

/**
 * Service.
 */
final class ParticipantOTPCreator
{
    /**
     * @var ParticipantOTPDeletorRepository
     */
    private $deletorRepository;

    /**
     * @var ParticipantOTPGetterRepository
     */
    private $getterRepository;

    /**
     * @var ParticipantOTPCreatorRepository
     */
    private $creatorRepository;

    /**
     * The constructor.
     *
     * @param ParticipantOTPCreatorRepository
     * @param ParticipantOTPGetterRepository
     * @param ParticipantOTPDeletorRepository
     */
    public function __construct(ParticipantOTPCreatorRepository $creatorRepository, ParticipantOTPGetterRepository $getterRepository, ParticipantOTPDeletorRepository $deletorRepository)
    {
        $this->creatorRepository = $creatorRepository;
        $this->getterRepository = $getterRepository;
        $this->deletorRepository = $deletorRepository;
    }

    /**
     * Create a new Affair.
     *
     * @param PvHasUserCreateData $Affair The Affair data
     *
     * @return PvHasUserData The new participant with his status
     */
    public function createParticiantOTP(ParticipantOTPData $participantOTP): ParticipantOTPData
    {
        // Validation
        if (empty($participantOTP->pvId)) {
            throw new UnexpectedValueException('pvId required');
        }

        if (empty($participantOTP->otp)) {
            throw new UnexpectedValueException('otp required');
        }

        $isAlreadyOTP =
            $this->getterRepository->getParticipantOTPByPvId($participantOTP->pvId);

        if ($isAlreadyOTP) {
            if ($isAlreadyOTP->createdAt->add(new DateInterval('PT10M')) > new DateTime()) {
                return $isAlreadyOTP;
            } else {
                $this->deletorRepository->deleteParticipantOTPByPvId($isAlreadyOTP->pvId);
            }
        }

        $this->creatorRepository->createParticipantOTP($participantOTP);

        $newParticipantOTP = $this->getterRepository->getParticipantOTPByPvId($participantOTP->pvId);

        return $newParticipantOTP;
    }
}
