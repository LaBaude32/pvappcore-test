<?php

namespace App\Domain\PvHasUser\Service;

use DateTime;
use DateInterval;
use UnexpectedValueException;
use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Data\ParticipantOTPData;
use App\Domain\PvHasUser\Data\PvHasUserCreateData;

use App\Domain\PvHasUser\Repository\ParticipantOTPGetterRepository;
use App\Domain\PvHasUser\Repository\ParticipantOTPCreatorRepository;

/**
 * Service.
 */
final class ParticipantOTPValidator
{
    /**
     * @var ParticipantOTPGetterRepository
     */
    private $getterRepository;

    /**
     * The constructor.
     *
     * @param ParticipantOTPGetterRepository
     */
    public function __construct(ParticipantOTPGetterRepository $getterRepository)
    {
        $this->getterRepository = $getterRepository;
    }

    /**
     * Validate OTP for participant.
     *
     * @param ParticipantOTPData OTP
     *
     * @return bool OTP validate or not
     */
    public function validateParticiantOTP(ParticipantOTPData $participantOTP): bool
    {
        // Validation
        if (empty($participantOTP->pvId)) {
            throw new UnexpectedValueException('pvId required');
        }

        if (empty($participantOTP->otp)) {
            throw new UnexpectedValueException('otp required');
        }

        $storedOTP = $this->getterRepository->getParticipantOTPByPvId($participantOTP->pvId);

        $valid = $storedOTP->otp == $participantOTP->otp && $storedOTP->createdAt->add(new DateInterval('PT10M')) > new DateTime();

        return $valid;
    }
}
