<?php

namespace App\Domain\PvHasUser\Service;

use UnexpectedValueException;
use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Data\PvHasUserCreateData;
use App\Domain\PvHasUser\Repository\PvHasUserUpdaterRepository;

/**
 * Service.
 */
final class PvHasUserUpdater
{
    /**
     * @var PvHasUserUpdaterRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param PvHasUserUpdaterRepository $repository The repository
     */
    public function __construct(PvHasUserUpdaterRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Update a Status of user for a PV.
     *
     * @param PvHasUserData
     * @return void
     */
    public function updatePvHasUser(PvHasUserData $pvHasUser): void
    {
        // Validation
        if (empty($pvHasUser->pvId)) {
            throw new UnexpectedValueException('pvId required');
        }

        if (empty($pvHasUser->userId)) {
            throw new UnexpectedValueException('userId required');
        }

        $this->repository->updatePvHasUser($pvHasUser);
    }
}
