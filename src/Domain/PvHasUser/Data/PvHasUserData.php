<?php

namespace App\Domain\PvHasUser\Data;

use App\Domain\User\Data\UserCreateData;

/**
 * @OA\Schema()
 * 
 * PvHasUserData
 */
final class PvHasUserData extends UserCreateData
{
    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $pvId;

    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $userId;

    /** 
     * @OA\Property(enum={"Présent","Absent","Excusé"})
     * 
     * @var string|null
     */
    public $statusPAE;

    /** 
     * @OA\Property()
     * 
     * @var bool|null
     */
    public $invitedCurrentMeeting;

    /** 
     * @OA\Property()
     * 
     * @var bool|null
     */
    public $invitedNextMeeting;

    /** 
     * @OA\Property()
     * 
     * @var bool|null
     */
    public $distribution;

    /** 
     * @OA\Property()
     * 
     * @var bool
     */
    public $owner;
}
