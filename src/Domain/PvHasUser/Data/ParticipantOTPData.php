<?php

namespace App\Domain\PvHasUser\Data;

/**
 * @OA\Schema()
 * 
 * ParticipantOTPData
 */
final class ParticipantOTPData
{
    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $pvId;

    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $otp;

    /** 
     * @OA\Property(title="date de création", format="date")
     * 
     * @var \DateTime
     */
    public $createdAt;

    /** 
     * @OA\Property(title="date de création", format="date")
     * 
     * @var \DateTime
     */
    public $createdAtTimeStamp;
}
