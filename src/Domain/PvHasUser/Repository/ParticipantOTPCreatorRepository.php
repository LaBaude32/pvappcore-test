<?php

namespace App\Domain\PvHasUser\Repository;

use App\Domain\PvHasUser\Data\ParticipantOTPData;
use PDO;

/**
 * Repository.
 */
class ParticipantOTPCreatorRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Insert lot row.
     *
     * @param PvHasUserCreateData $lot The affaire
     *
     * @return int The new ID
     */
    public function createParticipantOTP(ParticipantOTPData $participantOTP)
    {
        $query = "INSERT INTO participant_otp SET
                pv_id=:pv_id,
                otp=:otp";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('pv_id', $participantOTP->pvId, PDO::PARAM_INT);
        $statement->bindValue('otp', $participantOTP->otp, PDO::PARAM_INT);
        $statement->execute();
    }
}
