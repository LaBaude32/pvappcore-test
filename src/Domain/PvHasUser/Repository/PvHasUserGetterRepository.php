<?php

namespace App\Domain\PvHasUser\Repository;

use PDO;
use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\User\Data\UserGetData;

/**
 * Repository.
 */
class PvHasUserGetterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function getAllPvIdByUserId(int $userId): array|null
    {
        $query = "SELECT * FROM pv_has_user WHERE user_id=:id";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('id', $userId, PDO::PARAM_INT);
        $statement->execute();

        $pvs = [];
        while ($row = $statement->fetch()) {
            $pvs[] = $row['pv_id'];
        }

        return $pvs;
    }

    public function getPvOwner(int $pvId): UserGetData
    {
        $query = "SELECT u.*
        FROM user u
        INNER JOIN pv_has_user phu
        ON phu.user_id = u.id_user
        WHERE pv_id=:pvId AND phu.owner=1";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('pvId', $pvId, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();

        $user = new UserGetData();
        $user->userId = (int) $row['id_user'];
        $user->email = (string) $row['email'];
        $user->firstName = (string) $row['first_name'];
        $user->lastName = (string) $row['last_name'];
        $user->phone = (string) $row['phone'];
        $user->userGroup = (string) $row['user_group'];
        $user->userFunction = (string) $row['user_function'];
        $user->organism = (string) $row['organism'];

        return $user;
    }

    public function getAllPvHasUser(int $pvId): array
    {
        $query = "SELECT * FROM pv_has_user WHERE pv_id=:pvId";
        $statement = $this->connection->prepare($query);
        $statement->bindValue('pvId', $pvId, PDO::PARAM_INT);
        $statement->execute();

        $pHUs = [];
        while ($row = $statement->fetch()) {
            $pHU = new PvHasUserData;
            $pHU->pvId = (int) $row['pv_id'];
            $pHU->userId = (int) $row['user_id'];
            $pHU->statusPAE = $row['status_PAE'];
            $pHU->invitedCurrentMeeting = (bool) $row['invited_current_meeting'];
            $pHU->invitedNextMeeting = (bool) $row['invited_next_meeting'];
            $pHU->distribution = (bool) $row['distribution'];
            $pHU->owner = (bool) $row['owner'];

            $pHUs[] = $pHU;
        }

        return $pHUs;
    }

    /**
     * Get all the participants of a pv
     *
     * @param integer $pvId
     * @return array(PvHasUserData)
     */
    public function getParticipantsByPvId(int $pvId): array
    {
        $query = "SELECT u.*, phu.*
        FROM user u
        INNER JOIN pv_has_user phu
        ON phu.user_id = u.id_user
        WHERE phu.pv_id =:pv_id";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('pv_id', $pvId, PDO::PARAM_INT);
        $statement->execute();

        while ($row = $statement->fetch()) {
            $user = new PvHasUserData();
            $user->userId = (int) $row['id_user'];
            $user->email = (string) $row['email'];
            $user->firstName = (string) $row['first_name'];
            $user->lastName = (string) $row['last_name'];
            $user->phone = (string) $row['phone'];
            $user->userGroup = (string) htmlspecialchars_decode($row['user_group']);
            $user->userFunction = (string) $row['user_function'];
            $user->organism = (string) $row['organism'];
            $user->statusPAE = $row['status_PAE'];
            $user->invitedCurrentMeeting = (bool) $row['invited_current_meeting'];
            $user->invitedNextMeeting = (bool) $row['invited_next_meeting'];
            $user->distribution = (bool) $row['distribution'];

            $users[] = $user;
        }
        return (array) $users;
    }

    public function getAllConnectedParticipants($userId): array|null
    {
        // $query = "SELECT DISTINCT u.*, phu.status_PAE, phu.invited_current_meeting, phu.invited_next_meeting, phu.distribution
        $query = "SELECT DISTINCT u.*
        FROM user u
        INNER JOIN pv_has_user phu ON u.id_user = phu.user_id
        WHERE phu.pv_id IN (
            SELECT pv_id
            FROM pv_has_user
            WHERE user_id = :user_id
        )
        AND u.id_user != :user_id;";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('user_id', $userId, PDO::PARAM_INT);
        $statement->execute();

        $users = null;

        while ($row = $statement->fetch()) {
            $user = new PvHasUserData();
            $user->userId = (int) $row['id_user'];
            $user->email = (string) $row['email'];
            $user->firstName = (string) $row['first_name'];
            $user->lastName = (string) $row['last_name'];
            $user->phone = (string) $row['phone'];
            $user->userGroup = (string) htmlspecialchars_decode($row['user_group']);
            $user->userFunction = (string) $row['user_function'];
            $user->organism = (string) $row['organism'];

            $users[] = $user;
        }
        return $users;
    }

    public function getParticipantStatus(int $userId, int $pvId): PvHasUserData
    {
        $query = "SELECT *
        FROM user u
        INNER JOIN pv_has_user phu ON u.id_user = phu.user_id
        WHERE phu.pv_id = :pv_id
        AND u.id_user = :user_id;";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('user_id', $userId, PDO::PARAM_INT);
        $statement->bindValue('pv_id', $pvId, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();
        $user = new PvHasUserData();
        $user->userId = (int) $row['id_user'];
        $user->email = (string) $row['email'];
        $user->firstName = (string) $row['first_name'];
        $user->lastName = (string) $row['last_name'];
        $user->phone = (string) $row['phone'];
        $user->userGroup = (string) htmlspecialchars_decode($row['user_group']);
        $user->userFunction = (string) $row['user_function'];
        $user->organism = (string) $row['organism'];
        $user->statusPAE = $row['status_PAE'];
        $user->invitedCurrentMeeting = (bool) $row['invited_current_meeting'];
        $user->invitedNextMeeting = (bool)$row['invited_next_meeting'];
        $user->distribution = (bool) $row['distribution'];

        return $user;
    }
}
