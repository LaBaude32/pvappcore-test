<?php

namespace App\Domain\PvHasUser\Repository;

use PDO;

/**
 * Repository.
 */
class ParticipantOTPDeletorRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function deleteParticipantOTPByPvId(int $pvId)
    {
        $query = "DELETE FROM participant_otp WHERE pv_id=:pvId";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('pvId', $pvId, PDO::PARAM_INT);
        $statement->execute();
    }
}
