<?php

namespace App\Domain\PvHasUser\Repository;

use PDO;
use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Data\PvHasUserCreateData;

/**
 * Repository.
 */
class PvHasUserCreatorRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Insert lot row.
     *
     * @param PvHasUserCreateData $lot The affaire
     *
     * @return int The new ID
     */
    public function insertPvHasUser(PvHasUserData $pvHasUser)
    {
        $query = "INSERT INTO pv_has_user SET
                pv_id=:pv_id,
                user_id=:user_id,
                status_PAE=:status_PAE,
                invited_current_meeting=:invited_current_meeting,
                invited_next_meeting=:invited_next_meeting,
                distribution=:distribution, 
                owner=:owner";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('pv_id', $pvHasUser->pvId, PDO::PARAM_INT);
        $statement->bindValue('user_id', $pvHasUser->userId, PDO::PARAM_INT);
        $statement->bindValue('status_PAE', $pvHasUser->statusPAE, PDO::PARAM_STR);
        $statement->bindValue('invited_current_meeting', $pvHasUser->invitedCurrentMeeting, PDO::PARAM_BOOL);
        $statement->bindValue('invited_next_meeting', $pvHasUser->invitedNextMeeting, PDO::PARAM_BOOL);
        $statement->bindValue('distribution', $pvHasUser->distribution, PDO::PARAM_BOOL);
        $pvHasUser->owner ? $statement->bindValue('owner', $pvHasUser->owner, PDO::PARAM_INT) : $statement->bindValue('owner', 0, PDO::PARAM_BOOL);
        $statement->execute();
    }

    public function insertPvHasUserToNewPv(array $data)
    {
        $query = "INSERT INTO pv_has_user SET
                pv_id=:pv_id,
                user_id=:user_id,
                status_PAE=:status_PAE,
                invited_current_meeting=:invited_current_meeting,
                invited_next_meeting=:invited_next_meeting,
                distribution=:distribution,
                owner=:owner";

        $statement = $this->connection->prepare($query);

        foreach ($data as $value) {
            $statement->bindValue('pv_id', $value->pvId, PDO::PARAM_INT);
            $statement->bindValue('user_id', $value->userId, PDO::PARAM_INT);
            $statement->bindValue('status_PAE', $value->statusPAE, PDO::PARAM_STR);
            $statement->bindValue('invited_current_meeting', $value->invitedCurrentMeeting, PDO::PARAM_BOOL);
            $statement->bindValue('invited_next_meeting', $value->invitedNextMeeting, PDO::PARAM_BOOL);
            $statement->bindValue('distribution', $value->distribution, PDO::PARAM_BOOL);
            $value->owner ? $statement->bindValue('owner', $value->owner, PDO::PARAM_INT) : $statement->bindValue('owner', 0, PDO::PARAM_BOOL);

            $statement->execute();
        }
    }
}
