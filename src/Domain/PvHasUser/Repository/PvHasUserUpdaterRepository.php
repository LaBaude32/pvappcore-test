<?php

namespace App\Domain\PvHasUser\Repository;

use PDO;
use App\Domain\PvHasUser\Data\PvHasUserData;
use App\Domain\PvHasUser\Data\PvHasUserCreateData;

/**
 * Repository.
 */
class PvHasUserUpdaterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Insert lot row.
     *
     * @param PvHasUserCreateData $lot The affaire
     *
     * @return int The new ID
     */
    public function updatePvHasUser(PvHasUserData $pvHasUser): void
    {
        $query = "UPDATE pv_has_user SET
                status_PAE=:status_PAE,
                invited_current_meeting=:invited_current_meeting,
                invited_next_meeting=:invited_next_meeting,
                distribution=:distribution
                WHERE pv_id=:pv_id AND user_id=:user_id";
        $statement = $this->connection->prepare($query);
        $statement->bindValue('pv_id', $pvHasUser->pvId, PDO::PARAM_INT);
        $statement->bindValue('user_id', $pvHasUser->userId, PDO::PARAM_INT);
        !is_null($pvHasUser->statusPAE) ? $statement->bindValue('status_PAE', $pvHasUser->statusPAE, PDO::PARAM_STR) :
            $statement->bindValue('status_PAE', NULL, PDO::PARAM_NULL);;
        !is_null($pvHasUser->invitedCurrentMeeting) ?
            $statement->bindValue('invited_current_meeting', $pvHasUser->invitedCurrentMeeting, PDO::PARAM_BOOL) :
            $statement->bindValue('invited_current_meeting', NULL, PDO::PARAM_NULL);
        !is_null($pvHasUser->invitedNextMeeting) ?
            $statement->bindValue('invited_next_meeting', $pvHasUser->invitedNextMeeting, PDO::PARAM_BOOL) :
            $statement->bindValue('invited_next_meeting', NULL, PDO::PARAM_NULL);
        !is_null($pvHasUser->distribution) ?
            $statement->bindValue('distribution', $pvHasUser->distribution, PDO::PARAM_BOOL) :
            $statement->bindValue('distribution', NULL, PDO::PARAM_NULL);
        $statement->execute();
    }
}
