<?php

namespace App\Domain\PvHasUser\Repository;

use App\Domain\PvHasUser\Data\ParticipantOTPData;
use DateTime;
use PDO;

/**
 * Repository.
 */
class ParticipantOTPGetterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function getParticipantOTPByPvId(int $pvId): ParticipantOTPData|null
    {
        $query = "SELECT * FROM participant_otp WHERE pv_id=:pvId";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('pvId', $pvId, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();

        $participantOTP = null;

        if (!empty($row)) {
            $participantOTP = new ParticipantOTPData();
            $participantOTP->pvId = $row['pv_id'];
            $participantOTP->otp = $row['otp'];
            $participantOTP->createdAt = new DateTime($row['created_at']);
            $baseDate = new DateTime($row['created_at']);
            $participantOTP->createdAtTimeStamp = $baseDate->getTimestamp();
        }

        return $participantOTP;
    }
}
