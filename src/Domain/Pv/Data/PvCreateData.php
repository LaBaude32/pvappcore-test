<?php

namespace App\Domain\Pv\Data;

/**
 * @OA\Schema()
 * 
 * PvCreateData
 */
final class PvCreateData
{
    /** 
     * @OA\Property()
     * 
     * @var string 
     */
    public $state;

    /** 
     * @OA\Property(title="date de la réunion")
     * 
     * @var \DateTimeInterface 
     */
    public $meetingDate;

    /** 
     * @OA\Property(title="lieu de la réunion")
     * 
     * @var string 
     */
    public $meetingPlace;

    /** 
     * @OA\Property(title="date de la prochaine réunion")
     * 
     * @var \DateTimeInterface|null
     */
    public $meetingNextDate;

    /** 
     * @OA\Property(title="lieu de la prochaine réunion")
     * 
     * @var string|null
     */
    public $meetingNextPlace;

    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $affairId;

    /** 
     * @OA\Property(title="date de publication", format="date")
     * 
     * @var \DateTimeInterface|null
     */
    public $releaseDate;
}
