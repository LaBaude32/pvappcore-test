<?php

namespace App\Domain\Pv\Data;

/**
 * @OA\Schema()
 * 
 * PvHasItemData
 */
final class PvHasItemData
{
      /** 
     * @OA\Property()
     * 
     * @var int 
     */
  public $pvId;

      /** 
     * @OA\Property()
     * 
     * @var int 
     */
  public $itemId;
}
