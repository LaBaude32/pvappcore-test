<?php

namespace App\Domain\Pv\Data;

/**
 * @OA\Schema()
 * 
 * PvGetData
 */
final class PvGetData
{
    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $pvId;

    /** 
     * @OA\Property()
     * 
     * @var string 
     */
    public $state;

    /** 
     * @OA\Property(title="date de la réunion", format="date")
     * 
     * @var \DateTimeInterface 
     */
    public $meetingDate;

    /** 
     * @OA\Property(title="lieu de la réunion")
     * 
     * @var string 
     */
    public $meetingPlace;

    /** 
     * @OA\Property(title="date de la prochaine réunion")
     * 
     * @var \DateTimeInterface|null
     */
    public $meetingNextDate;

    /** 
     * @OA\Property(title="lieu de la prochaine réunion")
     * 
     * @var string|null
     */
    public $meetingNextPlace;

    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $affairId;

    /** 
     * @OA\Property(title="date de publication du Pv")
     * 
     * @var \DateTimeInterface|null
     */
    public $releaseDate;

    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $pvNumber;

    /** 
     * @OA\Property()
     * 
     * @var string 
     */
    public $affairName;

    /** 
     * @OA\Property(enum={"Chantier","Etude"})
     * 
     * @var string 
     */
    public $affairMeetingType;

    /** 
     * @OA\Property(ref="#/components/schemas/LotGetData")
     * 
     * @var array 
     */
    public $lots;

    /** 
     * @OA\Property(ref="#/components/schemas/AgendaGetData")
     * 
     * @var array 
     */
    public $agendas;
}
