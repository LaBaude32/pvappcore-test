<?php

namespace App\Domain\Pv\Repository;

use PDO;
use App\Domain\Pv\Data\PvGetData;
use App\Domain\Lot\Data\LotGetData;

/**
 * Repository.
 */
class PvGetterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function getPvByAffairId(int $id_affair): array
    {
        $sql = "SELECT * FROM pv WHERE affair_id=:id";

        $statement = $this->connection->prepare($sql);
        $statement->bindValue('id', $id_affair, PDO::PARAM_INT);
        $statement->execute();

        while ($row = $statement->fetch()) {
            $pv = new PvGetData();
            $pv->pvId = (int) $row['id_pv'];
            $pv->state = (string) $row['state'];
            $pv->meetingDate = (string) $row['meeting_date'];
            $pv->meetingPlace = (string) htmlspecialchars_decode($row['meeting_place']);
            $pv->meetingNextDate = (string) $row['meeting_next_date'];
            $pv->meetingNextPlace = $row['meeting_next_place'] ? htmlspecialchars_decode($row['meeting_next_place']) : null;
            $pv->affairId = (int) $row['affair_id'];
            $pv->releaseDate = $row['release_date'];

            $pvs[] = $pv;
        }
        return (array) $pvs;
    }

    public function getPvById(int $pvId): PvGetData
    {
        $query = "SELECT p.*, a.meeting_type as affair_meeting_type, a.name as affair_name
        FROM pv p
        INNER JOIN affair a ON a.id_affair = p.affair_id
        WHERE p.id_pv =:id";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('id', $pvId, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();
        $pv = new PvGetData();
        $pv->pvId = (int) $row['id_pv'];
        $pv->state = (string) $row['state'];
        $pv->meetingDate = (string) $row['meeting_date'];
        $pv->meetingPlace = (string) htmlspecialchars_decode($row['meeting_place']);
        $pv->meetingNextDate = (string) $row['meeting_next_date'];
        $pv->meetingNextPlace = $row['meeting_next_place'] ? htmlspecialchars_decode($row['meeting_next_place']) : null;
        $pv->affairId = (int) $row['affair_id'];
        $pv->affairName = (string) htmlspecialchars_decode($row['affair_name']);
        $pv->affairMeetingType = (string) $row['affair_meeting_type'];
        $pv->releaseDate = $row['release_date'];

        $query = "SELECT COUNT(created_at) as result FROM pv WHERE affair_id = :affairId AND id_pv <= :pvId";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('affairId', $pv->affairId, PDO::PARAM_INT);
        $statement->bindValue('pvId', $pv->pvId, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();

        $pv->pvNumber = (int) $row['result'];

        return $pv;
    }

    public function getPvNumber(PvGetData $pv): PvGetData
    {
        $query = "SELECT COUNT(created_at) as result FROM pv WHERE affair_id = :affairId AND id_pv <= :pvId";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('affairId', $pv->affairId, PDO::PARAM_INT);
        $statement->bindValue('pvId', $pv->pvId, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();

        $pv->pvNumber = (int) $row['result'];

        return $pv;
    }



    /**
     * [Description for getPvsByUserId]
     *
     * @param array $data
     * 
     * @return array[PvGetData]|null
     * 
     */
    public function getPvsByUserId(array $data): array|null
    {
        $query = "SELECT p.*, a.name as affair_name
        FROM pv p
        INNER JOIN pv_has_user phu ON phu.pv_id = p.id_pv
        INNER JOIN affair a ON a.id_affair = p.affair_id
        WHERE phu.user_id =:user_id
        ORDER BY p.created_at
        DESC";
        if (array_key_exists('numberOfPvs', $data)) {
            $query .= " LIMIT :nbPvs";
        }

        $statement = $this->connection->prepare($query);
        $statement->bindValue('user_id', $data['userId'], PDO::PARAM_INT);
        if (array_key_exists('numberOfPvs', $data)) {
            $statement->bindValue('nbPvs', $data['numberOfPvs'], PDO::PARAM_INT);
        }
        $statement->execute();

        $pvs = null;

        while ($row = $statement->fetch()) {
            $pv = new PvGetData();
            $pv->pvId = (int) $row['id_pv'];
            $pv->state = (string) $row['state'];
            $pv->meetingDate = (string) $row['meeting_date'];
            $pv->meetingPlace = (string) htmlspecialchars_decode($row['meeting_place']);
            $pv->meetingNextDate = (string) $row['meeting_next_date'];
            $pv->meetingNextPlace = $row['meeting_next_place'] ? htmlspecialchars_decode($row['meeting_next_place']) : null;
            $pv->affairId = (int) $row['affair_id'];
            $pv->affairName = (string) htmlspecialchars_decode($row['affair_name']);

            $pvs[] = $pv;
        }

        return $pvs;
    }

    public function getLotsForPv(PvGetData $pv): PvGetData
    {
        $query = "SELECT l.* FROM lot l
            INNER JOIN affair a ON a.id_affair = l.affair_id
            INNER JOIN pv i ON i.affair_id = a.id_affair
            WHERE i.id_pv =:pvId";

        $statement = $this->connection->prepare($query);
        $statement->bindValue("pvId", $pv->pvId, PDO::PARAM_INT);
        $statement->execute();
        while ($row = $statement->fetch()) {
            $result = new LotGetData();
            $result->lotId = (int) $row['id_lot'];
            $result->name = (string) $row['name'];

            $pv->lots[] = $result;
        }
        $pvToReturn = $pv;

        return $pvToReturn;
    }

    public function getReleasedPvsByUserId(int $userId): array|null
    {
        $query = "SELECT p.*, a.name as affair_name
        FROM pv p
        INNER JOIN pv_has_user phu ON phu.pv_id = p.id_pv
        INNER JOIN affair a ON a.id_affair = p.affair_id
        WHERE phu.user_id =:user_id AND p.state = 'Terminé'
        ORDER BY p.created_at
        DESC
        LIMIT 10";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('user_id', $userId, PDO::PARAM_INT);
        $statement->execute();

        $pvs = null;

        while ($row = $statement->fetch()) {
            $pv = new PvGetData();
            $pv->pvId = (int) $row['id_pv'];
            $pv->state = (string) $row['state'];
            $pv->meetingDate = (string) $row['meeting_date'];
            $pv->meetingPlace = (string) htmlspecialchars_decode($row['meeting_place']);
            $pv->meetingNextDate = (string) $row['meeting_next_date'];
            $pv->meetingNextPlace = $row['meeting_next_place'] ? htmlspecialchars_decode($row['meeting_next_place']) : null;
            $pv->releaseDate = $row['release_date'];
            $pv->affairId = (int) $row['affair_id'];
            $pv->affairName = (string) htmlspecialchars_decode($row['affair_name']);

            $pvs[] = $pv;
        }

        return $pvs;
    }

    // public function getPreviousPv(PvGetData $pv): PvGetData
    // {
    //     $query = "SELECT * FROM pv WHERE affair_id=:affairId AND id_pv=:pvId";
    //     $pv = new PvGetData;

    //     return $pv;
    // }
}
