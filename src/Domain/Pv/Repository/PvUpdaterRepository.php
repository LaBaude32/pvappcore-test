<?php

namespace App\Domain\Pv\Repository;

use PDO;
use App\Domain\Pv\Data\PvGetData;

/**
 * Repository.
 */
class PvUpdaterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Insert lot row.
     *
     * @param PvGetData $lot The affair
     *
     * @return int The new ID
     */
    public function UpdatePv(PvGetData $pv): int
    {
        $row = [
            'id_pv' => $pv->pvId,
            'state' => $pv->state,
            'meeting_date' => $pv->meetingDate,
            'meeting_place' => $pv->meetingPlace,
            'meeting_next_date' => $pv->meetingNextDate,
            'meeting_next_place' => $pv->meetingNextPlace,
            'affair_id' => $pv->affairId,
        ];

        $query = "UPDATE pv SET
                state=:state,
                meeting_date=:meeting_date,
                meeting_place=:meeting_place,
                meeting_next_date=:meeting_next_date,
                meeting_next_place=:meeting_next_place,
                affair_id=:affair_id
                WHERE id_pv=:id_pv";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('id_pv', $pv->pvId, PDO::PARAM_INT);
        $statement->execute($row);

        return (int) $pv->pvId; //TODO: supprimer ça et faire un getter dans le Action
    }

    /**
     * Validation d'un pv
     *
     * @param integer $pvId
     * @return void
     */
    public function validatePv(int $pvId)
    {
        $row = [
            "idPv" => $pvId,
            "state" => "Terminé"
        ];

        $query = "UPDATE pv SET
                state=:state,
                release_date = NOW()
                WHERE id_pv=:idPv";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('idPv', $pvId, PDO::PARAM_INT);
        $statement->execute($row);
    }

    /**
     * Remettre un pv en "En cours"
     *
     * @param integer $pvId
     * @return void
     */
    public function unValidatePv(int $pvId): void
    {
        $query = "UPDATE pv SET
                state=:state,
                release_date = :releaseDate
                WHERE id_pv=:idPv";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('idPv', $pvId, PDO::PARAM_INT);
        $statement->bindValue('releaseDate', NULL, PDO::PARAM_NULL);
        $statement->bindValue('state', "En cours", PDO::PARAM_STR);
        $statement->execute();
    }
}
