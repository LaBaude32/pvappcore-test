<?php

namespace App\Domain\Pv\Service;

use App\Domain\Agenda\Repository\AgendaGetterRepository;
use App\Domain\Lot\Repository\LotGetterRepository;
use App\Domain\Pv\Data\PvGetData;
use UnexpectedValueException;
use App\Domain\Pv\Repository\PvGetterRepository;

/**
 * Service.
 */
final class PvGetter
{
    /**
     * @var PvGetterRepository
     */
    private $repository;
    private $lotGetterRepository;
    private $agendaGetterRepository;

    /**
     * The constructor.
     *
     * @param PvGetterRepository $repository The repository
     */
    public function __construct(PvGetterRepository $repository, LotGetterRepository $lotGetterRepository, AgendaGetterRepository $agendaGetterRepository)
    {
        $this->repository = $repository;
        $this->lotGetterRepository = $lotGetterRepository;
        $this->agendaGetterRepository = $agendaGetterRepository;
    }

    /**
     * Get all the pvs.
     *
     * @return array All the pvs
     */
    public function getPvByAffairId(int $id): array
    {
        // Validation
        if (empty($id)) {
            throw new UnexpectedValueException('id required');
        }

        if ($id == 0) {
            throw new UnexpectedValueException('id doit être positif');
        }

        // Get All pvs
        $pvs = $this->repository->getPvByAffairId($id);

        foreach ($pvs as $pv) {
            $pvsToReturn[] = $this->repository->getPvNumber($pv);
        }

        return (array) $pvsToReturn;
    }


    /**
     * Get one pvs.
     *
     * @return PvGetData All one pv with his items
     */
    public function getPvById(int $id): PvGetData
    {
        // Validation
        if (empty($id)) {
            throw new UnexpectedValueException('id required');
        }

        if ($id == 0) {
            throw new UnexpectedValueException('id doit être positif');
        }

        // Get one pv
        $pv = $this->repository->getPvById($id);

        $pv->agendas = $this->agendaGetterRepository->getAllAgendasForPv($pv->pvId);

        return $pv;
    }

    public function getAffairsIdByPvsId(array $pvsId): array
    {
        foreach ($pvsId as $pvId) {
            $pv = $this->repository->getPvById($pvId);
            $affairsId[] = $pv->affairId;
        }

        $result = array_unique($affairsId);

        return (array) $result;
    }

    public function getLastsPvByUserId(array $data): array|null
    {
        // Validation
        if (empty($data['userId'])) {
            throw new UnexpectedValueException('id required');
        }

        if ($data['userId'] == 0) {
            throw new UnexpectedValueException('id doit être positif');
        }

        if ($data['numberOfPvs'] == 0) {
            $data['numberOfPvs'] = 1;
        }

        $pvs = $this->repository->getPvsByUserId($data);

        return $pvs;
    }

    /**
     * [Description for getAllPvByUserId]
     *
     * @param int $userId
     * 
     * @return array[PvGetData]
     * 
     */
    public function getAllPvByUserId(int $userId): array
    {
        // Validation
        if (empty($userId)) {
            throw new UnexpectedValueException('id required');
        }

        $data['userId'] = $userId;
        $pvs = $this->repository->getPvsByUserId($data);

        return (array) $pvs;
    }

    public function getLotsForPv(PvGetData $pv)
    {
        $pv->lots = $this->lotGetterRepository->getPvHasLots($pv->pvId);

        if (empty($pv->lots)) {
            $this->repository->getLotsForPv($pv);
        }

        return $pv;
    }

    public function getPvNumber(PvGetData $pv): PvGetData
    {
        $pvToReturn = $this->repository->getPvNumber($pv);

        return $pvToReturn;
    }


    /**
     * Recupération des Pvs diffusés par un user
     *
     * @param integer $userId
     * @return array|null[PvGetData] 
     */
    public function getRelesedPvsByUserId(int $userId): array|null
    {
        // Validation
        if (empty($userId)) {
            throw new UnexpectedValueException('id required');
        }

        $pvs = $this->repository->getReleasedPvsByUserId($userId);

        foreach ($pvs as $pv) {
            $pvsToReturn[] = $this->repository->getPvNumber($pv);
        }

        return $pvs;
    }

    // public function getPreviousPv(PvGetData $pv): PvGetData
    // {
    //     $pvToReturn = $this->repository->getPreviousPv($pv);

    //     return $pvToReturn;
    // }
}
