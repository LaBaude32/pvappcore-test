<?php

namespace App\Domain\Affair\Repository;

use PDO;
use App\Domain\Affair\Data\AffairGetData;
use App\Domain\Lot\Data\LotGetData;
use App\Domain\Lot\Data\LotProgressGetData;

/**
 * Repository.
 */
class AffairGetterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get All Affairs.
     *
     * @return array All the affairs
     */
    public function getAllAffairs(): array
    {
        $sql = "SELECT * FROM affair";

        $statement = $this->connection->prepare($sql);
        $statement->execute();

        while ($row = $statement->fetch()) {
            $affair = new AffairGetData();
            $affair->affairId = (int) $row['id_affair'];
            $affair->name = htmlspecialchars_decode($row['name']);
            $affair->address = htmlspecialchars_decode($row['address']);
            $affair->progress = (int) $row['progress'];
            $affair->meetingType = (string) $row['meeting_type'];
            $affair->description = (string) $row['description'];

            $affairs[] = $affair;
        }
        return (array) $affairs;
    }

    public function getAffairById(int $affairId): AffairGetData
    {
        $sql = "SELECT * 
        FROM affair 
        WHERE id_affair=:affairId";

        $statement = $this->connection->prepare($sql);
        $statement->bindValue('affairId', $affairId, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();
        $affair = new AffairGetData();
        $affair->affairId = (int) $row['id_affair'];
        $affair->name = htmlspecialchars_decode($row['name']);
        $affair->address = htmlspecialchars_decode($row['address']);
        $affair->progress = (int) $row['progress'];
        $affair->meetingType = (string) $row['meeting_type'];
        $affair->description = (string) $row['description'];

        return $affair;
    }

    public function getFullAffairById(int $affairId): AffairGetData
    {
        $sql = "SELECT
                    a.name AS affair_name,
                    a.id_affair,
                    a.address,
                    a.meeting_type,
                    a.description,
                    l.id_lot,
                    l.name AS lot_name,
                    pvl.id_pv,
                    pvl.progress AS lot_progress,
                    pvl.already_done
                FROM
                    affair a
                JOIN
                    pv p ON p.id_pv = (
                        SELECT MAX(id_pv)
                        FROM pv
                        WHERE affair_id = a.id_affair
                    )
                JOIN
                    pv_has_lot pvl ON pvl.id_pv = p.id_pv
                JOIN
                    lot l ON l.id_lot = pvl.id_lot
                WHERE
                    a.id_affair = :affairId;";

        $statement = $this->connection->prepare($sql);
        $statement->bindValue('affairId', $affairId, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();
        if (empty($row)) {
            $sql = "SELECT a.name AS affair_name,
                    a.id_affair,
                    a.address,
                    a.meeting_type,
                    a.description, 
                    l.id_lot,
                    l.name AS lot_name
                    FROM affair a
                    LEFT JOIN
                    lot l ON l.affair_id = a.id_affair
                    WHERE id_affair=:affairId";

            $statement = $this->connection->prepare($sql);
            $statement->bindValue('affairId', $affairId, PDO::PARAM_INT);
            $statement->execute();

            $row = $statement->fetch();
        }
        $affair = new AffairGetData();
        $affair->affairId = (int) $row['id_affair'];
        $affair->name = htmlspecialchars_decode($row['affair_name']);
        $affair->address = htmlspecialchars_decode($row['address']);
        // $affair->progress = (int) $row['progress'];
        $affair->meetingType = (string) $row['meeting_type'];
        $affair->description = (string) $row['description'];
        if ($row["id_lot"]) {
            $lot = new LotProgressGetData();
            $lot->lotId = (int) $row["id_lot"];
            $lot->name = (string) $row["lot_name"];
            if (array_key_exists("lot_progress", $row)) {
                $lot->progress = (int) $row["lot_progress"];
                $lot->alreadyDone = $row["already_done"];
            }
            $affair->lots[] = $lot;

            while ($row = $statement->fetch()) {
                $lot = new LotProgressGetData();
                $lot->lotId = (int) $row["id_lot"];
                $lot->name = (string) $row["lot_name"];
                if (array_key_exists("lot_progress", $row)) {
                    $lot->progress = (int) $row["lot_progress"];
                    $lot->alreadyDone = $row["already_done"];
                }
                $affair->lots[] = $lot;
            }
        }
        return $affair;
    }

    function getAffairsByUserId(int $userId): array|null
    {
        $query = "SELECT DISTINCT affair.* FROM affair
                    JOIN pv ON pv.affair_id = affair.id_affair
                    JOIN pv_has_user ON pv_has_user.pv_id = pv.id_pv
                    WHERE pv_has_user.user_id = :userId;";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('userId', $userId, PDO::PARAM_INT);
        $statement->execute();

        $affairs = NULL;

        while ($row = $statement->fetch()) {
            $affair = new AffairGetData();
            $affair->affairId = (int) $row['id_affair'];
            $affair->name = htmlspecialchars_decode($row['name']);
            $affair->address = htmlspecialchars_decode($row['address']);
            $affair->progress = (int) $row['progress'];
            $affair->meetingType = (string) $row['meeting_type'];
            $affair->description = (string) $row['description'];

            $affairs[] = $affair;
        }
        return (array) $affairs;
    }
}
