<?php

namespace App\Domain\Affair\Data;

/**
 * @OA\Schema()
 * 
 * AffairCreateData
 */
final class AffairCreateData
{
    /** 
     * @OA\Property()
     * 
     * @var string 
     */
    public $name;

    /** 
     * @OA\Property()
     * 
     * @var string 
     */
    public $address;

    /** 
     * @OA\Property()
     * 
     * @var int|null
     */
    public $progress;

    /** 
     * @OA\Property(enum={"Chantier","Etude"})
     * 
     * @var string 
     */
    public $meetingType;

    /** 
     * @OA\Property()
     * 
     * @var string|null
     */
    public $description;
}
