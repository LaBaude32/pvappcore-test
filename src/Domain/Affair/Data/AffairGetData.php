<?php

namespace App\Domain\Affair\Data;

/**
 * @OA\Schema()
 * 
 * AffairGetData
 */
final class AffairGetData
{
    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $affairId;

    /** 
     * @OA\Property()
     * 
     * @var string 
     */
    public $name;

    /** 
     * @OA\Property()
     * 
     * @var string 
     */
    public $address;

    /** 
     * @OA\Property()
     * 
     * @var int|null
     */
    public $progress;

    /** 
     * @OA\Property(enum={"Chantier","Etude"})
     * 
     * @var string 
     */
    public $meetingType;

    /** 
     * @OA\Property()
     * 
     * @var string|null
     */
    public $description;

    /** 
     * @OA\Property(type="array", @OA\Items(ref="#/components/schemas/LotProgressGetData"))
     * 
     * @var array|null
     */
    public $lots;
}
