<?php

declare(strict_types=1);

namespace App\Domain\Affair\Service;

use UnexpectedValueException;
use App\Domain\Affair\Repository\AffairGetterRepository;
use App\Domain\Affair\Data\AffairGetData;
use App\Domain\Lot\Repository\LotGetterRepository;

/**
 * Service.
 */
final class AffairGetter
{
    /**
     * @var AffairGetterRepository
     * @var LotGetterRepository
     */
    private $repository;
    private $lotGetterRepository;

    /**
     * The constructor.
     *
     * @param AffairGetterRepository $repository The repository
     */
    public function __construct(AffairGetterRepository $repository, LotGetterRepository $lotGetterRepository)
    {
        $this->repository = $repository;
        $this->lotGetterRepository = $lotGetterRepository;
    }

    /**
     * Get all the affairs.
     *
     * @return array All the affairs
     */
    public function getAllAffairs(): array
    {

        // Get All Affairs
        $affairs = $this->repository->getAllAffairs();

        return (array) $affairs;
    }

    public function getAffairById(int $id): AffairGetData
    {
        // Validation
        if (empty($id)) {
            throw new UnexpectedValueException('id required');
        }

        $affair = $this->repository->getAffairById($id);
        if ($affair->meetingType == "Chantier") {
            $affair->lots = $this->lotGetterRepository->getLotByAffairId($affair->affairId);
        }
        return $affair;
    }

    public function getFullAffairById(int $id): AffairGetData
    {
        // Validation
        if (empty($id)) {
            throw new UnexpectedValueException('id required');
        }

        $affair = $this->repository->getFullAffairById($id);
        return $affair;
    }

    public function getAffairsByIds(array $affairsId): array
    {
        // Validation
        if (empty($affairsId)) {
            throw new UnexpectedValueException('id required');
        }

        foreach ($affairsId as $affairId) {
            if (empty($affairId)) {
                throw new UnexpectedValueException('id required');
            }

            $affair = $this->repository->getAffairById($affairId);
            $affairs[] = $affair;
        }

        return (array) $affairs;
    }

    public function getAffairByIdWithLots($affairId): AffairGetData
    {
        if (empty($affairId)) {
            throw new UnexpectedValueException('id required');
        }

        $affair = $this->repository->getAffairById($affairId);

        return $affair;
    }

    public function getAffairsByUserId(int $userId): array|null
    {
        if (empty($userId)) {
            throw new UnexpectedValueException('user Id required');
        }

        $affairs = $this->repository->getAffairsByUserId($userId);

        return $affairs;
    }
}
