<?php

namespace App\Domain\Agenda\Data;

/**
 * @OA\Schema()
 * 
 * AgendaGetData
 */
final class AgendaGetData
{
    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $agendaId;

    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $position;

    /** 
     * @OA\Property()
     * 
     * @var string 
     */
    public $title;

    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $pvId;
}
