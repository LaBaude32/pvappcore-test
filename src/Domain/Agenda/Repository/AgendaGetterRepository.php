<?php

declare(strict_types=1);

namespace App\Domain\Agenda\Repository;

use App\Domain\Agenda\Data\AgendaGetData;
use PDO;

final class AgendaGetterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * getAgendaById
     *
     * @param integer $agendaId
     * @return AgendaGetData
     */
    public function getAgendaById(int $agendaId): AgendaGetData
    {
        $query = "SELECT * 
        FROM agenda 
        WHERE id_agenda=:agendaId";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('agendaId', $agendaId, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();
        $agenda = new AgendaGetData();
        $agenda->agendaId = (int) $row['id_agenda'];
        $agenda->position = (int) $row['position'];
        $agenda->title = (string) htmlspecialchars_decode($row['title']);
        $agenda->pvId = (int) $row['pv_id'];

        return $agenda;
    }
    /**
     * Recuperer tous les ordres du jour d'un Pv
     *
     * @param integer $pvId
     * @return array|null
     */
    public function getAllAgendasForPv(int $pvId): array|null
    {
        $query = "SELECT *
        FROM agenda 
        WHERE pv_id=:pvId
        ORDER BY position";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('pvId', $pvId, PDO::PARAM_INT);
        $statement->execute();

        $agendas = NULL;

        while ($row = $statement->fetch()) {
            $agenda = new AgendaGetData();
            $agenda->agendaId = (int) $row['id_agenda'];
            $agenda->position = (int) $row['position'];
            $agenda->title = (string) htmlspecialchars_decode($row['title']);
            $agenda->pvId = (int) $row['pv_id'];

            $agendas[] = $agenda;
        }

        return $agendas;
    }
}
