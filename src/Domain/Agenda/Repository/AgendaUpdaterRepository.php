<?php

declare(strict_types=1);

namespace App\Domain\Agenda\Repository;

use PDO;
use App\Domain\Agenda\Data\AgendaGetData;

final class AgendaUpdaterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }
    /**
     * MAJ d'un ordre du jour
     *
     * @param AgendaGetData $agenda
     * @return void
     */
    public function updateAgenda(array $agendas)
    {
        foreach ($agendas as $agenda) {
            $query = "INSERT INTO agenda SET
                id_agenda=:agendaId,
                position=:position,
                title=:title,
                pv_id=:pvId
                ON DUPLICATE KEY UPDATE position=:position, title=:title";
            //  WHERE id_agenda=:agendaId";

            $statement = $this->connection->prepare($query);
            if ($agenda->agendaId >= 0) {
                $statement->bindValue('agendaId', $agenda->agendaId, PDO::PARAM_INT);
            } else {
                $statement->bindValue('agendaId', PDO::PARAM_NULL);
            }
            $statement->bindValue('position', $agenda->position, PDO::PARAM_INT);
            $statement->bindValue('title', $agenda->title, PDO::PARAM_STR);
            $statement->bindValue('pvId', $agenda->pvId, PDO::PARAM_INT);
            $statement->execute();
        }
    }
}
