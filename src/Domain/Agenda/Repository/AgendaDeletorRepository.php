<?php

declare(strict_types=1);

namespace App\Domain\Agenda\Repository;

use PDO;

final class AgendaDeletorRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function deleteAgenda(int $agendaId)
    {
        $row = [
            'id_agenda' => $agendaId
        ];

        $query = "DELETE FROM agenda WHERE id_agenda=:id_agenda";

        $this->connection->prepare($query)->execute($row);
    }
}
