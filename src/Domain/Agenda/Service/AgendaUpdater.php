<?php

declare(strict_types=1);

namespace App\Domain\Agenda\Service;

use App\Domain\Agenda\Data\AgendaGetData;
use App\Domain\Agenda\Repository\AgendaGetterRepository;
use App\Domain\Agenda\Repository\AgendaUpdaterRepository;

class AgendaUpdater
{
    /**
     * @var AgendaUpdaterRepository
     */
    private $repository;
    private $getterRepository;

    /**
     * The constructor.
     *
     * @param AgendaUpdaterRepository $repository The repository
     */
    public function __construct(AgendaUpdaterRepository $repository, AgendaGetterRepository $getterRepository)
    {
        $this->repository = $repository;
        $this->getterRepository = $getterRepository;
    }

    /**
     * Update an Agenda.
     *
     * @param AgendaGetData $Agenda The Agenda data
     */
    public function updateAgenda(array $agendas, int $pvId): array
    {
        // Validation
        // if (empty($agenda->agendaId)) {
        //     throw new UnexpectedValueException('id required');
        // }

        // Insert Agenda
        $this->repository->updateAgenda($agendas);
        $agendas = $this->getterRepository->getAllAgendasForPv($pvId);
        // Logging here: Agenda created successfully

        return $agendas;
    }
}
