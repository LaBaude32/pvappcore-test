<?php

declare(strict_types=1);

namespace App\Domain\Agenda\Service;

use UnexpectedValueException;
use App\Domain\Agenda\Data\AgendaGetData;
use App\Domain\Agenda\Repository\AgendaGetterRepository;

final class AgendaGetter
{

    /**
     * @var AgendaGetterRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param AgendaGetterRepository $repository The repository
     */
    public function __construct(AgendaGetterRepository $repository)
    {
        $this->repository = $repository;
    }
    public function getAgendaById(int $id): AgendaGetData
    {
        // Validation
        if (empty($id)) {
            throw new UnexpectedValueException('id required');
        }

        $agenda = $this->repository->getAgendaById($id);

        return $agenda;
    }

    public function getAgendasByPvId(int $id): array|null
    {
        if (empty($id)) {
            throw new UnexpectedValueException('id required');
        }
        $agendas = NULL;
        $agendas = $this->repository->getAllAgendasForPv($id);

        return $agendas;
    }
}
