<?php

declare(strict_types=1);

namespace App\Domain\Agenda\Service;

use UnexpectedValueException;
use App\Domain\Agenda\Repository\AgendaDeletorRepository;

final class AgendaDeletor
{
    /**
     * @var AgendaDeletorRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param AgendaDeletorRepository $repository The repository
     */
    public function __construct(AgendaDeletorRepository $repository)
    {
        $this->repository = $repository;
    }

    public function deleteAgenda(int $agendaId)
    {
        // Validation
        if (empty($agendaId)) {
            throw new UnexpectedValueException('Id required');
        }

        // delete Agenda
        $this->repository->deleteAgenda($agendaId);

        // Logging here: Agenda created successfully
    }
}
