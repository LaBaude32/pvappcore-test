<?php

namespace App\Domain\Item\Repository;

use PDO;
use App\Domain\Item\Data\ItemGetData;
use App\Domain\Lot\Data\LotGetData;
use App\Domain\Pv\Data\PvHasItemData;

/**
 * Repository.
 */
class ItemGetterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    public function getItemById(int $itemId): ItemGetData
    {
        $query = "SELECT * FROM item WHERE id_item=:id_item";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('id_item', $itemId, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();
        $item = new ItemGetData();
        $item->itemId = (int) $row['id_item'];
        $item->position = (int) $row['position'];
        $item->note = isset($row['note']) ? htmlspecialchars_decode($row['note']) : null;
        $item->followUp = isset($row['follow_up']) ? htmlspecialchars_decode($row['follow_up']) : null;
        $item->resources = $row['resources'];
        $item->completion = $row['completion'];
        $item->completionDate = $row['completion_date'];
        $item->visible = (int) $row['visible'];
        $item->image = $row['image'];
        $item->thumbnail = $row['thumbnail'];
        $item->createdAt = (string) $row['created_at'];

        return $item;
    }

    public function getItemsByPvId(int $pvId): array|null
    {
        $query = "SELECT i.*
        FROM item i
        INNER JOIN pv_has_item phi
        ON phi.item_id = i.id_item
        WHERE phi.pv_id =:pv_id";

        // $query = "SELECT i.*, l.id_lot, l.name as lot_name
        // FROM item i
        // INNER JOIN pv_has_item phi ON phi.item_id = i.id_item
        // INNER JOIN item_has_lot ihl ON ihl.item_id = i.id_item
        // INNER JOIN lot l ON l.id_lot = ihl.lot_id
        // WHERE phi.pv_id =2";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('pv_id', $pvId, PDO::PARAM_INT);
        $statement->execute();

        $items = null;

        while ($row = $statement->fetch()) {
            $item = new ItemGetData();
            $item->itemId = (int) $row['id_item'];
            $item->position = (int) $row['position'];
            $item->note = isset($row['note']) ? htmlspecialchars_decode($row['note']) : null;
            $item->followUp = isset($row['follow_up']) ? htmlspecialchars_decode($row['follow_up']) : null;
            $item->resources = $row['resources'];
            $item->completion = $row['completion'];
            $item->completionDate = $row['completion_date'];
            $item->visible = (int) $row['visible'];
            $item->image = $row['image'];
            $item->thumbnail = $row['thumbnail'];
            $item->createdAt = (string) $row['created_at'];

            $items[] = $item;
        }
        return $items;
    }

    public function getVisibleItemsByPvId(int $pvId): ?array
    {
        $query = "SELECT i.*
        FROM item i
        INNER JOIN pv_has_item phi
        ON phi.item_id = i.id_item
        WHERE phi.pv_id =:pv_id
        AND i.visible = 1";

        // $query = "SELECT i.*, l.id_lot, l.name as lot_name
        // FROM item i
        // INNER JOIN pv_has_item phi ON phi.item_id = i.id_item
        // INNER JOIN item_has_lot ihl ON ihl.item_id = i.id_item
        // INNER JOIN lot l ON l.id_lot = ihl.lot_id
        // WHERE phi.pv_id =2";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('pv_id', $pvId, PDO::PARAM_INT);
        $statement->execute();

        while ($row = $statement->fetch()) {
            $item = new ItemGetData();
            $item->itemId = (int) $row['id_item'];
            $item->position = (int) $row['position'];
            $item->note = isset($row['note']) ? htmlspecialchars_decode($row['note']) : null;
            $item->followUp = isset($row['follow_up']) ? htmlspecialchars_decode($row['follow_up']) : null;
            $item->resources = $row['resources'];
            $item->completion = $row['completion'];
            $item->completionDate = $row['completion_date'];
            $item->visible = (int) $row['visible'];
            $item->image = $row['image'];
            $item->thumbnail = $row['thumbnail'];
            $item->createdAt = (string) $row['created_at'];

            $items[] = $item;
        }
        if (isset($items)) {
            return $items;
        } else {
            return null;
        }
    }

    public function getAllItems(): array
    {
        $query = "SELECT * FROM item";

        $statement = $this->connection->prepare($query);
        $statement->execute();

        while ($row = $statement->fetch()) {
            $item = new ItemGetData();
            $item->itemId = (int) $row['id_item'];
            $item->position = (int) $row['position'];
            $item->note = isset($row['note']) ? htmlspecialchars_decode($row['note']) : null;
            $item->followUp = isset($row['follow_up']) ? htmlspecialchars_decode($row['follow_up']) : null;
            $item->resources = $row['resources'];
            $item->completion = $row['completion'];
            $item->completionDate = $row['completion_date'];
            $item->visible = (int) $row['visible'];
            $item->image = $row['image'];
            $item->thumbnail = $row['thumbnail'];
            $item->createdAt = (int) $row['created_at'];

            $items[] = $item;
        }

        return (array) $items;
    }

    public function getLotsForItems(array $items): array
    {
        foreach ($items as $item) {
            $query = "SELECT l.* FROM lot l
            INNER JOIN item_has_lot ihl ON ihl.lot_id = l.id_lot
            INNER JOIN item i on i.id_item = ihl.item_id
            WHERE i.id_item =:itemId";

            $statement = $this->connection->prepare($query);
            $statement->bindValue("itemId", $item->itemId, PDO::PARAM_INT);
            $statement->execute();
            while ($row = $statement->fetch()) {
                $result = new LotGetData();
                $result->lotId = (int) $row['id_lot'];
                $result->name = (string) $row['name'];

                $item->lots[] = $result;
            }
            $itemsToReturn[] = $item;
        }

        return (array) $itemsToReturn;
    }

    public function getLotsForItem(ItemGetData $item): ItemGetData
    {
        $query = "SELECT l.* FROM lot l
            INNER JOIN item_has_lot ihl ON ihl.lot_id = l.id_lot
            INNER JOIN item i on i.id_item = ihl.item_id
            WHERE i.id_item =:itemId";

        $statement = $this->connection->prepare($query);
        $statement->bindValue("itemId", $item->itemId, PDO::PARAM_INT);
        $statement->execute();
        while ($row = $statement->fetch()) {
            $result = new LotGetData();
            $result->lotId = (int) $row['id_lot'];
            $result->name = (string) $row['name'];

            $item->lots[] = $result;
        }

        return $item;
    }

    public function getAllItemsFromPvHasItem(int $pvId): array
    {
        $query = "SELECT pHI.* FROM pv_has_item pHI
        INNER JOIN item i ON i.id_item = pHI.item_id
        WHERE pHI.pv_id =:pvId
        AND visible=1";

        //TODO: ajouter le visible

        $statement = $this->connection->prepare($query);
        $statement->bindValue("pvId", $pvId, PDO::PARAM_INT);
        $statement->execute();

        $result = [];
        while ($row = $statement->fetch()) {
            $pHI = new PvHasItemData;
            $pHI->itemId = (int) $row['item_id'];
            $pHI->pvId = (int) $row['pv_id'];

            $result[] = $pHI;
        }

        return $result;
    }
}
