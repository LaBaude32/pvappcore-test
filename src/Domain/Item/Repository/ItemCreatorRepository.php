<?php

namespace App\Domain\Item\Repository;

use PDO;
use App\Domain\Item\Data\ItemCreateData;

/**
 * Repository.
 */
class ItemCreatorRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Insert lot row.
     *
     * @param ItemCreateData $lot The affaire
     *
     * @return int The new ID
     */
    public function insertItem(ItemCreateData $item): int
    {
        $row = [
            'position' => $item->position,
            'note' => $item->note,
            'follow_up' => $item->followUp,
            'resources' => $item->resources,
            'completion' => $item->completion,
            'completion_date' => $item->completionDate,
            'visible' => $item->visible,
            'image' => $item->image,
            'thumbnail' => $item->thumbnail
        ];

        $query = "INSERT INTO item SET
                position=:position,
                note=:note,
                follow_up=:follow_up,
                resources=:resources,
                completion=:completion,
                completion_date=:completion_date,
                visible=:visible,
                image=:image,
                thumbnail=:thumbnail";

        $this->connection->prepare($query)->execute($row);

        return (int) $this->connection->lastInsertId();
    }

    /**
     * Insert lot row.
     *
     * @param ItemCreateData $lot The affaire
     *
     * @return int The new ID
     */
    public function insertPvHasItem(array $ids): int
    {
        $itemId = $ids['itemId'];
        $pvId = $ids['pvId'];


        $row = [
            'pv_id' => $pvId,
            'item_id' => $itemId
        ];

        $query = "INSERT INTO pv_has_item SET
                pv_id=:pv_id,
                item_id=:item_id";

        $this->connection->prepare($query)->execute($row);

        return (int) $this->connection->lastInsertId();
    }

    public function insertPvHasItemToNewPv(array $data)
    {
        foreach ($data as $value) {
            $row = [
                'pv_id' => $value->pvId,
                'item_id' => $value->itemId
            ];

            $rows[] = $row;
        }

        $query = "INSERT INTO pv_has_item SET
                pv_id=:pv_id,
                item_id=:item_id";

        $statement = $this->connection->prepare($query);

        foreach ($rows as $row) {
            $statement->execute($row);
        }
    }
}
