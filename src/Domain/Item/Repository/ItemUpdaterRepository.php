<?php

namespace App\Domain\Item\Repository;

use PDO;
use App\Domain\Item\Data\ItemGetData;

/**
 * Repository.
 */
class ItemUpdaterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Insert lot row.
     *
     * @param ItemGetData $lot The affaire
     *
     * @return int The new ID
     */
    public function updateItem(ItemGetData $item)
    {
        $row = [
            'id_item' => $item->itemId,
            'position' => $item->position,
            'note' => $item->note,
            'follow_up' => $item->followUp,
            'resources' => $item->resources,
            'completion' => $item->completion,
            'completion_date' => $item->completionDate,
            'visible' => $item->visible,
            'image' => $item->image,
            'thumbnail' => $item->thumbnail
        ];

        $query = "UPDATE item SET
                position=:position,
                note=:note,
                follow_up=:follow_up,
                resources=:resources,
                completion=:completion,
                completion_date=:completion_date,
                visible=:visible,
                image=:image,
                thumbnail=:thumbnail
                WHERE id_item=:id_item";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('id_item', $item->itemId, PDO::PARAM_INT);
        if ($item->image) {
            $statement->bindValue('image', $item->image, PDO::PARAM_STR);
        } else {
            $statement->bindValue('image', null, PDO::PARAM_NULL);
        }
        $statement->execute($row);
    }

    /**
     * Insert lot row.
     *
     * @param ItemGetData $lot The affaire
     *
     * @return int The new ID
     */
    public function updateVisible(ItemGetData $item)
    {
        $row = [
            'id_item' => $item->itemId,
            'visible' => $item->visible
        ];

        $query = "UPDATE item SET
                visible=:visible
                WHERE id_item=:id_item";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('id_item', $item->itemId, PDO::PARAM_INT);
        $statement->execute($row);
    }

    /**
     * Update an image
     *
     * @param ItemGetData $item
     * @return void
     */
    function updateImage(ItemGetData $item): void
    {
        $query = "UPDATE item SET
                 image=:image
                 WHERE id_item=:id_item";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('id_item', $item->itemId, PDO::PARAM_INT);
        $statement->bindValue('image', $item->image, PDO::PARAM_STR);
        $statement->execute();
    }
}
