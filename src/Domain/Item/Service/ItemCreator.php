<?php

namespace App\Domain\Item\Service;

use UnexpectedValueException;
use App\Domain\Item\Data\ItemCreateData;
use App\Domain\Item\Repository\ItemCreatorRepository;

/**
 * Service.
 */
final class ItemCreator
{
    /**
     * @var ItemCreatorRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param ItemCreatorRepository $repository The repository
     */
    public function __construct(ItemCreatorRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Create a new Affaire.
     *
     * @param ItemCreateData $Affaire The Affaire data
     *
     * @return int The new Affaire ID
     */
    public function createItem(ItemCreateData $item): int
    {
        // Validation
        if (empty($item->position)) {
            throw new UnexpectedValueException('position required');
        }

        if (!isset($item->visible)) {
            throw new UnexpectedValueException('visible required');
        }

        if (empty($item->pvId)) {
            throw new UnexpectedValueException('pv_id required');
        }

        // Insert item
        $itemId = $this->repository->insertItem($item);

        $ids = ["pvId" => $item->pvId, "itemId" => $itemId];

        $this->repository->insertPvHasItem($ids);

        // Logging here: item created successfully

        return (int) $itemId;
    }

    public function addItemsToNewPv(array $data)
    {
        if (empty($data)) {
            throw new UnexpectedValueException('data required');
        }

        $this->repository->insertPvHasItemToNewPv($data);
    }
}
