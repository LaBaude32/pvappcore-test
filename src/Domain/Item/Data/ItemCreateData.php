<?php

namespace App\Domain\Item\Data;

/**
 * @OA\Schema()
 * 
 * ItemCreateData
 */
final class ItemCreateData
{
    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $position;

    /** 
     * @OA\Property(example="Ceci est une note")
     * 
     * @var string|null
     */
    public $note;

    /** 
     * @OA\Property(title="suite à donner", example="Suite à donner")
     * 
     * @var string|null
     */
    public $followUp;

    /** 
     * @OA\Property(example="Ceci est une ressource")
     * 
     * @var string|null
     * TODO: peut-etre transformer en ENUM ici et dans la structure de la BDD
     */
    public $resources;

    /** 
     * @OA\Property(title="échéance", example="Donner une échéance")
     * 
     * @var string|null
     */
    public $completion;

    /** 
     * @OA\Property(title="date de l'échéance", format="date")
     * 
     * @var \DateTimeInterface|null 
     */
    public $completionDate;

    /** 
     * @OA\Property()
     * 
     * @var bool 
     * */
    public $visible;

    /**
     * @OA\Property(format="binary")
     * 
     * 
     * @var string|null
     */
    public $image;

    /**
     * @OA\Property()
     * 
     * 
     * @var string|null
     */
    public $thumbnail;

    /** 
     * @OA\Property()
     * 
     * @var int 
     * 
     */
    public $pvId;

    /** 
     * @OA\Property(
     *      type="array", 
     *      title="liste des id lots associés",
     *      @OA\Items(
     *          type="integer",
     *          title="lotId"
     *      )
     * )
     * 
     * @var array
     */
    public $lotsIds;
}
