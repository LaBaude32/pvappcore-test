<?php

namespace App\Domain\Item\Data;

/**
 * @OA\Schema()
 * 
 * ItemGetData
 */
final class ItemGetData
{
    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $itemId;

    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $position;

    /** 
     * @OA\Property(title="note", example="Ceci est une note")
     * 
     * @var string|null
     */
    public $note;

    /** 
     * @OA\Property(title="suite à donner", example="Suite à donner")
     * 
     * @var string|null
     */
    public $followUp;

    /** 
     * @OA\Property(title="ressource", example="Ceci est une ressource")
     * 
     * @var string|null
     */
    public $resources;

    /** 
     * @OA\Property(title="échéance", example="Donner une échéance")
     * 
     * @var string|null
     */
    public $completion;

    /** 
     * @OA\Property(title="date de l'échéance", format="date")
     * 
     * @var \DateTimeInterface|null 
     */
    public $completionDate;

    /** 
     * @OA\Property(title="visible")
     * 
     * @var bool 
     * */
    public $visible;

    /**
     * @OA\Property()
     * 
     * 
     * @var string|null
     */
    public $image;

    /**
     * @OA\Property()
     * 
     * 
     * @var string|null
     */
    public $thumbnail;

    /** 
     * @OA\Property(title="date de création", format="date")
     * 
     * @var \DateTimeInterface 
     */
    public $createdAt;

    /** 
     * @OA\Property(
     *      type="array", 
     *      title="lots associés",
     *      @OA\Items(ref="#/components/schemas/LotGetData")
     * )
     * 
     * @var array
     */
    public $lots;

    /** 
     * @OA\Property(
     *      type="array", 
     *      title="liste des id lots associés",
     *      @OA\Items(
     *          type="integer",
     *          title="lotId"
     *      )
     * )
     * 
     * @var array
     */
    public $lotsIds;
}
