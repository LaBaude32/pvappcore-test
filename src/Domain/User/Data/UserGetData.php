<?php

namespace App\Domain\User\Data;

/**
 * @OA\Schema()
 * 
 * UserUserGetData
 */
final class UserGetData extends UserCreateData
{
    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $userId;
}
