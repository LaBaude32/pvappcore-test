<?php

namespace App\Domain\User\Data;

/**
 * @OA\Schema()
 * 
 * UserCreateData
 */
class UserCreateData
{
    /** 
     * @OA\Property(example="baudouin@ik.me")
     * 
     * @var string 
     */
    public $email;

    /** 
     * @OA\Property(title="mot de passe")
     * 
     * @var string 
     */
    public $password;

    /** 
     * @OA\Property(example="Baudouin")
     * 
     * @var string 
     */
    public $firstName;

    /** 
     * @OA\Property(example="Coupey")
     * 
     * @var string 
     */
    public $lastName;

    /** 
     * @OA\Property(example="0123456789")
     * 
     * @var string 
     */ 
    public $phone;

    /** 
     * @OA\Property(example="Maitre d'oeuvre")
     * 
     * @var string|null
    */
    public $userGroup;

        /** 
     * @OA\Property(example="Maitre d'ouvrage")
     * 
     * @var string|null
    */
    public $userFunction;

            /** 
     * @OA\Property(example="Agece Casals")
     * 
     * @var string|null
    */
    public $organism;
}
