<?php

namespace App\Domain\User\Service;

use App\Domain\User\Data\UserGetData;
use App\Domain\User\Repository\UserGetterRepository;

final class UserAuth
{
  /**
   * @var UserGetterRepository
   */
  private $repository;

  /**
   * The constructor.
   *
   * @param UserGetterRepository $repository The repository
   */
  public function __construct(UserGetterRepository $repository)
  {
    $this->repository = $repository;
  }

  public function authenticate(string $email, string $password): bool
  {
    $user = $this->repository->getUserByEmail($email);

    $result = ($email === $user->email && password_verify($password, $user->password));

    return (bool) $result;
  }
}
