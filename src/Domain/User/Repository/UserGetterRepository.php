<?php

namespace App\Domain\User\Repository;

use PDO;
use App\Domain\User\Data\UserGetData;
use App\Domain\User\Data\UserStatusGetData;

/**
 * Repository.
 */
class UserGetterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Get All Users.
     *
     * @return array All the users
     */
    public function getAllUsers(): array
    {
        $sql = "SELECT * FROM user";

        $statement = $this->connection->prepare($sql);
        $statement->execute();

        while ($row = $statement->fetch()) {
            $user = new UserGetData();
            $user->userId = (int) $row['id_user'];
            $user->email = (string) $row['email'];
            $user->firstName = (string) $row['first_name'];
            $user->lastName = (string) $row['last_name'];
            $user->phone = (string) $row['phone'];
            $user->userGroup = (string) htmlspecialchars_decode($row['user_group']);
            $user->userFunction = (string) $row['user_function'];
            $user->organism = (string) $row['organism'];

            $users[] = $user;
        }
        return (array) $users;
    }

    /**
     * getUserById
     *
     * @param int $id_user
     *
     * @return UserStatusGetData
     */
    public function getUserWithStatusById(int $id_user): UserStatusGetData
    {
        $query = "SELECT u.*, phu.status_PAE
        FROM user u
        INNER JOIN pv_has_user phu
        ON phu.user_id = u.id_user
        WHERE id_user=:id_user";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('id_user', $id_user, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();
        $user = new UserStatusGetData();
        $user->userId = (int) $row['id_user'];
        $user->email = (string) $row['email'];
        // $user->password = (string) $row['password'];
        $user->firstName = (string) $row['first_name'];
        $user->lastName = (string) $row['last_name'];
        $user->phone = (string) $row['phone'];
        $user->userGroup = (string) htmlspecialchars_decode($row['user_group']);
        $user->userFunction = (string) $row['user_function'];
        $user->organism = (string) $row['organism'];
        $user->statusPAE = (string) $row['status_PAE'];

        return $user;
    }

    /**
     * getUserById
     *
     * @param int $id_user
     *
     * @return UserGetData
     */
    public function getUserById(int $id_user): UserGetData
    {
        $query = "SELECT * FROM user WHERE id_user=:id_user";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('id_user', $id_user, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();
        $user = new UserGetData();
        $user->userId = (int) $row['id_user'];
        $user->email = (string) $row['email'];
        $user->firstName = (string) $row['first_name'];
        $user->lastName = (string) $row['last_name'];
        $user->phone = (string) $row['phone'];
        $user->userGroup = (string) htmlspecialchars_decode($row['user_group']);
        $user->userFunction = (string) $row['user_function'];
        $user->organism = (string) $row['organism'];

        return $user;
    }

    public function getUserByEmail(string $email): UserGetData
    {
        $query = "SELECT * FROM user WHERE email=:email";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('email', $email, PDO::PARAM_STR);
        $statement->execute();

        $row = $statement->fetch();
        $user = new UserGetData();
        $user->userId = (int) $row['id_user'];
        $user->email = (string) $row['email'];
        $user->password = (string) $row['password'];
        $user->firstName = (string) $row['first_name'];
        $user->lastName = (string) $row['last_name'];
        $user->phone = (string) $row['phone'];
        $user->userGroup = (string) htmlspecialchars_decode($row['user_group']);
        $user->userFunction = (string) $row['user_function'];
        $user->organism = (string) $row['organism'];

        return $user;
    }
}
