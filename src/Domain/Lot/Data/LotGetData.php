<?php

namespace App\Domain\Lot\Data;

/**
 * @OA\Schema()
 * 
 * LotGetData
 */
final class LotGetData extends LotCreateData
{
    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $lotId;
}
