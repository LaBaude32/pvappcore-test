<?php

declare(strict_types=1);

namespace App\Domain\Lot\Data;

/**
 * @OA\Schema()
 * 
 * PvHasLotGetData
 */
final class LotProgressGetData extends LotCreateData
{
    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $lotId;

    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $pvId;

    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $progress;

    /** 
     * @OA\Property()
     * 
     * @var string|null
     */
    public $alreadyDone;
}
