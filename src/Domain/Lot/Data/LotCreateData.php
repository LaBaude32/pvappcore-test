<?php

namespace App\Domain\Lot\Data;

/**
 * @OA\Schema()
 * 
 * LotCreateData
 */
class LotCreateData
{
    /** 
     * @OA\Property(example="Plantations")
     * 
     * @var string 
     */
    public $name;

    /** 
     * @OA\Property()
     * 
     * @var int 
     */
    public $affairId;
}
