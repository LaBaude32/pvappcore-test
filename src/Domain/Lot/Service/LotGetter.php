<?php

namespace App\Domain\Lot\Service;

use App\Domain\Lot\Data\LotGetData;
use UnexpectedValueException;
use App\Domain\Lot\Repository\LotGetterRepository;

/**
 * Service.
 */
final class LotGetter
{
    /**
     * @var LotGetterRepository
     */
    private $repository;

    /**
     * The constructor.
     *
     * @param LotGetterRepository $repository The repository
     */
    public function __construct(LotGetterRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get all the lots.
     *
     * @return array All the lots
     */
    public function getLotById(int $id): LotGetData
    {
        // Validation
        if (empty($id)) {
            throw new UnexpectedValueException('id required');
        }

        // Get All lots
        $lot = $this->repository->getLotById($id);

        return $lot;
    }

    /**
     * Get all the lots.
     *
     * @return array|null All the lots or nothing
     */
    public function getLotByAffairId(int $id): array|null
    {
        // Validation
        if (empty($id)) {
            throw new UnexpectedValueException('id required');
        }

        // Get All lots
        $lots = $this->repository->getLotByAffairId($id);

        return $lots;
    }

    public function getLotsByPvId(int $pvId): array|null
    {
        // Validation
        if (empty($pvId)) {
            throw new UnexpectedValueException('pv id required');
        }

        // Get All lots
        $lots = $this->repository->getPvHasLots($pvId);

        return $lots;
    }
}
