<?php

namespace App\Domain\Lot\Service;

use UnexpectedValueException;
use App\Domain\Lot\Data\LotGetData;
use App\Domain\Lot\Data\LotProgressGetData;
use App\Domain\Lot\Repository\LotGetterRepository;
use App\Domain\Lot\Repository\LotUpdaterRepository;

/**
 * Service.
 */
final class LotUpdater
{
    /**
     * @var LotUpdaterRepository
     */
    private $repository;

    /**
     * @var LotGetterRepository
     */
    private $getterRepository;

    /**
     * The constructor.
     *
     * @param LotUpdaterRepository $repository
     * @param LotGetterRepository $getterRepository
     */
    public function __construct(LotUpdaterRepository $repository, LotGetterRepository $getterRepository)
    {
        $this->repository = $repository;
        $this->getterRepository = $getterRepository;
    }

    /**
     * Update a Lot.
     *
     * @param LotGetData
     */
    public function updateLot(LotGetData $lot): LotGetData
    {
        // Validation
        if (empty($lot->lotId)) {
            throw new UnexpectedValueException('id required');
        }

        // Update lot
        $this->repository->updateLot($lot);
        $newLot = $this->getterRepository->getLotById($lot->lotId);

        // Logging here: lot created successfully
        return $newLot;
    }

    public function updateLotProgress(LotProgressGetData $lot): LotProgressGetData
    {
        // Validation
        if (empty($lot->lotId)) {
            throw new UnexpectedValueException('lot id required');
        }

        if (empty($lot->pvId)) {
            throw new UnexpectedValueException('pv id required');
        }

        // Update lot
        $this->repository->updatePvHasLot($lot);
        $newLot = $this->getterRepository->getLotHasPv($lot);

        // Logging here: lot created successfully
        return $newLot;
    }
}
