<?php

namespace App\Domain\Lot\Service;

use UnexpectedValueException;
use App\Domain\Lot\Data\LotCreateData;
use App\Domain\Lot\Data\LotGetData;
use App\Domain\Lot\Data\LotProgressGetData;
use App\Domain\Lot\Repository\LotCreatorRepository;
use App\Domain\Lot\Repository\LotGetterRepository;

/**
 * Service.
 */
final class LotCreator
{
    /**
     * Undocumented function
     *
     * @param LotCreatorRepository $repository
     * @param LotGetterRepository $getterRepository
     */
    private $repository;
    private $getterRepository;

    /**
     * The constructor.
     *
     * @param LotCreatorRepository $repository The repository
     * @param LotGetterRepository $getterRepository The repository
     */
    public function __construct(LotCreatorRepository $repository, LotGetterRepository $getterRepository)
    {
        $this->repository = $repository;
        $this->getterRepository = $getterRepository;
    }

    /**
     * Create a new Lot.
     *
     * @param LotCreateData $Lot The Lot data
     *
     * @return int The new Lot ID
     */
    public function createLot(LotCreateData $lot): LotGetData
    {
        // Validation
        if (empty($lot->name)) {
            throw new UnexpectedValueException('Nom required');
        }

        // Insert lot
        $lotId = $this->repository->insertLot($lot);
        $newLot = $this->getterRepository->getLotById($lotId);

        // Logging here: lot created successfully

        return $newLot;
    }

    /**
     * Create a new Lot.
     *
     * @param LotCreateData $Lot The Lot data
     *
     * @return int The new Lot ID
     */
    public function linkLotsToItem(array $lotsIds, int $itemId): array
    {
        // Validation
        if (empty($lotsIds[0])) {
            throw new UnexpectedValueException('lots required');
        }

        if (empty($itemId)) {
            throw new UnexpectedValueException('item id required');
        }

        // Insert lot
        $lotsIds = $this->repository->linkLotsToItem($lotsIds, $itemId);

        return (array) $lotsIds;
    }

    public function linkLotToPv(LotProgressGetData $lot): LotProgressGetData
    {

        if (empty($lot->pvId)) {
            throw new UnexpectedValueException('pv id required');
        }

        if (empty($lot->lotId)) {
            throw new UnexpectedValueException('lot id required');
        }

        $this->repository->linkLotToPv($lot);
        $newLot = $this->getterRepository->getLotHasPv($lot);

        return $newLot;
    }
}
