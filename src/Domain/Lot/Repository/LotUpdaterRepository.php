<?php

namespace App\Domain\Lot\Repository;

use PDO;
use App\Domain\Lot\Data\LotGetData;
use App\Domain\Lot\Data\LotProgressGetData;

/**
 * Repository.
 */
class LotUpdaterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Update a lot.
     *
     * @param LotGetData $lot the Lot
     */
    public function updateLot(LotGetData $lot)
    {
        $row = [
            'lotId' => $lot->lotId,
            'name' => $lot->name,
        ];

        $sql = "UPDATE lot SET
                name=:name,
                WHERE id_lot=:lotId";

        $statement = $this->connection->prepare($sql);
        $statement->bindValue('lotId', $lot->lotId, PDO::PARAM_INT);
        $statement->execute($row);
    }

    /**
     * Update a lot.
     *
     * @param PvHasLotGetData $lot the Lot
     */
    public function updatePvHasLot(LotProgressGetData $lot)
    {
        // $sql = "UPDATE pv_has_lot SET
        //         progress=:progress,
        //         already_done=:alreadyDone
        //         WHERE id_lot=:lotId AND id_pv=:pvId";

        $sql = "INSERT INTO pv_has_lot SET
                id_lot=:lotId,
                id_pv=:pvId,
                progress=:progress,
                already_done=:alreadyDone
                ON DUPLICATE KEY UPDATE progress=:progress, already_done=:alreadyDone";

        $statement = $this->connection->prepare($sql);
        $statement->bindValue('lotId', $lot->lotId, PDO::PARAM_INT);
        $statement->bindValue('pvId', $lot->pvId, PDO::PARAM_INT);
        $statement->bindValue('progress', $lot->progress, PDO::PARAM_INT);
        $statement->bindValue('alreadyDone', $lot->alreadyDone);
        $statement->execute();
    }
}
