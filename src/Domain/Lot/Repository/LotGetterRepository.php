<?php

namespace App\Domain\Lot\Repository;

use PDO;
use App\Domain\Lot\Data\LotGetData;
use App\Domain\Lot\Data\LotProgressGetData;

/**
 * Repository.
 */
class LotGetterRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Récuperer les lots d'une affaire
     *
     * @param integer $id_affair
     * @return array|null
     */
    public function getLotByAffairId(int $id_affair): array|null
    {
        $sql = "SELECT * FROM lot WHERE affair_id=:id";

        $statement = $this->connection->prepare($sql);
        $statement->bindValue('id', $id_affair, PDO::PARAM_INT);
        $statement->execute();

        $lots = null;

        while ($row = $statement->fetch()) {
            $lot = new LotGetData();
            $lot->lotId = (int) $row['id_lot'];
            $lot->name = (string) $row['name'];
            //TODO: a voir si on join pas avec pv_has_lot pour avoir les avancements

            $lots[] = $lot;
        }
        return $lots;
    }

    public function getLotById(int $lotId): LotGetData
    {
        $sql = "SELECT * FROM lot WHERE id_lot=:id";

        $statement = $this->connection->prepare($sql);
        $statement->bindValue('id', $lotId, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();
        $lot = new LotGetData();
        $lot->lotId = (int) $row['id_lot'];
        $lot->name = (string) $row['name'];
        $lot->affairId = (int) $row['affair_id'];

        return $lot;
    }

    /**
     * recupération des lots liés à un pv avec leur progression
     *
     * @param integer $pvId
     * @return array|null
     */
    public function getPvHasLots(int $pvId): array|null
    {
        $query = "SELECT * FROM lot 
        INNER JOIN pv_has_lot phl
        ON phl.id_lot = lot.id_lot
        WHERE phl.id_pv = :pvId;";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('pvId', $pvId, PDO::PARAM_INT);
        $statement->execute();

        $lots = NULL;

        while ($row = $statement->fetch()) {
            $lot = new LotProgressGetData();
            $lot->lotId = (int) $row['id_lot'];
            $lot->pvId = (int) $row['id_pv'];
            $lot->name = (string) $row['name'];
            $lot->affairId = (int) $row['affair_id'];
            $lot->progress = (int) $row['progress'];
            $lot->alreadyDone = $row['already_done'];

            $lots[] = $lot;
        }
        return $lots;
    }

    /**
     * recuperer l'avancement d'un lot lié à un pv
     *
     * @param PvHasLotGetData $lot
     * @return LotProgressGetData
     */
    public function getLotHasPv(LotProgressGetData $lot): LotProgressGetData
    {
        $query = "SELECT * FROM lot 
        INNER JOIN pv_has_lot phl
        ON phl.id_lot = lot.id_lot
        WHERE phl.id_pv = :pvId AND phl.id_lot = :lotId;";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('pvId', $lot->pvId, PDO::PARAM_INT);
        $statement->bindValue('lotId', $lot->lotId, PDO::PARAM_INT);
        $statement->execute();

        $row = $statement->fetch();
        var_dump($row);
        $lot = new LotProgressGetData();
        $lot->lotId = (int) $row['id_lot'];
        $lot->pvId = (int) $row['id_pv'];
        $lot->name = (string) $row['name'];
        $lot->affairId = (int) $row['affair_id'];
        $lot->progress = (int) $row['progress'];
        $lot->alreadyDone = $row['already_done'];

        return $lot;
    }
}
