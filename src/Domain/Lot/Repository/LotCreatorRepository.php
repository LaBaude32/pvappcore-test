<?php

namespace App\Domain\Lot\Repository;

use PDO;
use App\Domain\Lot\Data\LotCreateData;
use App\Domain\Lot\Data\LotProgressGetData;

/**
 * Repository.
 */
class LotCreatorRepository
{
    /**
     * @var PDO The database connection
     */
    private $connection;

    /**
     * Constructor.
     *
     * @param PDO $connection The database connection
     */
    public function __construct(PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Insert lot row.
     *
     * @param LotCreateData $lot The affaire
     *
     * @return int The new ID
     */
    public function insertLot(LotCreateData $lot): int
    {
        $row = [
            'name' => $lot->name,
            'affairId' => $lot->affairId,
        ];

        $sql = "INSERT INTO lot SET
                name=:name,
                affair_id=:affairId";

        $this->connection->prepare($sql)->execute($row);

        return (int) $this->connection->lastInsertId();
    }

    public function linkLotsToItem(array $lotsIds, int $itemId)
    {
        foreach ($lotsIds as $lotId) {
            $row = [
                'lot_id' => $lotId,
                'item_id' => $itemId,
            ];

            $sql = "INSERT INTO item_has_lot SET
                lot_id=:lot_id,
                item_id=:item_id";

            $this->connection->prepare($sql)->execute($row);
        }
    }

    public function linkLotToPv(LotProgressGetData $lot)
    {
        $query = "INSERT INTO pv_has_lot SET
                id_lot=:lotId,
                id_pv=:pvId,
                progress=:progress,
                already_done=:alreadyDone";

        $statement = $this->connection->prepare($query);
        $statement->bindValue('lotId', $lot->lotId, PDO::PARAM_INT);
        $statement->bindValue('pvId', $lot->pvId, PDO::PARAM_INT);
        $statement->bindValue('progress', $lot->progress, PDO::PARAM_INT);
        $statement->bindValue('alreadyDone', $lot->alreadyDone);;
    }
}
