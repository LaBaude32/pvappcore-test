<?php

namespace App;

use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *      title="PvApp API", 
 *      version="1",
 *      description="Application de suivi de chantier"
 * )
 * @OA\Server(
 *      url="http://localhost:8081/api/v1"
 * )
 * 
 */

class PvApp
{
}
