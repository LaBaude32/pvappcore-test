dev:
	docker compose -f docker-compose-dev.yml up

prod:
	docker compose -f docker-compose-prod.yml up

stop-dev:
	docker compose -f docker-compose-dev.yml down

stop-prod:
	docker compose -f docker-compose-prod.yml down

doc: 
	php vendor/bin/openapi -o resources/docs/pvApp.yaml src/
	
doc-debug: 
	php vendor/bin/openapi -d -o resources/docs/pvApp.yaml src/

first-deployment:
	#install docker debian
	sudo apt-get update
	sudo apt-get install ca-certificates curl
	sudo install -m 0755 -d /etc/apt/keyrings
	sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
	sudo chmod a+r /etc/apt/keyrings/docker.asc
	echo \
		"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
		$(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
		sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
	sudo apt-get update
	sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
	
	#install lazydocker
	curl https://raw.githubusercontent.com/jesseduffield/lazydocker/master/scripts/install_update_linux.sh | bash
	echo "alias lzd='.local/bin/lazydocker'" >> ~/.bash_aliases
	
	#install PvAPP
	docker build . -f ./docker/Dockerfile -t baldwin/pvapp-core
	mkdir certs
	touch certs/acme.json
	chmod 600 certs/acme.json
	cp .env.template .env
	openssl genrsa -out private.pem 2048
	openssl rsa -in private.pem -outform PEM -pubout -out public.pem
	chmod 755 public.pem
	chmod 755 private.pem
	mkdir public/images
	sudo chown www-data:www-data public/images/
	sudo apt-get install php php-xml php-mbstring zip unzip php-zip -y
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
	php composer-setup.php
	php -r "unlink('composer-setup.php');"
	sudo mv composer.phar /usr/local/bin/composer
	composer install
	nano .env

dev-deploy:
	cp .env.template .env
	openssl genrsa -out private.pem 2048
	openssl rsa -in private.pem -outform PEM -pubout -out public.pem
	chmod 755 public.pem
	chmod 755 private.pem
	mkdir public/images
	sudo chown www-data:www-data public/images/
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
	php composer-setup.php
	php -r "unlink('composer-setup.php');"
	sudo mv composer.phar /usr/local/bin/composer
	composer install
	docker build . -f ./docker/Dockerfile -t baldwin/pvapp-core

update-deployment:
	git pull
	composer update
	docker build . -f ./docker/Dockerfile -t baldwin/pvapp-core
	docker compose -f docker-compose-prod.yml up

phoenix-status:
	docker exec pvapp_core-app-1 php vendor/bin/phoenix status

phoenix-migrate:
	docker exec pvapp_core-app-1 php vendor/bin/phoenix migrate

# phoenix-create-migration:
	# docker exec pvapp_core-app-1 php vendor/bin/phoenix create "MyFirstMigration"