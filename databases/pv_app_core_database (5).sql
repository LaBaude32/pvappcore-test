-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : db
-- Généré le : ven. 22 nov. 2024 à 19:26
-- Version du serveur : 9.0.1
-- Version de PHP : 8.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `pv_app_core_database`
--

-- --------------------------------------------------------

--
-- Structure de la table `affair`
--

CREATE TABLE `affair` (
  `id_affair` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `progress` int DEFAULT NULL,
  `meeting_type` enum('Chantier','Etude') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `affair`
--

INSERT INTO `affair` (`id_affair`, `name`, `address`, `progress`, `meeting_type`, `description`) VALUES
(13, 'Maison individuelle', 'Higueres 32700 Catstera Lectourois', 80, 'Etude', 'Descriptions'),
(15, 'Réalisation d\'une piscine', 'Higueres 32700 Castera-Lectourois', 0, 'Chantier', ''),
(16, 'Affaire de test', 'En télétravail', 0, 'Etude', ''),
(17, 'Affaire de test', 'En télétravail', 0, 'Etude', ''),
(18, 'Affaire de test', 'En télétravail', 0, 'Etude', NULL),
(19, 'Affaire de test', 'En télétravail', 0, 'Etude', NULL),
(20, 'Affaire de test', 'En télétravail', 0, 'Etude', NULL),
(21, 'Affaire de test', 'En télétravail', 0, 'Etude', NULL),
(22, 'Affaire de test', 'En télétravail 2', 55, 'Etude', 'fref'),
(23, 'Affaire de test 2', 'Ici et la garenne', 0, 'Chantier', 'Bla bla bla'),
(24, 'fezthtreyhtyru', 'htryhytrujuy', 0, 'Chantier', NULL),
(25, 'fregtezrgrethety', 'gterhtryjryujyur', 0, 'Chantier', ''),
(26, 'Lalalaf ref ezre', 'fezrgokrth freofr', 0, 'Chantier', NULL),
(27, 'Affaire de test encore', '87 rue d&#039;aboukir', NULL, 'Chantier', ''),
(28, 'dazdzeadzed', 'dezadzaedzed', NULL, 'Chantier', ''),
(29, 'dezadzad', 'dezdezdezde', NULL, 'Etude', ''),
(30, 'Affaire de test chantier sans lots', 'daezfpirjzegrtegfez', NULL, 'Chantier', '');

-- --------------------------------------------------------

--
-- Structure de la table `agenda`
--

CREATE TABLE `agenda` (
  `id_agenda` int NOT NULL,
  `position` int NOT NULL DEFAULT '0',
  `title` varchar(535) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pv_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `agenda`
--

INSERT INTO `agenda` (`id_agenda`, `position`, `title`, `pv_id`) VALUES
(13, 0, 'lala', 65),
(14, 1, 'dazpofjzerg', 65),
(15, 2, 'dazeofpjkzer 6547', 65),
(19, 2, 'odj 1', 70),
(20, 0, 'deded 33', 70),
(22, 1, 'deazd', 70);

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

CREATE TABLE `item` (
  `id_item` int NOT NULL,
  `position` int NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `follow_up` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `resources` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `completion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `completion_date` timestamp NULL DEFAULT NULL,
  `visible` tinyint NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `item`
--

INSERT INTO `item` (`id_item`, `position`, `note`, `follow_up`, `resources`, `completion`, `completion_date`, `visible`, `image`, `thumbnail`, `created_at`) VALUES
(18, 1, 'Un levé topographique est necessaire pour lancer la mission', 'demande de devis à faire (levé topo 3D + décpoupage en volume)', 'client', 'Urgent', '2020-05-24 22:00:00', 1, NULL, NULL, '2020-05-23 09:40:17'),
(19, 2, 'L\'AVP doit aller aussi loin que possible dans la démarche HQE', '', '', '', '2020-05-15 22:00:00', 1, NULL, NULL, '2020-05-23 09:41:00'),
(20, 3, 'Un étage au dessus de la cuisine est souhaité', 'Vérifier la faisabilité règlementaire au PLU', 'CASALS', 'Urgent', '2020-05-23 22:00:00', 1, NULL, NULL, '2020-05-23 09:42:13'),
(21, 4, 'dggsd', '', '', '', NULL, 1, NULL, NULL, '2020-05-23 10:13:50'),
(22, 4, 'Contacter la SAUR pour modification de branchement', '', 'CASALS', 'A faire', '2020-07-13 22:00:00', 1, NULL, NULL, '2020-05-23 10:17:12'),
(23, 1, '', 'Le contrat d\'honoraires est à renvoyer signé à CASALS', 'Client', 'Urgent', NULL, 0, NULL, NULL, '2020-05-24 19:24:45'),
(24, 2, 'L\'organigramme présenté ce jour est validé à exception de la position de la piscine', 'A modifier pour la prochaine réunion, en phase ESQ+', 'CASALS', 'A faire', '2020-05-30 22:00:00', 1, NULL, NULL, '2020-05-24 19:26:36'),
(25, 1, 'Le levé topo existe', 'A transmettre à l\'entreprise', 'CASALS', 'Urgent', '2020-05-25 22:00:00', 1, NULL, NULL, '2020-05-24 19:48:11'),
(26, 2, '', 'Transmettre le plan d\'EXE avec les notes de calcul ', 'entreprise DONIS', 'A faire', '2020-05-30 22:00:00', 1, NULL, NULL, '2020-05-24 19:50:18'),
(27, 5, 'test 2', 'de gthy', '', '', NULL, 1, NULL, NULL, '2020-05-24 20:41:05'),
(28, 1, 'test 1', '', '', '', NULL, 1, NULL, NULL, '2020-05-24 22:13:32'),
(29, 1, 'UNe note', 'Suite', '', 'A faire', '2020-05-12 22:00:00', 0, NULL, NULL, '2020-05-25 16:56:44'),
(30, 2, 'Une deuxieme note', '', '', '', NULL, 1, NULL, NULL, '2020-05-25 16:57:43'),
(31, 1, 'Etanchélité de la fontaine', 'Utiliser de la résine', 'BTP entreprise', 'Urgent', '2020-05-26 22:00:00', 1, NULL, NULL, '2020-05-26 10:19:07'),
(32, 1, 'Agrandire legerement la fosse pour la plomberie', 'Verifier avec l\'entreprise de plomberie', 'BTP', 'Urgent', NULL, 1, NULL, NULL, '2020-05-26 10:26:16'),
(33, 2, 'Préparation des végétaux', '', 'végétal SARL', 'A faire', NULL, 1, NULL, NULL, '2020-05-26 10:26:44'),
(34, 3, '', 'Transmettre les plans de plomberie à l\'entreprise de BTP', 'Plomberie René ', 'Fait', NULL, 0, NULL, NULL, '2020-05-26 10:27:31'),
(35, 2, 'Peinture de la fontine', '', '', '', NULL, 1, NULL, NULL, '2020-05-26 11:31:27'),
(36, 4, 'Verfier la comptaibilité des plante avec l\'etanchelité', '', '', '', NULL, 1, NULL, NULL, '2020-05-26 11:32:38'),
(37, 3, 'Un item', 'suite', '', 'A faire', '2020-05-12 22:00:00', 1, NULL, NULL, '2020-05-26 12:53:51'),
(38, 3, 'note', 'iftzoigf', '', 'A faire', '2020-06-08 22:00:00', 1, NULL, NULL, '2020-06-02 08:40:01'),
(39, 5, 'fuhrôicn', '', '', '', NULL, 1, NULL, NULL, '2020-06-02 08:51:55'),
(40, 1, 'fref', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2023-11-11 17:18:36'),
(41, 1, 'test', 'dergrte', 'fsref', 'Urgent', '2023-11-15 00:00:00', 1, NULL, NULL, '2023-11-12 13:53:39'),
(42, 2, 'getgtr', 'grtg', 'gsrt', 'A faire', '2023-11-06 00:00:00', 1, NULL, NULL, '2023-11-12 13:53:48'),
(43, 3, 'tgrsg', 'gtr', 'gtsgt', 'Urgent', '2023-11-06 00:00:00', 1, NULL, NULL, '2023-11-12 13:53:57'),
(44, 1, 'rthtyhl,fre\nezrunfoe - eazon\nezarjfghrez', 'trujyuj', 'htryjt', 'Urgent', '2023-11-21 00:00:00', 1, NULL, NULL, '2023-11-26 22:16:12'),
(45, 2, 'dyjtyr', 'rujyutj', 'htyrjtyu', 'Fait', '2023-11-06 00:00:00', 1, '', '', '2023-11-26 22:16:19'),
(46, 3, 'hrtyjyuj', 'rthtryj', 'ujyutj', 'Urgent', '2023-11-07 00:00:00', 1, NULL, NULL, '2023-11-26 22:16:26'),
(47, 1, 'ergrteh', 'getryhryujdede', 'ethgyt', 'A faire', '2023-11-29 00:00:00', 1, 'b6b3001b-092e-42e9-b892-9883953ee672.png', NULL, '2023-11-27 20:53:30'),
(48, 2, 'kdtgg', 'rshtry', 'ddddd', 'A faire', '2023-11-30 00:00:00', 0, NULL, NULL, '2023-11-27 20:53:39'),
(49, 3, 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l&#039;imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n&#039;a pas fait que survivre cinq siècles, mais s&#039;est aussi adapté à la bureautique informatique, sans que son contenu n&#039;en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.', '&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;', 'ggd', 'A faire', '2023-11-14 00:00:00', 1, NULL, NULL, '2023-11-27 20:53:54'),
(50, 1, NULL, NULL, NULL, 'undefined', NULL, 0, '4bdc029d-f8b7-4bd3-bc5e-7594877abfca.png', NULL, '2024-01-01 20:14:46'),
(51, 1, NULL, NULL, NULL, NULL, NULL, 1, '51df85c1-8b3e-49cc-8d52-9dc4207beda3.png', NULL, '2024-01-01 20:16:14'),
(52, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, '', '2024-01-01 20:16:57'),
(53, 2, 'ezghtreyhty', NULL, NULL, 'Urgent', NULL, 1, NULL, NULL, '2024-01-01 21:24:14'),
(54, 3, 'fegtrgrt', NULL, NULL, 'Urgent', NULL, 1, '217d8cd2-67fa-4aff-9e37-e8c5dd0c12b7.jpg', NULL, '2024-01-01 21:27:52'),
(55, 4, 'dezde', 'dezd', NULL, 'A faire', NULL, 1, NULL, NULL, '2024-01-01 21:38:05'),
(59, 5, 'gtegythytr', 'dezad', 'ded', 'A faire', '2024-10-16 00:00:00', 1, NULL, NULL, '2024-02-25 16:04:12'),
(60, 1, 'dede', 'deede', NULL, 'A faire', NULL, 1, NULL, NULL, '2024-08-12 22:44:12'),
(61, 2, 'ferf', 'frezfe', 'fr', 'Urgent', NULL, 1, NULL, NULL, '2024-08-13 20:01:55'),
(62, 3, 'dd', 'fddf', 'dqzed', 'A faire', NULL, 1, NULL, NULL, '2024-08-13 20:03:58'),
(63, 4, 'cc', NULL, NULL, 'Urgent', NULL, 1, NULL, NULL, '2024-08-13 20:05:07'),
(64, 5, 'xee', NULL, 'dd', NULL, NULL, 1, NULL, NULL, '2024-08-13 20:06:34'),
(65, 6, 'freaf', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-14 20:30:40'),
(66, 6, 'freaf', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-14 20:31:24'),
(67, 7, 'dd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-14 20:43:28'),
(68, 8, 'dezd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-14 20:45:26'),
(69, 9, 'refr', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-14 20:47:08'),
(70, 10, 'xx', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-14 21:05:48'),
(71, 11, 'dedd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-14 21:07:44'),
(72, 12, 'cdcs', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-14 21:11:01'),
(73, 13, 'dezad', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-14 21:13:31'),
(74, 14, 'dd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-15 20:29:52'),
(75, 15, 'ss', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-15 20:35:27'),
(76, 16, 'ff', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-15 20:36:27'),
(77, 17, 'dd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-15 20:44:14'),
(78, 18, 'dd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-15 20:49:13'),
(79, 19, 'cdsc', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-15 20:51:33'),
(80, 20, 'dffre', NULL, NULL, NULL, NULL, 1, '878a8f86-f48d-4444-9972-6660a7c95dab.png', NULL, '2024-08-15 20:53:14'),
(81, 21, 'fref', NULL, NULL, NULL, NULL, 1, '724cc541-cf5e-463d-9255-342f42ef58d4.png', NULL, '2024-08-21 18:21:15'),
(82, 22, 'fqefe', NULL, NULL, NULL, NULL, 1, '1e386390-e4de-4e3e-851f-5630ec85eea9.png', NULL, '2024-08-21 18:23:20'),
(83, 4, 'frezf', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:37:21'),
(84, 4, 'dezd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:38:51'),
(85, 5, 'dez', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:39:13'),
(86, 6, 'de', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:39:59'),
(87, 7, 'dede', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:51:08'),
(88, 8, 'dzDEZd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:51:25'),
(89, 9, 'dezdz', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:55:05'),
(90, 10, 'dezd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:55:26'),
(91, 11, 'dezadzea', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:55:45'),
(92, 12, 'dezd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:56:07'),
(93, 13, 'dzaedz', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:57:56'),
(94, 14, 'dzeFERZTGTREYH', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:58:22'),
(95, 15, 'xsqXQE', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 19:59:41'),
(96, 16, 'dzaed', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 20:06:27'),
(97, 17, 'xdzeDC', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 20:07:50'),
(98, 18, 'dzed', NULL, NULL, NULL, NULL, 1, 'e8e170bb-b2f6-4341-bbbb-6b9c6c62d4b5.png', NULL, '2024-08-21 20:16:19'),
(99, 19, 'dezdez', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-21 20:19:25'),
(100, 23, 'ezfezr', NULL, NULL, NULL, NULL, 0, NULL, NULL, '2024-08-22 12:44:52'),
(101, 23, 'ezfezr', NULL, NULL, NULL, NULL, 0, NULL, NULL, '2024-08-22 12:45:37'),
(102, 24, 'dezd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-22 12:45:47'),
(103, 23, 'dezd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-08-24 10:52:07'),
(104, 4, 'lala', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-09-07 17:33:11'),
(105, 6, 'dezdsqX', NULL, NULL, NULL, '2024-10-09 00:00:00', 1, NULL, NULL, '2024-10-10 12:42:53'),
(106, 7, NULL, 'dezada', NULL, NULL, NULL, 1, NULL, NULL, '2024-10-10 12:43:59'),
(107, 8, '&quot;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur \n\naut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non - \n\nnumquam eius modi tempora\n incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&quot;', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-10-10 20:16:14'),
(108, 9, 'cdqsc', 'cdsqc', NULL, NULL, NULL, 1, NULL, NULL, '2024-10-11 20:10:51'),
(109, 10, 'deza', 'deza', NULL, 'Urgent', '2024-11-12 00:00:00', 1, NULL, NULL, '2024-11-07 19:30:10'),
(110, 1, 'cdqlzef', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-11-09 22:08:28'),
(111, 1, 'dzedzd', NULL, NULL, NULL, NULL, 1, NULL, NULL, '2024-11-09 22:10:10'),
(112, 2, 'e&quot;frzf&#039;', NULL, NULL, NULL, NULL, 1, '6d2ca621-6f4b-4260-b427-35b152f048f3.png', NULL, '2024-11-21 20:24:59');

-- --------------------------------------------------------

--
-- Structure de la table `item_has_lot`
--

CREATE TABLE `item_has_lot` (
  `item_id` int NOT NULL,
  `lot_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `item_has_lot`
--

INSERT INTO `item_has_lot` (`item_id`, `lot_id`) VALUES
(26, 25),
(25, 26),
(32, 33),
(39, 33),
(36, 34),
(39, 34),
(33, 35),
(34, 36),
(43, 40),
(85, 40),
(90, 40),
(96, 40),
(97, 40),
(41, 41),
(47, 41),
(49, 41),
(84, 41),
(89, 41),
(90, 41),
(91, 41),
(93, 41),
(94, 41),
(95, 41),
(96, 41),
(97, 41),
(41, 42),
(49, 42),
(91, 42),
(93, 42),
(94, 42),
(95, 42),
(59, 43),
(105, 44),
(106, 44),
(59, 45),
(105, 45),
(106, 45),
(60, 46);

-- --------------------------------------------------------

--
-- Structure de la table `lot`
--

CREATE TABLE `lot` (
  `id_lot` int NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `affair_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `lot`
--

INSERT INTO `lot` (`id_lot`, `name`, `affair_id`) VALUES
(25, 'Piscine', 14),
(26, 'Terrassements', 14),
(27, 'Espaces Verts', 14),
(28, 'Maçonneries', 14),
(29, 'Etancheité', 14),
(33, 'Maçonnerie', 15),
(34, 'Etanchelité', 15),
(35, 'Vegetaux', 15),
(36, 'Plomberie', 15),
(40, 'Lot test 1', 23),
(41, 'Lot 2', 23),
(42, 'Lot 3', 23),
(43, 'Lot 2', 25),
(44, 'Lot 3', 25),
(45, 'Lot 1', 25),
(46, 'Lot de test 1', 26),
(47, 'Lot de tessst 2', 26),
(48, 'Lot 3333', 26),
(49, 'Lot de test 1', 28),
(50, 'Lot de test 1', 28);

-- --------------------------------------------------------

--
-- Structure de la table `pv`
--

CREATE TABLE `pv` (
  `id_pv` int NOT NULL,
  `state` enum('En cours','Terminé') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'En cours',
  `meeting_date` timestamp NOT NULL,
  `meeting_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meeting_next_date` timestamp NULL DEFAULT NULL,
  `meeting_next_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `release_date` timestamp NULL DEFAULT NULL,
  `affair_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `pv`
--

INSERT INTO `pv` (`id_pv`, `state`, `meeting_date`, `meeting_place`, `meeting_next_date`, `meeting_next_place`, `release_date`, `affair_id`, `created_at`) VALUES
(11, 'Terminé', '2020-05-24 20:59:00', 'sur site', '2020-05-25 12:00:00', 'sur site', '2020-05-24 22:09:26', 13, '2020-05-28 21:09:09'),
(12, 'Terminé', '2020-05-24 19:37:43', 'a l\'agence', NULL, 'sur site', '2020-05-24 22:11:04', 13, '2020-05-28 21:09:09'),
(17, 'En cours', '2020-05-26 10:00:00', 'En Mairie', '2020-05-20 11:10:00', 'Sur place', NULL, 13, '2020-05-28 21:09:09'),
(18, 'Terminé', '2020-05-28 11:30:00', 'Sur place', '2020-05-27 12:15:00', 'Sur place', '2020-06-02 08:40:58', 13, '2020-05-28 21:09:09'),
(19, 'En cours', '2020-05-26 10:24:00', 'Indéfini', NULL, '', '2020-05-26 12:54:56', 15, '2020-05-28 21:09:09'),
(22, 'En cours', '2020-05-28 20:00:00', 'Pv test owner 2', NULL, '', '2020-05-28 20:49:02', 13, '2020-05-28 21:09:09'),
(23, 'En cours', '2020-05-28 21:00:00', 'pv test pour number', NULL, '', NULL, 13, '2020-05-28 21:23:07'),
(24, 'En cours', '2020-06-02 08:00:00', 'osijdfrt', NULL, '', NULL, 15, '2020-06-02 08:52:20'),
(25, 'En cours', '2023-11-03 23:21:06', 'Indéfini', NULL, '', NULL, 21, '2023-11-03 22:21:06'),
(26, 'En cours', '2023-11-03 23:25:00', 'Indéfini', NULL, 'rtehyt', NULL, 22, '2023-11-03 22:25:54'),
(27, 'Terminé', '2023-11-04 00:08:08', 'Indéfini', NULL, NULL, '2023-11-12 13:55:32', 23, '2023-11-03 23:08:08'),
(44, 'En cours', '2023-11-17 22:00:00', 'zpeife$', NULL, 'elioihgtr', NULL, 22, '2023-11-17 21:49:06'),
(45, 'En cours', '2023-11-17 22:00:00', 'frezrg', NULL, 'fezrgtr', NULL, 22, '2023-11-17 21:50:05'),
(46, 'En cours', '2023-11-17 22:00:00', 'dferfertf', NULL, 'frefetg', NULL, 22, '2023-11-17 21:51:22'),
(47, 'Terminé', '2023-11-17 22:00:00', 'fqreg', '2023-11-23 22:50:00', 'ggggggggggggg', '2023-11-26 22:16:29', 22, '2023-11-17 21:51:37'),
(48, 'Terminé', '2023-11-27 21:00:00', 'ici et là le lieu de la réu', '2023-11-22 15:30:00', 'lieu de la prochaine réu', '2023-11-27 20:54:00', 23, '2023-11-27 20:52:31'),
(49, 'En cours', '2023-12-20 14:00:00', 'réuinon de test', '2023-12-22 21:30:00', 'on verra plus tard', NULL, 23, '2023-12-20 13:19:46'),
(50, 'En cours', '2023-12-28 18:37:00', 'A modifier', NULL, NULL, NULL, 24, '2023-12-28 17:37:38'),
(51, 'En cours', '2023-12-28 18:41:00', 'A modifier', NULL, NULL, NULL, 25, '2023-12-28 17:41:42'),
(52, 'En cours', '2024-01-03 22:00:00', 'tyjytukuyik', NULL, '', NULL, 25, '2024-01-03 21:39:04'),
(53, 'En cours', '2024-06-13 12:00:00', 'Ici et là dd', '2024-06-23 18:30:00', 'ddezfrtgtyh', NULL, 23, '2024-06-13 19:58:43'),
(54, 'En cours', '2024-06-13 23:30:00', 'dezrgrtgtrg', NULL, NULL, NULL, 22, '2024-06-13 20:17:36'),
(55, 'En cours', '2024-06-13 23:30:00', 'dezrgrtgtrg', NULL, NULL, NULL, 22, '2024-06-13 20:17:47'),
(56, 'En cours', '2024-06-12 21:41:00', 'dezdezdedezdez', NULL, NULL, NULL, 25, '2024-06-13 20:18:32'),
(57, 'En cours', '2024-06-12 21:41:00', 'dezdezdedezdez', NULL, NULL, NULL, 25, '2024-06-13 20:18:37'),
(58, 'En cours', '2024-06-13 20:19:40', 'Indéfini', NULL, NULL, NULL, 26, '2024-06-13 20:19:40'),
(59, 'En cours', '2024-08-08 16:29:00', 'dezdezdezd', '2024-08-02 20:40:00', 'deazdaz', NULL, 23, '2024-08-24 18:33:08'),
(60, 'En cours', '2024-08-06 21:51:00', 'cdeeqrgrtgrtegrtgrt', NULL, NULL, NULL, 22, '2024-08-25 17:45:55'),
(61, 'En cours', '2024-08-07 23:30:00', 'dezadezadezd', NULL, NULL, NULL, 22, '2024-08-25 18:16:21'),
(62, 'En cours', '2024-08-25 21:45:00', 'dezadezdzed', NULL, NULL, NULL, 23, '2024-08-25 20:13:19'),
(63, 'En cours', '2024-08-08 23:30:00', 'dezadezadezd', NULL, NULL, NULL, 22, '2024-09-07 20:48:54'),
(64, 'En cours', '2024-08-15 23:30:00', 'dezadezadezd', '2024-09-13 15:35:00', 'rrdezr', NULL, 22, '2024-09-07 20:49:16'),
(65, 'En cours', '2024-08-15 23:30:00', 'ttdddezadezadezdffzzeerr12', NULL, 'Lala34567ef', NULL, 22, '2024-09-07 20:57:44'),
(66, 'En cours', '2024-09-05 23:50:00', 'Ce sera la prochaine réu', NULL, 'frezfe33', NULL, 23, '2024-09-11 21:01:15'),
(67, 'En cours', '2024-09-12 14:34:24', 'Indéfini', NULL, NULL, NULL, 27, '2024-09-12 14:34:24'),
(68, 'En cours', '2024-09-12 14:49:13', 'Indéfini', NULL, NULL, NULL, 28, '2024-09-12 14:49:13'),
(69, 'En cours', '2024-09-13 11:37:34', 'Indéfini', NULL, NULL, NULL, 29, '2024-09-13 11:37:34'),
(70, 'En cours', '2024-09-14 20:40:00', 'DZEDZAEDZEADEZ', NULL, NULL, NULL, 25, '2024-09-19 22:06:42'),
(71, 'En cours', '2024-11-09 22:09:08', 'Indéfini', NULL, NULL, NULL, 30, '2024-11-09 22:09:08'),
(72, 'En cours', '2024-11-22 20:25:00', 'ici et la', '2024-11-17 22:34:00', 'on verra', NULL, 30, '2024-11-21 20:36:21'),
(73, 'En cours', '2024-11-13 03:21:00', 'dezadzaed', NULL, NULL, NULL, 30, '2024-11-21 20:40:32'),
(74, 'En cours', '2024-11-13 03:21:00', 'dezadzaed', NULL, NULL, NULL, 30, '2024-11-21 20:54:58'),
(75, 'En cours', '2024-11-22 12:30:00', 'pzoagjert', NULL, NULL, NULL, 30, '2024-11-21 20:57:03');

-- --------------------------------------------------------

--
-- Structure de la table `pv_has_item`
--

CREATE TABLE `pv_has_item` (
  `pv_id` int NOT NULL,
  `item_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `pv_has_item`
--

INSERT INTO `pv_has_item` (`pv_id`, `item_id`) VALUES
(11, 18),
(11, 19),
(11, 20),
(11, 22),
(12, 24),
(11, 27),
(18, 31),
(19, 32),
(24, 32),
(19, 33),
(24, 33),
(47, 44),
(54, 44),
(55, 44),
(60, 44),
(61, 44),
(63, 44),
(64, 44),
(65, 44),
(47, 45),
(54, 45),
(55, 45),
(60, 45),
(61, 45),
(63, 45),
(64, 45),
(65, 45),
(47, 46),
(54, 46),
(55, 46),
(60, 46),
(61, 46),
(63, 46),
(64, 46),
(65, 46),
(48, 47),
(49, 47),
(53, 47),
(59, 47),
(62, 47),
(66, 47),
(48, 48),
(49, 48),
(53, 48),
(48, 49),
(49, 49),
(53, 49),
(59, 49),
(62, 49),
(66, 49),
(51, 50),
(52, 50),
(51, 51),
(52, 51),
(56, 51),
(57, 51),
(70, 51),
(51, 52),
(52, 52),
(51, 53),
(52, 53),
(56, 53),
(57, 53),
(70, 53),
(51, 54),
(52, 54),
(56, 54),
(57, 54),
(70, 54),
(51, 55),
(52, 55),
(56, 55),
(57, 55),
(70, 55),
(52, 59),
(56, 59),
(57, 59),
(70, 59),
(58, 60),
(58, 64),
(58, 78),
(58, 82),
(49, 83),
(49, 84),
(49, 85),
(49, 86),
(49, 87),
(49, 88),
(49, 89),
(49, 90),
(49, 91),
(49, 92),
(49, 93),
(49, 94),
(49, 95),
(49, 96),
(49, 97),
(49, 98),
(49, 99),
(58, 103),
(62, 104),
(66, 104),
(70, 105),
(70, 106),
(70, 107),
(70, 108),
(70, 109),
(68, 110),
(71, 111),
(72, 111),
(73, 111),
(74, 111),
(75, 111),
(71, 112),
(72, 112),
(73, 112),
(74, 112),
(75, 112);

-- --------------------------------------------------------

--
-- Structure de la table `pv_has_lot`
--

CREATE TABLE `pv_has_lot` (
  `id_pv` int NOT NULL,
  `id_lot` int NOT NULL,
  `progress` int NOT NULL,
  `already_done` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `pv_has_lot`
--

INSERT INTO `pv_has_lot` (`id_pv`, `id_lot`, `progress`, `already_done`) VALUES
(56, 43, 0, NULL),
(56, 44, 0, NULL),
(56, 45, 0, NULL),
(57, 43, 90, NULL),
(57, 44, 90, NULL),
(57, 45, 90, NULL),
(62, 40, 0, NULL),
(62, 41, 50, NULL),
(62, 42, 0, NULL),
(66, 40, 25, NULL),
(66, 41, 0, NULL),
(66, 42, 0, NULL),
(68, 49, 25, NULL),
(68, 50, 0, NULL),
(70, 43, 50, NULL),
(70, 44, 80, 'déjà fait'),
(70, 45, 30, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pv_has_user`
--

CREATE TABLE `pv_has_user` (
  `pv_id` int NOT NULL,
  `user_id` int NOT NULL,
  `status_PAE` enum('Présent','Absent','Excusé') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `invited_current_meeting` tinyint(1) DEFAULT '0',
  `invited_next_meeting` tinyint(1) DEFAULT '0',
  `distribution` tinyint(1) DEFAULT '0',
  `owner` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `pv_has_user`
--

INSERT INTO `pv_has_user` (`pv_id`, `user_id`, `status_PAE`, `invited_current_meeting`, `invited_next_meeting`, `distribution`, `owner`) VALUES
(11, 2, 'Présent', NULL, NULL, NULL, 1),
(11, 19, 'Présent', NULL, NULL, NULL, 1),
(11, 21, 'Présent', NULL, NULL, NULL, 1),
(11, 22, 'Absent', NULL, NULL, NULL, 1),
(12, 20, 'Présent', NULL, NULL, NULL, 1),
(18, 24, 'Présent', NULL, NULL, NULL, 1),
(19, 2, 'Présent', NULL, NULL, NULL, 1),
(22, 2, 'Présent', NULL, NULL, NULL, 1),
(23, 2, 'Présent', NULL, NULL, NULL, 1),
(24, 2, 'Présent', NULL, NULL, NULL, 1),
(26, 42, 'Présent', NULL, NULL, NULL, 1),
(27, 42, 'Présent', NULL, NULL, NULL, 1),
(27, 67, 'Présent', NULL, NULL, NULL, 0),
(44, 42, 'Présent', 0, 0, 0, 1),
(45, 42, 'Présent', 0, 0, 0, 1),
(46, 42, 'Présent', 0, 0, 0, 1),
(47, 42, 'Présent', 0, 0, 0, 1),
(48, 42, 'Présent', 0, 0, 0, 1),
(48, 67, 'Excusé', 0, 0, 0, 0),
(48, 68, 'Absent', NULL, NULL, NULL, 0),
(49, 42, 'Présent', 0, 0, 0, 1),
(49, 67, 'Excusé', 0, 0, 0, 0),
(49, 68, 'Absent', 0, 0, 0, 0),
(50, 42, 'Présent', NULL, NULL, NULL, 1),
(51, 42, 'Présent', NULL, NULL, NULL, 1),
(52, 42, 'Présent', 0, 0, 0, 1),
(53, 42, 'Présent', 0, 0, 0, 1),
(53, 67, 'Excusé', 0, 0, 0, 0),
(53, 68, 'Absent', 0, 0, 0, 0),
(54, 42, 'Présent', 0, 0, 0, 1),
(55, 42, 'Présent', 0, 0, 0, 1),
(56, 42, 'Présent', 0, 0, 0, 1),
(57, 42, 'Présent', 0, 0, 0, 1),
(58, 42, 'Présent', NULL, NULL, NULL, 1),
(58, 70, 'Absent', NULL, NULL, NULL, 0),
(59, 42, 'Présent', 0, 0, 0, 1),
(59, 67, 'Excusé', 0, 0, 0, 0),
(59, 68, 'Absent', 0, 0, 0, 0),
(60, 42, 'Présent', 0, 0, 0, 1),
(61, 42, 'Présent', 0, 0, 0, 1),
(62, 42, 'Présent', 0, 0, 0, 1),
(62, 67, 'Excusé', 0, 0, 0, 0),
(62, 68, 'Absent', 0, 0, 0, 0),
(62, 71, 'Absent', NULL, NULL, NULL, 0),
(63, 42, 'Présent', 0, 0, 0, 1),
(64, 42, 'Présent', 0, 0, 0, 1),
(65, 42, 'Présent', 0, 0, 0, 1),
(65, 72, 'Présent', NULL, NULL, NULL, 0),
(65, 73, 'Présent', NULL, NULL, NULL, 0),
(65, 74, 'Présent', NULL, NULL, NULL, 0),
(66, 42, 'Présent', 0, 0, 0, 1),
(66, 67, 'Excusé', 0, 0, 0, 0),
(66, 68, 'Absent', 0, 0, 0, 0),
(66, 71, 'Absent', 0, 0, 0, 0),
(67, 42, 'Présent', NULL, NULL, NULL, 1),
(68, 42, 'Présent', NULL, NULL, NULL, 1),
(69, 42, 'Absent', NULL, NULL, NULL, 1),
(70, 42, 'Présent', 1, 1, 1, 1),
(70, 67, 'Présent', 0, 1, 0, 0),
(70, 68, 'Présent', 0, 0, 0, 0),
(70, 70, 'Présent', 0, 1, 0, 0),
(70, 71, 'Excusé', 1, 0, 0, 0),
(71, 42, 'Présent', 1, 1, 1, 1),
(72, 42, 'Présent', 1, 1, 1, 1),
(72, 70, 'Absent', 0, 1, 0, 0),
(72, 74, 'Présent', 0, 1, 1, 0),
(73, 42, 'Présent', 1, 1, 1, 1),
(74, 42, 'Présent', 1, 1, 1, 1),
(74, 71, 'Présent', 0, 1, 1, 0),
(74, 74, 'Excusé', 0, 0, 0, 0),
(75, 42, 'Présent', 1, 1, 1, 1),
(75, 71, 'Présent', 0, 1, 1, 0),
(75, 74, 'Excusé', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `schedule`
--

CREATE TABLE `schedule` (
  `id_schedule` int NOT NULL,
  `note` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deadline` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `pv_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `token`
--

CREATE TABLE `token` (
  `token` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `device` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration_date` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id_user` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id_user` int NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_function` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organism` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `email`, `password`, `first_name`, `last_name`, `phone`, `user_group`, `user_function`, `organism`) VALUES
(42, 'john@doe.fr', '$2y$10$ZES4P7zcYXAWjXQOr5q5Kuio/dBZ6nqXlBoBrwEs8l.fe8CVUZOZm', 'John', 'Doe', '1234567890', 'Maîtrise d&#039;ouvrage', '', ''),
(64, 'baudouin@ik.me', '$2y$10$0EWEQpSSbkVZGwPD6ZWkG.mo0VV5p1BJt3ClXvql9XnU5y//91x82', 'Baudouin', 'Coupey', '0123456789', '', NULL, NULL),
(65, 'marie@curie.fr', '$2y$10$MpacAvKh4EEz6TUE0ySQpurBKTdsCykZvjHeJ9MaRdUq6tLUCA8mK', 'Marie', 'Curie', '0123456789', 'Maîtrise d\'oeuvre', 'Physicienne', 'Paster'),
(66, 'maie@curie.fr', '$2y$10$jWlw30SYH8Y6GrbSAyxRCe04ioEoV9Onkp3d/fhfGgVrcX.9KVzpm', 'Marie', 'Curie', '0123456789', 'Maîtrise d\'oeuvre', 'Pysicienne', 'Paster'),
(67, 'marie@curie.fr', '$2y$10$jm915m7eeJ8UWzElnqRPKeiFAVQMrxClTTVyUidG9I6ZKii8fto6e', 'Marie', 'Curie', '0123456789', 'Maîtrise d\'oeuvre', 'Pysicienne', 'Paster'),
(68, 'mail@mail.fr', '$2y$10$Bfukx6n48uaQnwJuJ7Znq.ON/az3ajysk9XiSKJtn9CBR787izoGa', 'Omb', 'Coup', '0123456789', 'Concessionnaire', 'fct', 'orga'),
(69, 'baud@baud.fr', '$2y$10$h6JK3rFcmTqV5RGLmJKULO4rEYLEINqVcMFm2BiNuRy6PYPWhq2by', 'Baudouin', 'Coupey', '0123456789', 'Maîtrise d&#039;ouvrage', 'Dev', 'PvApp'),
(70, 'dad@ded.fr', '$2y$10$LlqtlQwZmGAohat7BFVvpe5orV60kXQ5IHqDvil.pVcBcnSmYe2dS', 'de', 'dez', '0123456789', 'Maîtrise d&#039;oeuvre', 'de', 'dez'),
(71, 'de@de.fr', '$2y$10$hiMKVAMDbWU.q0fBKlwaieon3qKLK7/oOwMGZ1gGh4Ys2V976B05i', 'Baudouin', 'Coup', '0123456789', 'Maîtrise d&#039;ouvrage', 'defre', 'dezafder'),
(72, 'de@de.Fr', '$2y$10$YCPX5fn6Qb5AATU8/SjFs.Qzo1D/1drSFyIo65EbmyLZz7gLr2Q76', 'Baud', 'Coupe', '1234567890', 'Maîtrise d&#039;ouvrage', 'dez', 'Casals'),
(73, 'dez@der.Fr', '$2y$10$PMcafSGA9jdaVXkShGjttepKb.W.7XXNcNbqDUjlwNaq9A.gdDPeS', 'dezd', 'efder', '1234567890', 'Maîtrise d&#039;oeuvre', 'ddede', 'dezdz'),
(74, 'deza@de.fr', '$2y$10$b4LxsYB9ps1nKeK8OYtBj.cNYjXWfzy9e41m9i1swqPjiadv6bjly', 'dezad', 'dezad', '1234567890', 'Maîtrise d&#039;oeuvre', 'dezadz', 'dezdz');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `affair`
--
ALTER TABLE `affair`
  ADD PRIMARY KEY (`id_affair`);

--
-- Index pour la table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id_agenda`,`pv_id`),
  ADD KEY `fk_agenda_pv1_idx` (`pv_id`);

--
-- Index pour la table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id_item`);

--
-- Index pour la table `item_has_lot`
--
ALTER TABLE `item_has_lot`
  ADD PRIMARY KEY (`item_id`,`lot_id`),
  ADD KEY `fk_item_has_lot_lot1_idx` (`lot_id`),
  ADD KEY `fk_item_has_lot_item1_idx` (`item_id`);

--
-- Index pour la table `lot`
--
ALTER TABLE `lot`
  ADD PRIMARY KEY (`id_lot`),
  ADD KEY `fk_lot_affair1_idx` (`affair_id`);

--
-- Index pour la table `pv`
--
ALTER TABLE `pv`
  ADD PRIMARY KEY (`id_pv`),
  ADD KEY `fk_pv_affair1_idx` (`affair_id`);

--
-- Index pour la table `pv_has_item`
--
ALTER TABLE `pv_has_item`
  ADD PRIMARY KEY (`pv_id`,`item_id`),
  ADD KEY `fk_pv_has_item_item1_idx` (`item_id`),
  ADD KEY `fk_pv_has_item_pv1_idx` (`pv_id`);

--
-- Index pour la table `pv_has_lot`
--
ALTER TABLE `pv_has_lot`
  ADD PRIMARY KEY (`id_pv`,`id_lot`),
  ADD KEY `fk_pv_has_lot_lot1_idx` (`id_lot`),
  ADD KEY `fk_pv_has_lot_pv1_idx` (`id_pv`);

--
-- Index pour la table `pv_has_user`
--
ALTER TABLE `pv_has_user`
  ADD PRIMARY KEY (`pv_id`,`user_id`),
  ADD KEY `fk_pv_has_user_user1_idx` (`user_id`),
  ADD KEY `fk_pv_has_user_pv1_idx` (`pv_id`);

--
-- Index pour la table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id_schedule`,`pv_id`),
  ADD KEY `fk_schedule_pv1_idx` (`pv_id`);

--
-- Index pour la table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`token`),
  ADD KEY `fk_token_user1_idx` (`user_id_user`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `affair`
--
ALTER TABLE `affair`
  MODIFY `id_affair` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id_agenda` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `item`
--
ALTER TABLE `item`
  MODIFY `id_item` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT pour la table `lot`
--
ALTER TABLE `lot`
  MODIFY `id_lot` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT pour la table `pv`
--
ALTER TABLE `pv`
  MODIFY `id_pv` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `agenda`
--
ALTER TABLE `agenda`
  ADD CONSTRAINT `fk_agenda_pv1` FOREIGN KEY (`pv_id`) REFERENCES `pv` (`id_pv`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `item_has_lot`
--
ALTER TABLE `item_has_lot`
  ADD CONSTRAINT `fk_item_has_lot_item1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id_item`),
  ADD CONSTRAINT `fk_item_has_lot_lot1` FOREIGN KEY (`lot_id`) REFERENCES `lot` (`id_lot`);

--
-- Contraintes pour la table `lot`
--
ALTER TABLE `lot`
  ADD CONSTRAINT `fk_lot_affair1` FOREIGN KEY (`affair_id`) REFERENCES `affair` (`id_affair`);

--
-- Contraintes pour la table `pv`
--
ALTER TABLE `pv`
  ADD CONSTRAINT `fk_pv_affair1` FOREIGN KEY (`affair_id`) REFERENCES `affair` (`id_affair`);

--
-- Contraintes pour la table `pv_has_item`
--
ALTER TABLE `pv_has_item`
  ADD CONSTRAINT `fk_pv_has_item_item1` FOREIGN KEY (`item_id`) REFERENCES `item` (`id_item`),
  ADD CONSTRAINT `fk_pv_has_item_pv1` FOREIGN KEY (`pv_id`) REFERENCES `pv` (`id_pv`);

--
-- Contraintes pour la table `pv_has_lot`
--
ALTER TABLE `pv_has_lot`
  ADD CONSTRAINT `fk_pv_has_lot_lot1` FOREIGN KEY (`id_lot`) REFERENCES `lot` (`id_lot`),
  ADD CONSTRAINT `fk_pv_has_lot_pv1` FOREIGN KEY (`id_pv`) REFERENCES `pv` (`id_pv`);

--
-- Contraintes pour la table `pv_has_user`
--
ALTER TABLE `pv_has_user`
  ADD CONSTRAINT `fk_pv_has_user_pv1_idx` FOREIGN KEY (`pv_id`) REFERENCES `pv` (`id_pv`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_pv_has_user_user1_idx` FOREIGN KEY (`user_id`) REFERENCES `user` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `fk_schedule_pv1` FOREIGN KEY (`pv_id`) REFERENCES `pv` (`id_pv`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Contraintes pour la table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_token_user1` FOREIGN KEY (`user_id_user`) REFERENCES `user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
