<?php

use Slim\App;
use App\Middleware\CorsMiddleware;
use App\Middleware\LoginMiddleware;
use App\Middleware\JwtClaimMiddleware;
use Selective\BasePath\BasePathMiddleware;

return function (App $app) {
    // Parse json, form data and xml
    $app->addBodyParsingMiddleware();

    // Add the Slim built-in routing middleware
    $app->addRoutingMiddleware();

    // Detect and configure basepath
    $app->add(BasePathMiddleware::class);
    //old way to set basepath
    // $app->setBasePath('/public');

    // Catch exceptions and errors
    $app->addErrorMiddleware(true, true, true); 
    
    // Add CORS middleware
    $app->add(CorsMiddleware::class);

    $app->add(JwtClaimMiddleware::class);
};
