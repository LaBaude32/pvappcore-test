<?php

use Slim\App;
use App\Middleware\JwtAuthMiddleware;
use Slim\Routing\RouteCollectorProxy;

return function (App $app) {
    // This route must not be protected
    $app->get('/api/v1/home', \App\Action\HomeAction::class);
    $app->post('/api/v1/tokens', \App\Action\Auth\TokenCreateAction::class);
    $app->post('/api/v1/users', \App\Action\User\UserCreateAction::class);
    $app->get('/docs/v1', \App\Action\Docs\SwaggerUiAction::class);
    $app->get('/api/v1/pvs/pvId/released/pdf', \App\Action\Pv\PvGetPdfAction::class);
    $app->post('/api/v1/participants/selfCreate', \App\Action\Participant\ParticipantSelfCreateAction::class);

    //Protected routes
    $app->group('/api/v1', function (RouteCollectorProxy $group) {
        $group->get('/login', \App\Action\User\LoginAction::class);

        //User
        // $group->post('/addUser', \App\Action\UserCreateAction::class);
        $group->get('/users', \App\Action\User\UsersListAction::class);
        $group->put('/users/userId', \App\Action\User\UserUpdateAction::class);
        $group->delete('/users/userId', \App\Action\User\UserDeleteAction::class);

        //Participants
        $group->get('/participants/connected/userId', \App\Action\Participant\ParticipantGetConnectedAction::class);
        $group->get('/participants/otp/pvId', \App\Action\Participant\ParticipantOTPForSelfCreateAction::class);
        $group->put('/participants/userId', \App\Action\Participant\ParticipantUpdateAction::class);
        $group->put('/participants/userId/updateStatus', \App\Action\Participant\ParticipantUpdateStatusAction::class);
        $group->delete('/participants/userId', \App\Action\Participant\ParticipantDeleteAction::class);
        $group->post('/participants', \App\Action\Participant\ParticipantCreateAction::class);

        //Affair
        $group->post('/affairs', \App\Action\Affair\AffairCreateAction::class);
        $group->get('/affairs', \App\Action\Affair\AffairsListAction::class);
        $group->get('/affairs/affairId', \App\Action\Affair\AffairGetByIdAction::class);
        $group->get('/affairs/full/affairId', \App\Action\Affair\AffairGetFullByIdAction::class);
        $group->get('/affairs/userId', \App\Action\Affair\AffairsGetByUserIdAction::class);
        $group->put('/affairs/affairId', \App\Action\Affair\AffairUpdateAction::class);
        $group->delete('/affairs/affairId', \App\Action\Affair\AffairDeleteAction::class);

        //Lot
        $group->post('/lots', \App\Action\Lot\LotCreateAction::class);
        $group->put('/lots/lotId', \App\Action\Lot\LotUpdateAction::class);
        $group->put('/lots/progress', \App\Action\Lot\LotUpdateProgressAction::class);
        $group->delete('/lots/lotId', \App\Action\Lot\LotDeleteAction::class);

        //Pv
        $group->post('/pvs', \App\Action\Pv\PvCreateAction::class);
        $group->get('/pvs/pvId', \App\Action\Pv\PvGetByIdAction::class);
        $group->put('/pvs/pvId', \App\Action\Pv\PvUpdateAction::class);
        $group->delete('/pvs/pvId', \App\Action\Pv\PvDeleteAction::class);
        $group->get('/pvs/affairId', \App\Action\Pv\PvGetByAffairIdAction::class);
        $group->get('/pvs/userId', \App\Action\Pv\PvsGetLastsByUserIdAction::class);
        $group->get('/pvs/pvId/released', \App\Action\Pv\PvGetReleasedDetails::class);
        $group->get('/pvs/userId/released', \App\Action\Pv\PvsGetReleasedByUserIdAction::class);
        $group->put('/pvs/pvId/validation', \App\Action\Pv\PvValidateAction::class);
        $group->put('/pvs/pvId/unValidate', \App\Action\Pv\PvUnValidateAction::class);

        //Item
        $group->post('/items', \App\Action\Item\ItemCreateAction::class);
        $group->post('/items/updateImage', \App\Action\Item\ImageUpdateAction::class);
        $group->post('/items/uploadImage', \App\Action\Item\ImageUploadAction::class);
        $group->put('/items/itemId', \App\Action\Item\ItemUpdateAction::class);
        $group->put('/items/itemId/visibility', \App\Action\Item\ItemUpdateVisibleAction::class);
        $group->delete('/items/itemId', \App\Action\Item\ItemDeleteAction::class);
        $group->delete('/itemHasPv', \App\Action\Item\ItemHasPvDeleteAction::class);

        //Agenda
        $group->put('/agendas/agendaId', \App\Action\Agenda\AgendaUpdateAction::class);
        $group->delete('/agendas/agendaId', \App\Action\Agenda\AgendaDeleteAction::class);

        //Token
        $group->delete('/deleteToken', \App\Action\Auth\TokenDeleteAction::class);
        $group->get('/getTokensByUserId', \App\Action\Auth\TokensGetByUserIdAction::class);
    })->add(JwtAuthMiddleware::class);
};
