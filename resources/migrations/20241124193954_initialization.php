<?php

declare(strict_types=1);

use Phoenix\Migration\AbstractMigration;

final class Initialization extends AbstractMigration
{
    protected function up(): void
    {
        $this->table('affair', 'id_affair')
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('id_affair', 'integer', ['autoincrement' => true])
            ->addColumn('name', 'string')
            ->addColumn('address', 'string')
            ->addColumn('progress', 'integer', ['null' => true])
            ->addColumn('meeting_type', 'enum', ['length' => 0, 'decimals' => 0, 'values' => ['Chantier', 'Etude']])
            ->addColumn('description', 'text', ['null' => true])
            ->create();

        $this->table('agenda', ['id_agenda', 'pv_id'])
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('id_agenda', 'integer', ['autoincrement' => true])
            ->addColumn('position', 'integer', ['default' => 0])
            ->addColumn('title', 'string', ['length' => 535])
            ->addColumn('pv_id', 'integer')
            ->addIndex('pv_id', '', 'btree', 'fk_agenda_pv1_idx')
            ->create();

        $this->table('item', 'id_item')
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('id_item', 'integer', ['autoincrement' => true])
            ->addColumn('position', 'integer')
            ->addColumn('note', 'text', ['null' => true])
            ->addColumn('follow_up', 'text', ['null' => true])
            ->addColumn('resources', 'string', ['null' => true])
            ->addColumn('completion', 'string', ['null' => true])
            ->addColumn('completion_date', 'timestamp', ['null' => true])
            ->addColumn('visible', 'tinyinteger', ['length' => 4])
            ->addColumn('image', 'string', ['null' => true])
            ->addColumn('thumbnail', 'string', ['null' => true])
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->create();

        $this->table('item_has_lot', ['item_id', 'lot_id'])
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('item_id', 'integer')
            ->addColumn('lot_id', 'integer')
            ->addIndex('item_id', '', 'btree', 'fk_item_has_lot_item1_idx')
            ->addIndex('lot_id', '', 'btree', 'fk_item_has_lot_lot1_idx')
            ->create();

        $this->table('lot', 'id_lot')
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('id_lot', 'integer', ['autoincrement' => true])
            ->addColumn('name', 'string')
            ->addColumn('affair_id', 'integer')
            ->addIndex('affair_id', '', 'btree', 'fk_lot_affair1_idx')
            ->create();

        $this->table('pv', 'id_pv')
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('id_pv', 'integer', ['autoincrement' => true])
            ->addColumn('state', 'enum', ['default' => 'En cours', 'length' => 0, 'decimals' => 0, 'values' => ['En cours', 'Terminé']])
            ->addColumn('meeting_date', 'timestamp')
            ->addColumn('meeting_place', 'string')
            ->addColumn('meeting_next_date', 'timestamp', ['null' => true])
            ->addColumn('meeting_next_place', 'string', ['null' => true])
            ->addColumn('release_date', 'timestamp', ['null' => true])
            ->addColumn('affair_id', 'integer')
            ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
            ->addIndex('affair_id', '', 'btree', 'fk_pv_affair1_idx')
            ->create();

        $this->table('pv_has_item', ['pv_id', 'item_id'])
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('pv_id', 'integer')
            ->addColumn('item_id', 'integer')
            ->addIndex('item_id', '', 'btree', 'fk_pv_has_item_item1_idx')
            ->addIndex('pv_id', '', 'btree', 'fk_pv_has_item_pv1_idx')
            ->create();

        $this->table('pv_has_lot', ['id_pv', 'id_lot'])
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('id_pv', 'integer')
            ->addColumn('id_lot', 'integer')
            ->addColumn('progress', 'integer')
            ->addColumn('already_done', 'string', ['null' => true])
            ->addIndex('id_lot', '', 'btree', 'fk_pv_has_lot_lot1_idx')
            ->addIndex('id_pv', '', 'btree', 'fk_pv_has_lot_pv1_idx')
            ->create();

        $this->table('pv_has_user', ['pv_id', 'user_id'])
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('pv_id', 'integer')
            ->addColumn('user_id', 'integer')
            ->addColumn('status_PAE', 'enum', ['length' => 0, 'decimals' => 0, 'values' => ['Présent', 'Absent', 'Excusé']])
            ->addColumn('invited_current_meeting', 'boolean', ['null' => true, 'default' => false])
            ->addColumn('invited_next_meeting', 'boolean', ['null' => true, 'default' => false])
            ->addColumn('distribution', 'boolean', ['null' => true, 'default' => false])
            ->addColumn('owner', 'boolean', ['default' => false])
            ->addIndex('pv_id', '', 'btree', 'fk_pv_has_user_pv1_idx')
            ->addIndex('user_id', '', 'btree', 'fk_pv_has_user_user1_idx')
            ->create();

        $this->table('schedule', ['id_schedule', 'pv_id'])
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('id_schedule', 'integer')
            ->addColumn('description', 'string')
            ->addColumn('deadline', 'string')
            ->addColumn('pv_id', 'integer')
            ->addIndex('pv_id', '', 'btree', 'fk_schedule_pv1_idx')
            ->create();

        $this->table('token', 'token')
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('token', 'string', ['length' => 45])
            ->addColumn('device', 'string', ['length' => 45])
            ->addColumn('expiration_date', 'string', ['null' => true, 'length' => 45])
            ->addColumn('user_id_user', 'integer')
            ->addIndex('user_id_user', '', 'btree', 'fk_token_user1_idx')
            ->create();

        $this->table('user', 'id_user')
            ->setCharset('utf8mb4')
            ->setCollation('utf8mb4_unicode_ci')
            ->addColumn('id_user', 'integer', ['autoincrement' => true])
            ->addColumn('email', 'string')
            ->addColumn('password', 'string')
            ->addColumn('first_name', 'string', ['length' => 100])
            ->addColumn('last_name', 'string')
            ->addColumn('phone', 'string', ['length' => 10])
            ->addColumn('user_group', 'string')
            ->addColumn('user_function', 'string', ['null' => true])
            ->addColumn('organism', 'string', ['null' => true])
            ->create();

        $this->table('agenda')
            ->addForeignKey('pv_id', 'pv', 'id_pv', 'restrict', 'restrict')
            ->save();

        $this->table('item_has_lot')
            ->addForeignKey('item_id', 'item', 'id_item', 'no action', 'no action')
            ->addForeignKey('lot_id', 'lot', 'id_lot', 'no action', 'no action')
            ->save();

        $this->table('lot')
            ->addForeignKey('affair_id', 'affair', 'id_affair', 'no action', 'no action')
            ->save();

        $this->table('pv')
            ->addForeignKey('affair_id', 'affair', 'id_affair', 'no action', 'no action')
            ->save();

        $this->table('pv_has_item')
            ->addForeignKey('item_id', 'item', 'id_item', 'no action', 'no action')
            ->addForeignKey('pv_id', 'pv', 'id_pv', 'no action', 'no action')
            ->save();

        $this->table('pv_has_lot')
            ->addForeignKey('id_lot', 'lot', 'id_lot', 'no action', 'no action')
            ->addForeignKey('id_pv', 'pv', 'id_pv', 'no action', 'no action')
            ->save();

        $this->table('pv_has_user')
            ->addForeignKey('pv_id', 'pv', 'id_pv', 'restrict', 'restrict')
            ->addForeignKey('user_id', 'user', 'id_user', 'restrict', 'restrict')
            ->save();

        $this->table('schedule')
            ->addForeignKey('pv_id', 'pv', 'id_pv', 'restrict', 'restrict')
            ->save();

        $this->table('token')
            ->addForeignKey('user_id_user', 'user', 'id_user', 'no action', 'no action')
            ->save();
    }

    protected function down(): void
    {
        $this->table('affair')
            ->drop();

        $this->table('agenda')
            ->drop();

        $this->table('item')
            ->drop();

        $this->table('item_has_lot')
            ->drop();

        $this->table('lot')
            ->drop();

        $this->table('pv')
            ->drop();

        $this->table('pv_has_item')
            ->drop();

        $this->table('pv_has_lot')
            ->drop();

        $this->table('pv_has_user')
            ->drop();

        $this->table('schedule')
            ->drop();

        $this->table('token')
            ->drop();

        $this->table('user')
            ->drop();
    }
}
