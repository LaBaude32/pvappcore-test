<?php

declare(strict_types=1);

use Phoenix\Migration\AbstractMigration;

final class ScheduleLinkToLots extends AbstractMigration
{
    protected function up(): void
    {
        $this->table('schedule')
            ->addColumn('lot_id', 'integer')
            // ->addIndex('pv_id', '', 'btree', 'fk_schedule_pv1_idx')
            ->save();
    }

    protected function down(): void
    {
        $this->table('schedule')
            ->dropColumn('lot_id')
            // ->addIndex('pv_id', '', 'btree', 'fk_schedule_pv1_idx')
            ->save();
    }
}
