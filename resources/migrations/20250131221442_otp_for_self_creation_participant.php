<?php

declare(strict_types=1);

use Phoenix\Migration\AbstractMigration;

final class OtpForSelfCreationParticipant extends AbstractMigration
{
    protected function up(): void
    {
        $this->table('participant_otp', 'pv_id')
            ->addColumn('pv_id', 'integer')
            ->addColumn('otp', 'integer')
            ->addColumn('created_at', 'datetime')
            ->create();
    }

    protected function down(): void
    {
        $this->table('participant_otp')
            ->drop();
    }
}
