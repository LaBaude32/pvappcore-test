# MAKE :

Installer make :

```
 apt-get install make
```

# DOCKER :

Creer l'image :
Se mettre à la racine et executer :

```
docker build . -f ./docker/Dockerfile -t baldwin/pvapp-core
```

# DEPLOIEMENT :

### Certifiaction avec Traefik

créer un fichier `acme.json` dans le dossier `certs`

```
mkdir certs
touch certs/acme.json
chmod 600 certs/acme.json
```

### .env

créer un fichier `.env` à partir du `.env.template` et le remplir correctement

### JWT

ajouter la clef RSA pourr JWT :

```
openssl genrsa -out private.pem 2048
openssl rsa -in private.pem -outform PEM -pubout -out public.pem
chmod 755 public.pem
chmod 755 private.pem
```

### Déploiement -> Obsolète

- installer composer
- lancer un `composer install`
- lancer un `composer run-prod`

### Images

- Creer un dossier `images` dans le dossier `public`
- Faire un `sudo chown www-data:www-data public/images/`
